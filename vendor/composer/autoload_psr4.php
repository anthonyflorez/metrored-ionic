<?php

// autoload_psr4.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'Twig\\' => array($vendorDir . '/twig/twig/src'),
    'Slim\\Views\\' => array($vendorDir . '/slim/twig-view/src'),
    'Psr\\Http\\Message\\' => array($vendorDir . '/psr/http-message/src'),
);
