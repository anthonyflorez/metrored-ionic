import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';

/*
  Generated class for the TokenPushProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class TokenPushProvider {

  public addTokenData: {
    id:number,
    alias:string,
    token:string
  }
  public setToken:{
    id_user:string,
    id_token:string
  }
  public token_id: {
    token: string,
    id_persona: string
  }
  public get_token:{
    id_persona:string
  }
id_token:string 
id_user:string

  private base_url:string
  constructor(public http: Http) {
    this.base_url = "http://metroredapp.hmetro.med.ec/metrored/public/auth/";

  }

  addToken(){
    this.addTokenData={
      id:Number(this.id_user),
      alias:"",
      token:this.id_token
    }
    let headers = new Headers();
    headers.append('Content-Type','application/json');
    return new Promise(resolve => {
      this.http.post(this.base_url+"tokenPush", this.addTokenData, {headers})
          .map(res => res.json())
          .subscribe(data =>  {
            resolve(data)
          })
    })
  }
  setTokenPush(){

    let headers = new Headers();
    headers.append('Content-Type','application/json');
    return new Promise(resolve => {
      this.http.post(this.base_url+"setTokenPush", this.token_id, {headers})
          .map(res => res.json())
          .subscribe(data =>  {
            resolve(data)
          })
    })
  }
  getTokenPush(): Observable<{}> {

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return  this.http.post(this.base_url + "getTokenPush", this.get_token, { headers })
      .map(res => res.json());

  }
}
