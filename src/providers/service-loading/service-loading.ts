// importamos el modulo Injectable de AngularJS
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NavController, NavParams, LoadingController, Loading, AlertController  } from 'ionic-angular';

// import { OrderServicesProvider } from '../providers/order-services/order-services';

// Permitimos que este objeto se pueda inyectar con la DI
@Injectable()
export class ServiceLoading {
    loading: Loading;
    alert_present:boolean
    
    constructor(private loadingCtrl: LoadingController,private alertCtrl: AlertController){
        this.alert_present=false;
    }


    showLoading(message){
        this.loading = this.loadingCtrl.create({
            content: message,
            dismissOnPageChange: false
        })
        this.loading.present()
      }


     Loading()
     {
        // this.showLoading("Reintentando conexiòn \n espere por favor...")
        // setTimeout(() => {
        //     this.loading.dismiss();
            // this.showLoading("Reintentando conexiòn \n espere por favor...");
            // this.Reintentar();
            // setTimeout(()=>{
                // this.loading.dismiss();
                let loading = this.loadingCtrl.create({
                    content: 'Reintentando conexiòn \n espere por favor...'
                  });
                
                  loading.present();
                
                  setTimeout(() => {
                    loading.dismiss();
                    if(this.alert_present==false){
                        this.presentAlert("Error","Disculpe en estos momentos presentamos inconvenientes con el sistema de gestiòn de citas, por favor intente mas tarde.","Cerrar");
                        this.alert_present=true
                    }

                  }, 5000);
                  this.alert_present=false
                // this.showLoading("Reintentando conexiòn \n espere por favor...");
    
                
                
                // setTimeout(()=>{
                //     this.loading.dismiss();
                // },9000);
                // setTimeout(()=>{
                //     this.loading.dismiss();
                //     this.presentAlert("Error","Disculpe en estos momentos presentamos inconvenientes con el sistema de gestiòn de citas, por favor intente mas tarde.","Cerrar");
                // },10000);
        //     },5000);
        // }, 5000);
     }

     presentAlert(title, subtitle, text_button) 
     {
        let alert = this.alertCtrl.create({
          title: title,
          subTitle: subtitle,
          buttons: [{
              text:text_button,
              handler: () => {
                
              }
        },
    ]
        });
        alert.present();
      }
  

}