import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the SpecialtyProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class SpecialtyProvider {

  private base_url:string;
  data:any
  public especialidad: string
  constructor(public http: Http) {
    this.base_url = "http://metroredapp.hmetro.med.ec/metrored/public/api/generales/";
  }

  getEspecialidades(){
    if(this.data){
      return Promise.resolve(this.data);
    }
    let headers = new Headers();
    headers.append('Content-Type','application/json');
    headers.append('Accept', 'application/json');
    headers.append('Authorization','Bearer ZXN0ZSBlcyBlbCB0b2tlbiBxdWUgc2UgdmEgYSB1dGlsaXphciBkZSBwcnVlYmE=');
    return new Promise((resolve, reject) => {
      this.http.get(this.base_url+"especialidades",{ headers: headers })
          .map(res => res.json())
          .subscribe(data => {
            this.data = data;
            resolve(this.data)
          }, error=>{
            reject(error)
          })
    })
  }
  getSpecialiteByName(){
    let headers = new Headers();
    headers.append('Content-Type','application/json');
    headers.append('Accept', 'application/json');
    headers.append('Authorization','Bearer ZXN0ZSBlcyBlbCB0b2tlbiBxdWUgc2UgdmEgYSB1dGlsaXphciBkZSBwcnVlYmE=');
    return new Promise(resolve => {
      this.http.post(this.base_url+"especialidadNombre",{especialidad: this.especialidad}, { headers: headers })
          .map(res => res.json())
          .subscribe(data => {
            this.data = data;
            resolve(this.data)
          })
    })
  }
}
