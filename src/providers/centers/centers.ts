import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the CentersProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class CentersProvider {
  private base_url:string;
  public especialidad: number;
  public centro: string
  constructor(public http: Http) {
    this.base_url = "http://metroredapp.hmetro.med.ec/metrored/public/api/localidades/";
  }

  getCentros(){
    let headers = new Headers();
    headers.append('Content-Type','application/json');
    headers.append('Accept', 'application/json');
    headers.append('Authorization','Bearer ZXN0ZSBlcyBlbCB0b2tlbiBxdWUgc2UgdmEgYSB1dGlsaXphciBkZSBwcnVlYmE=');
    return new Promise((resolve,reject) => {
      this.http.get(this.base_url+"todos/"+this.especialidad,{ headers: headers })
          .map(res => res.json())
          .subscribe(data => {
            resolve(data.result)
          },error =>{
            reject("error")
          })

    })
  }
  getCentros2(){
    let headers = new Headers();
    headers.append('Content-Type','application/json');
    headers.append('Accept', 'application/json');
    headers.append('Authorization','Bearer ZXN0ZSBlcyBlbCB0b2tlbiBxdWUgc2UgdmEgYSB1dGlsaXphciBkZSBwcnVlYmE=');
    return new Promise((resolve,reject) => {
      this.http.get(this.base_url+"todos2/"+this.especialidad,{ headers: headers })
          .map(res => res.json())
          .subscribe(data => {
            resolve(data.result)
            
          },error =>{
            reject("error")
          }
        )
         
    })
  }
  getCentroByName(){
    let headers = new Headers();
    headers.append('Content-Type','application/json');
    headers.append('Accept', 'application/json');
    headers.append('Authorization','Bearer ZXN0ZSBlcyBlbCB0b2tlbiBxdWUgc2UgdmEgYSB1dGlsaXphciBkZSBwcnVlYmE=');
    return new Promise(resolve => {
      this.http.post(this.base_url+"centroNombre",{centro: this.centro},{headers})
          .map(res => res.json())
          .subscribe(data => {
            resolve(data)
          })
    })
  }
  getCentroByNameLocal(){
    let headers = new Headers();
    headers.append('Content-Type','application/json');
    headers.append('Accept', 'application/json');
    headers.append('Authorization','Bearer ZXN0ZSBlcyBlbCB0b2tlbiBxdWUgc2UgdmEgYSB1dGlsaXphciBkZSBwcnVlYmE=');
    return new Promise(resolve => {
      this.http.post("http://metroredapp.hmetro.med.ec/metrored/public/api/local/pacientes/getCenterByName",{centro: this.centro},{headers})
          .map(res => res.json())
          .subscribe(data => {
            resolve(data)
          })
    })
  }

  getCenterIn(){
    let headers = new Headers();
    headers.append('Content-Type','application/json');
    headers.append('Accept', 'application/json');
    headers.append('Authorization','Bearer ZXN0ZSBlcyBlbCB0b2tlbiBxdWUgc2UgdmEgYSB1dGlsaXphciBkZSBwcnVlYmE=');
    return new Promise(resolve => {
      this.http.post("http://metroredapp.hmetro.med.ec/metrored/public/api/local/pacientes/getCenterIn",{centro: this.centro},{headers})
          .map(res => res.json())
          .subscribe(data => {
            resolve(data)
          })
    })
  }
}
