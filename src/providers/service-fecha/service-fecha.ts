// importamos el modulo Injectable de AngularJS
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { NavController, NavParams, LoadingController, Loading, AlertController } from 'ionic-angular';
import { COMPOSITION_BUFFER_MODE } from '@angular/forms';

// import { OrderServicesProvider } from '../providers/order-services/order-services';

// Permitimos que este objeto se pueda inyectar con la DI
@Injectable()
export class ServiceFecha {
    meses: any
    constructor() {
        this.meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre']

    }
    convertirModificaturno_inicio(data) {
        let anio = Number(data.fecha_modifica_turno.slice(0, 4));
        let mes = Number(data.fecha_modifica_turno.slice(5, 7) - 1);
        let dia = Number(data.fecha_modifica_turno.slice(8, 10));
        let hora = Number(data.fecha_modifica_turno.slice(11, 13));
        let minutos = Number(data.fecha_modifica_turno.slice(11, 13));
        let startDate = new Date(anio, mes, dia, hora, minutos, 0, 0);
        return startDate;
    }
    convertirModificaturno_final(data) {
        let anio = Number(data.fecha_modifica_turno_final.slice(0, 4));
        let mes = Number(data.fecha_modifica_turno_final.slice(5, 7) - 1);
        let dia = Number(data.fecha_modifica_turno_final.slice(8, 10));
        let hora = Number(data.fecha_modifica_turno_final.slice(11, 13));
        let minutos = Number(data.fecha_modifica_turno_final.slice(14, 16));
        let endDate = new Date(anio, mes, dia, hora, minutos, 0, 0);
        return endDate;
    }
    convertirhora_turno_inicio(data) {
        let anio = Number(data.hora_turno.slice(0, 4));
        let mes = Number(data.hora_turno.slice(5, 7) - 1);
        let dia = Number(data.hora_turno.slice(8, 10));
        let hora = Number(data.hora_turno.slice(11, 13));
        let minutos = Number(data.hora_turno.slice(14, 16));
        let startDate = new Date(anio, mes, dia, hora, minutos, 0, 0);
        return startDate;
    }
    convertirhora_turno_final(data) {
        let anio = Number(data.hora_final_turno.slice(0, 4));
        let mes = Number(data.hora_final_turno.slice(5, 7) - 1);
        let dia = Number(data.hora_final_turno.slice(8, 10));
        let hora = Number(data.hora_final_turno.slice(11, 13));
        let minutos = Number(data.hora_final_turno.slice(14, 16));
        let endDate = new Date(anio, mes, dia, hora, minutos, 0, 0);
        return endDate;
    }
    getmeshumano(fecha) {
        let mes = Number(fecha.slice(6, 7)) - 1;

        for (let i = 0; i < this.meses.length; i++) {
            const element = this.meses[i];
            if (mes == i) {
                return element;
            }
        }
    }
    getfechahumano(fecha) {
        console.log(fecha)
        var hora = fecha.slice(11, 13);
        var horanueva;
        if (Number(hora) > 12) {
            horanueva = hora - 12 + ':' + fecha.slice(14, 16) + ' pm';
        } else {
            horanueva = hora + ':' + fecha.slice(14, 16) + ' am';
        }
        return fecha.slice(8, 10) + ' de ' + this.getmeshumano(fecha) + ' de ' + fecha.slice(0, 4)+' a las '+horanueva
    }
}