import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Persona } from "../../model/persona";
import { Storage } from "@ionic/storage";
/*
  Generated class for the AuthProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class AuthProvider {

  public data:{email:string, password:string};
  public dataReset: {identification:string, email:string, type_document:string};
  public dataUsuario: Persona;
  private base_url:string;
  public dataBeneficiario: {
    type_document: number,
    identification: string,
    nombres: string,
    apellidos: string,
    parentesco: string,
    afiliado: number,
    email: string,
    celular: string
  };
  public id:string
  private base_url_api:string;
  public change_password: {password:string, id:number}
  public change_phone: {celular: string, id: number}
  constructor(public http: Http, private storage: Storage) {
    this.base_url = "http://metroredapp.hmetro.med.ec/metrored/public/auth/";
    this.base_url_api = "http://metroredapp.hmetro.med.ec/metrored/public/api/";
  }
  login(){
    let headers = new Headers();
    headers.append('Content-Type','application/json');
    return new Promise(resolve => {
      this.http.post(this.base_url+"login", this.data, {headers})
      .map(res => res.json())
      .subscribe(data => {
        if(data.response == true){
            this.storage.clear();
            this.storage.set('session',data.result.session);
            this.storage.set('persona',data.result.persona);
            this.storage.set('dispensario',data.result.dispensario);
        }
        resolve(data)
      })
    })
  }

  UpdatePersonDispensario(){
    let headers = new Headers();
    headers.append('Content-Type','application/json');
    return new Promise(resolve => {
      this.http.post(this.base_url+'updatedispensario',{ id:this.id }, {headers})
      .map(res => res.json())
      .subscribe(data => {
        resolve(data);
      })
    });
  }

  resetPassword(){
    let headers = new Headers();
    headers.append('Content-Type','application/json');
    return new Promise(resolve => {
      this.http.post(this.base_url+'reset',this.dataReset, {headers})
      .map(res => res.json())
      .subscribe(data => {
        resolve(data);
      })
    });
  }

  addUser(){
    let headers = new Headers();
    headers.append('Content-Type','application/json');
    return new Promise(resolve => {
      this.http.post(this.base_url+"registro",this.dataUsuario,{headers})
      .map(res => res.json())
      .subscribe(data => {
        this.storage.clear();
        resolve(data)
      })
    })
  }

  addBeneficiarion(){
    let headers = new Headers();
    headers.append('Content-Type','application/json');
    headers.append('Authorization','Bearer ZXN0ZSBlcyBlbCB0b2tlbiBxdWUgc2UgdmEgYSB1dGlsaXphciBkZSBwcnVlYmE=');
    return new Promise(resolve => {
      this.http.post(this.base_url_api+"local/pacientes/addBeneficiario",this.dataBeneficiario, { headers })
          .map(res => res.json())
          .subscribe(data => {
            resolve(data)
          })
    })
  }

  addTokenPush(token){
    let headers = new Headers();
    headers.append('Content-Type','application/json');
    return new Promise(resolve => {
      this.http.post(this.base_url+"tokenPush",{ token:token }, { headers })
      .map(res => res.json())
      .subscribe(data => {
        resolve(data)
      })
    })
  }

  changePassword(){
    let headers = new Headers();
    headers.append('Content-Type','application/json');
    return new Promise(resolve => {
      this.http.post(this.base_url+"change", this.change_password, {headers})
          .map(res => res.json())
          .subscribe(data => {
            resolve(data)
          })
    })
  }
    //servicio que valida un numero de mas de 8 caracteres 
    validatePhone(id){
      console.log("id:"+id)
      let headers = new Headers();
      headers.append('Content-Type','application/json');
      return new Promise(resolve => {
        this.http.post(this.base_url+"validateTelefono",{id:id}, {headers})
            .map(res => res.json())
            .subscribe(data => {
              resolve(data)
            })
      })
    }

  changePhone(){
    let headers = new Headers();
    headers.append('Content-Type','application/json');
    return new Promise(resolve => {
      this.http.post(this.base_url+"changeCelular", this.change_phone, {headers})
          .map(res => res.json())
          .subscribe(data => {
            resolve(data)
          })
    })
  }
}
