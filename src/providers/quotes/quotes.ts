import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import { Calendario } from "../../model/calenderario";
/*
  Generated class for the QuotesProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class QuotesProvider {
  calendario:Calendario[];
  public document: number;
  private base_url:string;
  public id:string;
  public quote: {id_especialidad:number, id_centro:number}
  public addQuote: {
    turno: number,
    horario: number,
    paciente: number,
    hora_turno: string,
    email: string,
    nombres: string,
    apellidos: string,
    celular: string,
    cedula: string
  }
  public addQuoteLocal:{
    turno: number,
    horario: number,
    paciente: number,
    hora_turno: string,
    hora_final_turno:string,
    paciente_local: number,
    tipo_persona: number,
    locacion: string,
    doctor: string,
    especialidad: string,
    mejor_dia:number,
    estado:number,
    fecha_old:string,
    fecha_old_final:string
    
  }
  public validateTurnData:{
    id: number,
    fecha: string,
    tipo_paciente: number
  }
  public dataDeleteQuote:{
    id_local: number,
    id_horario: number,
    id_turno: number,
    hora_turno: string
  }
  public dataSetMedico : {
    id:number,
    nombre: string,
    fk_cita:number
  }
  public beforeQuote: {
    id: number,
    horario: number,
    turno: number
  }
  public dataSetMedicoFavorito: {
    id: number,
    nombre: string,
    id_medico: number
  }
  public setNotasData: {
    horario: number,
    notas: string,
    turno: number
  }
  public dataMedico:{
    id_especialidad:number, 
    id_centro:number,
    nombre: string
  }
  public deleteBeneficiary:{
    id_paciente: number,
    id_beneficiario: number
  }
  public remoteNote: {
    horario: number,
    turno: number,
    notas: string
  }
  constructor(public http: Http) {
   this.calendario = [
     {nombre: 'Carlos Alberto Granada', duracion:'20 minutos', fecha:'06-JUN-2017', hora:'15:35', id:'123456'},
     {nombre: 'Carlos Alberto Granada1', duracion:'30 minutos', fecha:'07-JUN-2017', hora:'16:35', id:'123456'},
     {nombre: 'Carlos Alberto Granada2', duracion:'30 minutos', fecha:'06-JUN-2017', hora:'18:00', id:'123456'},
     {nombre: 'Carlos Alberto Granada3', duracion:'15 minutos', fecha:'08-JUN-2017', hora:'20:35', id:'123456'},
     {nombre: 'Carlos Alberto Granada4', duracion:'30 minutos', fecha:'06-JUN-2017', hora:'16:35', id:'123456'},
     {nombre: 'Carlos Alberto Granada5', duracion:'45 minutos', fecha:'10-JUN-2017', hora:'12:10', id:'123456'},
   ]
    this.base_url = 'http://metroredapp.hmetro.med.ec/metrored/public/api/'
  }

  getMedicos(){
    return this.calendario;
  }
  getPacienteAfilidos(){
    let headers = new Headers();
    headers.append('Content-Type','application/json');
    headers.append('Accept', 'application/json');
    headers.append('Authorization','Bearer ZXN0ZSBlcyBlbCB0b2tlbiBxdWUgc2UgdmEgYSB1dGlsaXphciBkZSBwcnVlYmE=');
    return new Promise((resolve, reject) => {
      this.http.post(this.base_url+"local/pacientes/getBeneficiarios", {id: this.id}, {headers: headers})
          .map(res => res.json())
          .subscribe(data => {
            resolve(data)
          }, error=>{
            reject(error);
          })
    })
  }


  removeBeneficiary(){
    let headers = new Headers();
    headers.append('Content-Type','application/json');
    headers.append('Accept', 'application/json');
    headers.append('Authorization','Bearer ZXN0ZSBlcyBlbCB0b2tlbiBxdWUgc2UgdmEgYSB1dGlsaXphciBkZSBwcnVlYmE=');
    return new Promise(resolve => {
      this.http.post(this.base_url+"local/pacientes/removeBeneficiario", this.deleteBeneficiary, {headers: headers})
          .map(res => res.json())
          .subscribe(data => {
            resolve(data)
          })
    })
  }
  getQuotes(){
    let headers = new Headers();
    headers.append('Content-Type','application/json');
    headers.append('Accept', 'application/json');
    headers.append('Authorization','Bearer ZXN0ZSBlcyBlbCB0b2tlbiBxdWUgc2UgdmEgYSB1dGlsaXphciBkZSBwcnVlYmE=');
    return new Promise(resolve => {
      this.http.post(this.base_url+"citas/medicos", this.quote ,{headers: headers})
          .map(res => res.json())
          .subscribe(data => {
            resolve(data)
          })
    })
  }

  addQuotes(){
    let headers = new Headers();
    headers.append('Content-Type','application/json');
    headers.append('Accept', 'application/json');
    headers.append('Authorization','Bearer ZXN0ZSBlcyBlbCB0b2tlbiBxdWUgc2UgdmEgYSB1dGlsaXphciBkZSBwcnVlYmE=');
    return new Promise(resolve => {
      this.http.post(this.base_url+'citas/agregar', this.addQuote, {headers})
          .map(res => res.json())
          .subscribe(data => {
            resolve(data)
          })
    })
  }
  addQuotesLocal(){
    let headers = new Headers();
    headers.append('Content-Type','application/json');
    headers.append('Accept', 'application/json');
    headers.append('Authorization','Bearer ZXN0ZSBlcyBlbCB0b2tlbiBxdWUgc2UgdmEgYSB1dGlsaXphciBkZSBwcnVlYmE=');
    return new Promise(resolve => {
      this.http.post(this.base_url+"local/citas/agregar", this.addQuoteLocal, {headers})
          .map(res => res.json())
          .subscribe(data => {
            resolve(data)
          })
    })
  }

  validateTurn(){
    let headers = new Headers();
    console.log("+++++++++++++++++")
    console.log(this.validateTurnData)
    console.log("+++++++++++++++++")
    headers.append('Content-Type','application/json');
    headers.append('Accept', 'application/json');
    headers.append('Authorization','Bearer ZXN0ZSBlcyBlbCB0b2tlbiBxdWUgc2UgdmEgYSB1dGlsaXphciBkZSBwcnVlYmE=');
    return new Promise(resolve => {
      this.http.post(this.base_url+"local/citas/validar/hora", this.validateTurnData, {headers})
          .map(res => res.json())
          .subscribe(data => {
            resolve(data)
          })
    })
  }

  validateDis(horario, turno){
    let headers = new Headers();
    console.log("+++++++++++++++++")
    console.log(this.validateTurnData)
    console.log("+++++++++++++++++")
    headers.append('Content-Type','application/json');
    headers.append('Accept', 'application/json');
    headers.append('Authorization','Bearer ZXN0ZSBlcyBlbCB0b2tlbiBxdWUgc2UgdmEgYSB1dGlsaXphciBkZSBwcnVlYmE=');
    return new Promise(resolve => {
      this.http.post(this.base_url+"citas/validatedis", {horario:horario, turno:turno}, {headers})
          .map(res => res.json())
          .subscribe(data => {
            resolve(data)
          })
    })
  }
  historyQuotes(){
    console.log(this.id);
    let headers = new Headers();
    headers.append('Content-Type','application/json');
    headers.append('Accept', 'application/json');
    headers.append('Authorization','Bearer ZXN0ZSBlcyBlbCB0b2tlbiBxdWUgc2UgdmEgYSB1dGlsaXphciBkZSBwcnVlYmE=');
    return new Promise((resolve, reject) => {
      this.http.post(this.base_url+"local/citas/getCitas",{ id:this.id },{headers})
          .map(res => res.json())
          .subscribe(data => {
            resolve(data)
          }, error=>{
            reject(error)
          })
    })
  }

  UpdateQuotes(){
    console.log(this.id);

    let headers = new Headers();
    headers.append('Content-Type','application/json');
    headers.append('Accept', 'application/json');
    headers.append('Authorization','Bearer ZXN0ZSBlcyBlbCB0b2tlbiBxdWUgc2UgdmEgYSB1dGlsaXphciBkZSBwcnVlYmE=');
    return new Promise((resolve, reject) => {
      this.http.post(this.base_url+"local/citas/getCitasEliminadas",{ id:this.id},{headers}) 
          .map(res => res.json())
          .subscribe(data => {
            resolve(data)
          }, error=>{
            reject(error)
          })
    })
  }
  getHistory(){
    let headers = new Headers();
    headers.append('Content-Type','application/json');
    headers.append('Accept', 'application/json');
    headers.append('Authorization','Bearer ZXN0ZSBlcyBlbCB0b2tlbiBxdWUgc2UgdmEgYSB1dGlsaXphciBkZSBwcnVlYmE=');
    return new Promise((resolve,reject) => {
      this.http.post(this.base_url+"local/citas/getCitasHistorico",{ id:this.id },{headers})
          .map(res => res.json())
          .subscribe(data => {
            resolve(data)
          }, error=>{
            reject(error);
          })
    })
  }
  deleteQuote(){
    let headers = new Headers();
    headers.append('Content-Type','application/json');
    headers.append('Accept', 'application/json');
    headers.append('Authorization','Bearer ZXN0ZSBlcyBlbCB0b2tlbiBxdWUgc2UgdmEgYSB1dGlsaXphciBkZSBwcnVlYmE=');
    return new Promise((resolve, reject) => {
      this.http.post(this.base_url+"local/citas/cancelar", this.dataDeleteQuote, {headers})
          .map(res => res.json())
          .subscribe(data => {
            resolve(data)
          }, error=>{
            reject(error);
          })
    })
  }

  setMedico(){
    let headers = new Headers();
    headers.append('Content-Type','application/json');
    headers.append('Accept', 'application/json');
    headers.append('Authorization','Bearer ZXN0ZSBlcyBlbCB0b2tlbiBxdWUgc2UgdmEgYSB1dGlsaXphciBkZSBwcnVlYmE=');
    return new Promise(resolve => {
      this.http.post(this.base_url+"local/citas/setMedico", this.dataSetMedico, {headers})
          .map(res => res.json())
          .subscribe(data => {
            resolve(data);
          })
    })
  }

  getMedicoFavorito(){
    let headers = new Headers();
    headers.append('Content-Type','application/json');
    headers.append('Accept', 'application/json');
    headers.append('Authorization','Bearer ZXN0ZSBlcyBlbCB0b2tlbiBxdWUgc2UgdmEgYSB1dGlsaXphciBkZSBwcnVlYmE=');
    return new Promise(resolve => {
      this.http.post(this.base_url+"local/citas/getMedico", this.dataSetMedico, {headers})
          .map(res => res.json())
          .subscribe(data => {
            resolve(data);
          })
    })
  }

  getAllfavoriteDoctors(){
    let headers = new Headers();
    headers.append('Content-Type','application/json');
    headers.append('Accept', 'application/json');
    headers.append('Authorization','Bearer ZXN0ZSBlcyBlbCB0b2tlbiBxdWUgc2UgdmEgYSB1dGlsaXphciBkZSBwcnVlYmE=');
    return new Promise(resolve => {
      this.http.post(this.base_url+"local/citas/getAllMedico", this.dataSetMedico, {headers})
          .map(res => res.json())
          .subscribe(data => {
            resolve(data);
          })
    })
  }

  getQuoteDoctorId(){
    let headers = new Headers();
    headers.append('Content-Type','application/json');
    headers.append('Accept', 'application/json');
    headers.append('Authorization','Bearer ZXN0ZSBlcyBlbCB0b2tlbiBxdWUgc2UgdmEgYSB1dGlsaXphciBkZSBwcnVlYmE=');
    return new Promise((resolve, reject) => {
      this.http.post(this.base_url+"citas/getCitas", this.dataSetMedico, {headers})
          .map(res => res.json())
          .subscribe(data => {
            resolve(data)
          }, error =>{
            reject("error")
          })
    })
  }

  getQuoteDoctorFavorite(){
    let headers = new Headers();
    headers.append('Content-Type','application/json');
    headers.append('Accept', 'application/json');
    headers.append('Authorization','Bearer ZXN0ZSBlcyBlbCB0b2tlbiBxdWUgc2UgdmEgYSB1dGlsaXphciBkZSBwcnVlYmE=');
    return new Promise(resolve => {
      this.http.post(this.base_url+"citas/getCitasMedicoFavorito", this.dataSetMedico, {headers})
          .map(res => res.json())
          .subscribe(data => {
            resolve(data)
          })
    })
  }

  rescheduleQuote(){
    let headers = new Headers();
    headers.append('Content-Type','application/json');
    headers.append('Accept', 'application/json');
    headers.append('Authorization','Bearer ZXN0ZSBlcyBlbCB0b2tlbiBxdWUgc2UgdmEgYSB1dGlsaXphciBkZSBwcnVlYmE=');
    return new Promise(resolve => {
      this.http.post(this.base_url+"local/citas/reprogramarCita", this.beforeQuote, {headers})
          .map(res => res.json())
          .subscribe(data => {
            resolve(data)
          })
    })
  }

  setMedicoFavorito(){
    let headers = new Headers();
    headers.append('Content-Type','application/json');
    headers.append('Accept', 'application/json');
    headers.append('Authorization','Bearer ZXN0ZSBlcyBlbCB0b2tlbiBxdWUgc2UgdmEgYSB1dGlsaXphciBkZSBwcnVlYmE=');
    return new Promise(resolve => {
      this.http.post(this.base_url+"local/citas/setMedicoFavorito", this.dataSetMedicoFavorito, {headers})
          .map(res => res.json())
          .subscribe(data => {
            resolve(data)
          })
    })
  }
  setNotas(){
    let headers = new Headers();
    headers.append('Content-Type','application/json');
    headers.append('Accept', 'application/json');
    headers.append('Authorization','Bearer ZXN0ZSBlcyBlbCB0b2tlbiBxdWUgc2UgdmEgYSB1dGlsaXphciBkZSBwcnVlYmE=');
    return new Promise(resolve => {
      this.http.post(this.base_url+"local/citas/setNotas", this.setNotasData, {headers})
          .map(res => res.json())
          .subscribe(data => {
            resolve(data)
          })
    })
  }
  setNotas2(){
    let headers = new Headers();
    headers.append('Content-Type','application/json');
    headers.append('Accept', 'application/json');
    headers.append('Authorization','Bearer ZXN0ZSBlcyBlbCB0b2tlbiBxdWUgc2UgdmEgYSB1dGlsaXphciBkZSBwcnVlYmE=');
    return new Promise(resolve => {
      this.http.post(this.base_url+"local/citas/setNotas2", this.setNotasData, {headers})
          .map(res => res.json())
          .subscribe(data => {
            resolve(data)
          })
    })
  }
  historyQuotesdispensary(){
    let headers = new Headers();
    headers.append('Content-Type','application/json');
    headers.append('Accept', 'application/json');
    headers.append('Authorization','Bearer ZXN0ZSBlcyBlbCB0b2tlbiBxdWUgc2UgdmEgYSB1dGlsaXphciBkZSBwcnVlYmE=');
    return new Promise(resolve => {
      this.http.post(this.base_url+"local/citas/getCitasDispensario",{ id:this.id },{headers})
          .map(res => res.json())
          .subscribe(data => {
            resolve(data)
          })
    })
  }
  getDoctorQuote(){
    let headers = new Headers();
    headers.append('Content-Type','application/json');
    headers.append('Accept', 'application/json');
    headers.append('Authorization','Bearer ZXN0ZSBlcyBlbCB0b2tlbiBxdWUgc2UgdmEgYSB1dGlsaXphciBkZSBwcnVlYmE=');
    return new Promise((resolve, reject) => {
      this.http.post(this.base_url+"getMedicos",this.quote,{headers})
          .map(res => res.json())
          .subscribe(data => {
            resolve(data)
          }, error=>{
            reject(error);
          })
    })
  }

  getMedicoName(){
    let headers = new Headers();
    headers.append('Content-Type','application/json');
    headers.append('Accept', 'application/json');
    headers.append('Authorization','Bearer ZXN0ZSBlcyBlbCB0b2tlbiBxdWUgc2UgdmEgYSB1dGlsaXphciBkZSBwcnVlYmE=');
    return new Promise(resolve => {
      this.http.post(this.base_url+"getMedicosByName",this.dataMedico,{headers})
          .map(res => res.json())
          .subscribe(data => {
            resolve(data)
          })
    })
  }

  setRemoteNote(){
    let headers = new Headers();
    headers.append('Content-Type','application/json');
    headers.append('Accept', 'application/json');
    headers.append('Authorization','Bearer ZXN0ZSBlcyBlbCB0b2tlbiBxdWUgc2UgdmEgYSB1dGlsaXphciBkZSBwcnVlYmE=');
    return new Promise(resolve => {
      this.http.post(this.base_url+"citas/setNotas",this.remoteNote,{headers})
          .map(res => res.json())
          .subscribe(data => {
            resolve(data)
          })
    })
  }
}
