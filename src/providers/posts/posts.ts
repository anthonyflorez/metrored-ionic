import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the PostsProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class PostsProvider {
  private base_url:string;
  public id: number
  constructor(public http: Http) {
    this.base_url = 'http://metroredapp.hmetro.med.ec/metrored/public/api/local/posts/'
  }

  listPosts(){
    let headers = new Headers();
    headers.append('Content-Type','application/json');
    headers.append('Accept', 'application/json');
    headers.append('Authorization','Bearer ZXN0ZSBlcyBlbCB0b2tlbiBxdWUgc2UgdmEgYSB1dGlsaXphciBkZSBwcnVlYmE=');
    return new Promise(resolve => {
      this.http.get(this.base_url+"listar/articulos",{ headers: headers })
          .map(res => res.json())
          .subscribe(data => {
            resolve(data.result)
          })
    })
  }
   listEspecialidades(){
    let headers = new Headers();
    headers.append('Content-Type','application/json');
    headers.append('Accept', 'application/json');
    headers.append('Authorization','Bearer ZXN0ZSBlcyBlbCB0b2tlbiBxdWUgc2UgdmEgYSB1dGlsaXphciBkZSBwcnVlYmE=');
    return new Promise(resolve => {
      this.http.get(this.base_url+"listar/especialidades/"+this.id,{ headers: headers })
          .map(res => res.json())
          .subscribe(data => {
            resolve(data.result)
          })
    })
  }
  listCenters(){
    let headers = new Headers();
    headers.append('Content-Type','application/json');
    headers.append('Accept', 'application/json');
    headers.append('Authorization','Bearer ZXN0ZSBlcyBlbCB0b2tlbiBxdWUgc2UgdmEgYSB1dGlsaXphciBkZSBwcnVlYmE=');
    return new Promise(resolve => {
      this.http.get(this.base_url+"listar/centros", {headers})
          .map(res => res.json())
          .subscribe(data => {
            resolve(data.result)
          })
    })
  }

  listUniquePost(id){
    let headers = new Headers();
    headers.append('Content-Type','application/json');
    headers.append('Accept', 'application/json');
    headers.append('Authorization','Bearer ZXN0ZSBlcyBlbCB0b2tlbiBxdWUgc2UgdmEgYSB1dGlsaXphciBkZSBwcnVlYmE=');
    return new Promise(resolve => {
      this.http.get(this.base_url+id, {headers})
          .map(res => res.json())
          .subscribe(data => {
            resolve(data.result)
          })
    })
  }

  listPromotions(){
    let headers = new Headers();
    headers.append('Content-Type','application/json');
    headers.append('Accept', 'application/json');
    headers.append('Authorization','Bearer ZXN0ZSBlcyBlbCB0b2tlbiBxdWUgc2UgdmEgYSB1dGlsaXphciBkZSBwcnVlYmE=');
    return new Promise(resolve => {
      this.http.get(this.base_url+"listar/promociones", {headers})
          .map(res => res.json())
          .subscribe(data => {
            resolve(data.result)
          })
    })
  }
}
