import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the QuotesDispensaryProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class QuotesDispensaryProvider {

  public id;
  public dispesary :{
    id:number,
    nombre: string
  }
  private base_url:string;
  constructor(public http: Http) {
    this.base_url = 'http://metroredapp.hmetro.med.ec/metrored/public/api/'
  }
  
  listDispensary(){
    let headers = new Headers();
    headers.append('Content-Type','application/json');
    headers.append('Accept', 'application/json');
    headers.append('Authorization','Bearer ZXN0ZSBlcyBlbCB0b2tlbiBxdWUgc2UgdmEgYSB1dGlsaXphciBkZSBwcnVlYmE=');
    return new Promise(resolve => {
      this.http.post(this.base_url+"local/dispensario/listar", {id: this.id}, {headers: headers})
          .map(res => res.json())
          .subscribe(data => {
            resolve(data)
          })
    })
  }
  listDoctorDispensary(){
    let headers = new Headers();
    headers.append('Content-Type','application/json');
    headers.append('Accept', 'application/json');
    headers.append('Authorization','Bearer ZXN0ZSBlcyBlbCB0b2tlbiBxdWUgc2UgdmEgYSB1dGlsaXphciBkZSBwcnVlYmE=');
    return new Promise(resolve => {
      this.http.post(this.base_url+"local/dispensario/listarMedicos", {id: this.id}, {headers: headers})
          .map(res => res.json())
          .subscribe(data => {
            resolve(data)
          })
    })
  }
  listDoctorDispensaryId(){
    let headers = new Headers();
    headers.append('Content-Type','application/json');
    headers.append('Accept', 'application/json');
    headers.append('Authorization','Bearer ZXN0ZSBlcyBlbCB0b2tlbiBxdWUgc2UgdmEgYSB1dGlsaXphciBkZSBwcnVlYmE=');
    return new Promise(resolve => {
      this.http.post(this.base_url+"local/dispensario/listarDispensarioId", this.dispesary, {headers: headers})
          .map(res => res.json())
          .subscribe(data => {
            resolve(data)
          })
    })
  }
  listDoctorDispensaryDoctorId(){
    let headers = new Headers();
    headers.append('Content-Type','application/json');
    headers.append('Accept', 'application/json');
    headers.append('Authorization','Bearer ZXN0ZSBlcyBlbCB0b2tlbiBxdWUgc2UgdmEgYSB1dGlsaXphciBkZSBwcnVlYmE=');
    return new Promise(resolve => {
      this.http.post(this.base_url+"local/dispensario/listarMedicosId", {id: this.id}, {headers: headers})
          .map(res => res.json())
          .subscribe(data => {
            resolve(data)
          })
    })
  }
}
