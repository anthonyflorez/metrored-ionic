import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Especialidades } from "../../model/especialidades";

/*
  Generated class for the SpecialtiesProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class SpecialtiesProvider {

  especialidades: Especialidades[];

  constructor(public http: Http) {
    this.especialidades = [
      {id:1, imagen:'http://www.alexanderczech.com/wp-content/uploads/2016/05/medicina_general.jpg', texto: 'esto es medician general', titulo: 'Medicina general'},
      {id:2, imagen:'https://www.definicionabc.com/wp-content/uploads/2014/10/medicina-interna.jpg', texto: 'esto es medician interna', titulo: 'Medicina interna'},
      {id:3, imagen:'http://www.suizalab.com/imagenes/especialidades-medicas-suiza-lab/pediatria.jpg', texto: 'esto es pediatria', titulo: 'pediatria'}
    ]
  }
  getSpecialties(){
    return this.especialidades;
  }
}
