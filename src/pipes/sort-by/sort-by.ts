import { Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the SortByPipe pipe.
 *
 * See https://angular.io/docs/ts/latest/guide/pipes.html for more info on
 * Angular Pipes.
 */
@Pipe({
  name: 'sort',
  pure: false
})
export class SortByPipe implements PipeTransform {
  static _orderByComparator(a:any, b:any):number{
      if((isNaN(parseFloat(a)) || !isFinite(a)) || (isNaN(parseFloat(b)) || !isFinite(b))){
        if(a.toLowerCase() < b.toLowerCase()) return -1;
        if(a.toLowerCase() > b.toLowerCase()) return 1;
      }
      else{
        if(parseFloat(a) < parseFloat(b)) return -1;
        if(parseFloat(a) > parseFloat(b)) return 1;
      }
      return 0;
  }
 transform(array: any, [config = '+']) {
    if(!Array.isArray(array)) return array;
    if(!Array.isArray(config) || (Array.isArray(config) && config.length == 1)){
      let propertyToCheck:string = !Array.isArray(config) ? config : config[0];
      let desc = propertyToCheck.substr(0, 1) == '-';
      if(!propertyToCheck || propertyToCheck == '-' || propertyToCheck == '+'){
          return !desc ? array.sort() : array.sort().reverse();
      }
      else{
        let property:string = propertyToCheck.substr(0, 1) == '+' || propertyToCheck.substr(0, 1) == '-'
            ? propertyToCheck.substr(1)
            : propertyToCheck;

        return array.sort(function(a:any,b:any){
            return !desc 
                ? SortByPipe._orderByComparator(a[property], b[property]) 
                : -SortByPipe._orderByComparator(a[property], b[property]);
        });
      }
    }
    else{
       return array.sort(function(a:any,b:any){
          for(var i:number = 0; i < config.length; i++){
              var desc = config[i].substr(0, 1) == '-';
              var property = config[i].substr(0, 1) == '+' || config[i].substr(0, 1) == '-'
                  ? config[i].substr(1)
                  : config[i];

              var comparison = !desc 
                  ? SortByPipe._orderByComparator(a[property], b[property]) 
                  : -SortByPipe._orderByComparator(a[property], b[property]);
              if(comparison != 0) return comparison;
          }

          return 0; 
      });
    }
  }
}
