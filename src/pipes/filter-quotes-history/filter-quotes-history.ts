import { Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the FilterQuotesHistoryPipe pipe.
 *
 * See https://angular.io/docs/ts/latest/guide/pipes.html for more info on
 * Angular Pipes.
 */
@Pipe({
  name: 'filterQuotesHistory',
  pure: false
})
export class FilterQuotesHistoryPipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  transform(value , keys: string, term: string) {

    if (!term || term == "0") return value;
    return value.filter((item) => {
      if(item.documento == term || item.cedula == term){
        return item;
      }
    })
  }
}
