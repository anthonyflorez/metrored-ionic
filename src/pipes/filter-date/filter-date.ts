import { Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the FilterDatePipe pipe.
 *
 * See https://angular.io/docs/ts/latest/guide/pipes.html for more info on
 * Angular Pipes.
 */
@Pipe({
  name: 'filterDate',
  pure: false
})
export class FilterDatePipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  transform(value, keys: string, term: string) {
    if (!term) return value;
    return value.filter((item) =>{
      return item.indexOf(term) !== -1
    });
  }

  validateTime(time){
    
  }
}
