import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Storage } from "@ionic/storage";
import { HomePage } from "../home/home";
import { MenuQuotesPage } from "../quotes/menu-quotes/menu-quotes";
import { MenuSpecialtiesPage } from "../specialties/menu-specialties";
import { FavoriteDoctorsPage } from "../favorite-doctors/favorite-doctors";
import { MoreInfoDoctorPage } from "../more-info-doctor/more-info-doctor";
import { MenuQuotesDispensaryPage } from "../quotes-dispensary/menu-quotes-dispensary/menu-quotes-dispensary";
/**
 * Generated class for the ArticlePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@Component({
  selector: 'page-article',
  templateUrl: 'article.html',
})
export class ArticlePage {

  article:any
  doctors:any
  visibleDispensary: boolean = false
  constructor(public navCtrl: NavController, public navParams: NavParams, private storage: Storage) {
    storage.get('dispensario').then(data => {
      if(data == true){
        this.visibleDispensary = true
      }
    });
    this.article = navParams.data
    if('doctores' in this.article){
      this.doctors = this.article.doctores;
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ArticlePage');
  }
  goToHome(){
    this.navCtrl.setRoot(HomePage);
  }
  goToQuotes(){
    this.navCtrl.setRoot(MenuQuotesPage)
  }
  goToSpecialite(){
    this.navCtrl.setRoot(MenuSpecialtiesPage)
  }
  goToFavorite(){
        this.navCtrl.setRoot(FavoriteDoctorsPage)
  }
  gotoQuotesDispensary(){
    this.navCtrl.setRoot(MenuQuotesDispensaryPage);
  }
  moreInfo(doctor){
    this.navCtrl.push(MoreInfoDoctorPage,doctor)
  }
}
