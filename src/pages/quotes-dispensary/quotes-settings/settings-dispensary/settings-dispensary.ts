import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, Loading, AlertController } from 'ionic-angular';
import { QuotesDispensaryProvider } from "../../../../providers/quotes-dispensary/quotes-dispensary"
import { Storage } from "@ionic/storage";
@Component({
    selector:'page-settings-dispensary',
    templateUrl: 'settings-dispensary.html',
    providers: [QuotesDispensaryProvider]
})
export class SettingDispensaryPage{
    reponse:any;
    especialidades:any;
    loading: Loading;
    private navRequest:any
    constructor(public navCtrl: NavController, public navParams: NavParams, private quoteDispensaryProvider: QuotesDispensaryProvider, private loadingCtrl: LoadingController,
                private alertCtrl:AlertController, private storage:Storage){
        this.load();
        this.navRequest = navParams.data;
    }


    load(){
        this.showLoading();
        this.storage.get('persona').then(data => 
        {
            this.quoteDispensaryProvider.id = data.id
            this.quoteDispensaryProvider.listDispensary().then(dataR => {
                this.reponse = dataR;
                this.especialidades = this.reponse.result;
                this.loading.dismiss();
            })
        })
    }
    showLoading(){
      this.loading = this.loadingCtrl.create({
          content: "Cargando información\n espere por favor...",
          dismissOnPageChange: false
      })
      this.loading.present()
    }

    showError(text:string){
        let alert = this.alertCtrl.create({
            title: "Error",
            subTitle: text,
            buttons:['OK']
        })
        alert.present(prompt)
    }

    addEspecialidad(especialidad){
        this.navRequest.descripcion_especialidad = especialidad.ob_hm_dispensario;
        this.navRequest.id_especialidad = especialidad.id;
        this.navRequest.disabled_centro = true;
        this.navCtrl.pop()
    }
}