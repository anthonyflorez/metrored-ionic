import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, Loading, AlertController } from 'ionic-angular';
import { QuotesDispensaryProvider } from "../../../../providers/quotes-dispensary/quotes-dispensary";
@Component({
    selector: 'page-settings-doctor',
    templateUrl: 'settings-doctor.html',
    providers: [QuotesDispensaryProvider]
})
export class SettingsDoctorPage{
    date:string;
    medicos:any;
    medicosResult:any;
    keys:any;
    navRequest:any;
    loading:Loading;
    constructor(public navCtrl: NavController, public navParams: NavParams, public quoteDispensaryProvider: QuotesDispensaryProvider, private loadingCtrl: LoadingController,
                private alertCtrl: AlertController){
        this.navRequest = navParams.data;
        this.load()
    }
    load(){
        this.showLoading()
        this.quoteDispensaryProvider.id = this.navRequest.id_especialidad
        this.quoteDispensaryProvider.listDoctorDispensary().then(data => {
            this.medicos = data;
            this.medicos = this.medicos.result;
            this.keys = Object.keys(this.medicos);
            this.loading.dismiss()
        })
    }
    validateDuracion(time){
        let slice = time.split(' ');
        let timeSlide = slice[1].split(':');
        let returnTime;
        if(timeSlide[0] > 0){
            returnTime = timeSlide[0]+":"+timeSlide[1]+" Hora";
        }
        else{
            returnTime = timeSlide[1]+" Minutos";
        }
        return returnTime;
    }

    setDoctor(doctor){
        this.navRequest.descripcion_centro = doctor.ob_hm_nombre_medico;
        this.navRequest.id_centro = doctor.ob_hm_id_medico;
        this.navRequest.disabled_doctor = true;
        this.navCtrl.pop()
    }
    showLoading(){
      this.loading = this.loadingCtrl.create({
          content: "Cargando información\n espere por favor...",
          dismissOnPageChange: false
      })
      this.loading.present()
    }

    showError(text:string){
        let alert = this.alertCtrl.create({
            title: "Error",
            subTitle: text,
            buttons:['OK']
        })
        alert.present(prompt)
    }
}