import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, Loading, AlertController, ModalController } from 'ionic-angular';
import { SettingDispensaryPage } from "./settings-dispensary/settings-dispensary";
import { SettingsDoctorPage } from "./settings-doctor/settings-doctor";
import { SettingsQuotesPage } from "./settings-quote/settings-quotes";
import { MenuQuotesPage } from "../../quotes/menu-quotes/menu-quotes";
import { Storage } from "@ionic/storage";
import { ModalSelectPage } from "../../modal-select/modal-select";
import { HomePage } from "../../home/home";
import { MenuSpecialtiesPage } from "../../specialties/menu-specialties";
import { FavoriteDoctorsPage } from "../../favorite-doctors/favorite-doctors";
import { Calendar } from '@ionic-native/calendar';
import { QuotesDispensaryProvider } from "../../../providers/quotes-dispensary/quotes-dispensary";
import { QuotesProvider } from "../../../providers/quotes/quotes";
import { QuoteDispensaryPage } from "../quote/quote-dispensary";
@Component({
    selector: 'page-quotes-dispensary',
    templateUrl: 'quotes-dispensary.html',
    providers: [QuotesDispensaryProvider, QuotesProvider]
})
export class QuotesDispensaryPage{

    loading: Loading;
    response: any;
    public navRequest:any;
    public cita: {
       descripcion_especialidad:string,
       id_especialidad:number,
       descripcion_centro:string,
       disabled_centro: boolean,
       id_centro:number,
       disabled_doctor:boolean,
       doctor: any,
       nombre_doctor:string,
       disabled_reserva:boolean,
       disable_mejor_cita: boolean
    };
    responseValidateTurn:any;
    responseLocal:any;
    bestDate: boolean = false
    full_name: string = null
    constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController,
                public alertCtrl: AlertController, private quotesDispensaryProvider: QuotesDispensaryProvider, private storage: Storage, public modalCtrl: ModalController,
                public calendar: Calendar, private quotesProvider: QuotesProvider){
        this.cita = {
            id_especialidad: 0, 
            descripcion_especialidad: 'Dispensario',
            descripcion_centro: 'Doctor',
            disabled_centro: false,
            id_centro: 0,
            disabled_doctor: false,
            doctor: [],
            nombre_doctor: 'Cita',
            disabled_reserva: false,
            disable_mejor_cita: false
        }
        this.storage.get('persona').then(data => {
            this.full_name = data.nombres + " " + data.apellidos
        })
        // this.navRequest = this.navParams.data;
        // if(this.navRequest.paciente.estado != 1){
        //     this.showError('El usuario esta bloqueado o su afiliación esta inactiva.');
        //     this.navCtrl.pop()
        // }
    }
    cancelarCita(datosCitas){
        console.log(this.navRequest);
        this.quotesProvider.dataDeleteQuote = {
            hora_turno: this.navRequest.hora_turno,
            id_horario: this.navRequest.hm_fk_horario,
            id_local: this.navRequest.id,
            id_turno: this.navRequest.numero_turno
            
        }
        this.showLoading()
        this.quotesProvider.deleteQuote().then(data => {
            this.response = data;
            if(this.response.response == true){
                    this.loading.dismiss();
                    this.showError(this.response.result, "Exito");
                    // this.calendar.deleteEvent(
                    //     'Cita Medica', this.cita.descripcion_centro+",ecuador","Lugar: "+this.cita.descripcion_centro+"\n"+"Medico: "+this.cita.nombre_doctor+"\n"+"Hora: "+this.cita.doctor.turno.FECHA_HORA,
                    //     new Date(this.cita.doctor.medico.HORA_INICIO_TURNO.split(' ')[0]+" "+this.cita.doctor.turno.FECHA_HORA.split(' ')[1])
                    // );
                    this.navCtrl.pop()
            }
            else{
                // this.calendar.deleteEvent(
                //     'Cita Medica', this.cita.descripcion_centro+",ecuador","Lugar: "+this.cita.descripcion_centro+"\n"+"Medico: "+this.cita.nombre_doctor+"\n"+"Hora: "+this.cita.doctor.turno.FECHA_HORA,
                //     new Date(this.cita.doctor.medico.HORA_INICIO_TURNO.split(' ')[0]+" "+this.cita.doctor.turno.FECHA_HORA.split(' ')[1])
                // );
                this.loading.dismiss()
                this.showError(this.response.result, "Error");
            }
        })
    }
    openModal(){
        let modal = this.modalCtrl.create(ModalSelectPage)
        modal.onDidDismiss(data => {
            console.log(data);
        })
        modal.present()
    }
    goToCentro(){
        this.navCtrl.push(SettingDispensaryPage, this.cita)
    }
    goToQuote(){
        this.storage.get('persona').then(data => {
            this.quotesProvider.addQuote = {
                apellidos:data.apellidos,
                celular: (typeof data.celular == 'undefined')? 0 : data.celular,
                email: data.email,
                hora_turno: this.cita.doctor.turno.FECHA_HORA,
                horario: this.cita.doctor.medico.CODIGO_HORARIO,
                nombres: data.nombres,
                paciente: data.hm_fk_persona,
                turno: this.cita.doctor.turno.NUMERO_TURNO,
                cedula: (typeof  data.documento == "undefined")? data.cedula : data.documento
            }
            this.quotesProvider.addQuoteLocal = {
                paciente: data.hm_fk_persona,
                hora_turno: this.cita.doctor.medico.HORA_INICIO_TURNO.split(' ')[0]+" "+this.cita.doctor.turno.FECHA_HORA.split(' ')[1],
                hora_final_turno: this.cita.doctor.medico.FECHA_FINAL_TURNO,
                horario: this.cita.doctor.medico.CODIGO_HORARIO,
                paciente_local: data.id,
                tipo_persona: 1,
                turno: this.cita.doctor.turno.NUMERO_TURNO,
                locacion:this.cita.descripcion_centro,
                doctor: this.cita.nombre_doctor,
                especialidad: this.cita.descripcion_especialidad,
                mejor_dia: 0,
                estado: 2,
                fecha_old: "",
                fecha_old_final:""

            }
            this.quotesProvider.validateTurnData = {
                id: data.id,
                fecha: this.cita.doctor.medico.HORA_INICIO_TURNO.split(' ')[0]+" "+this.cita.doctor.turno.FECHA_HORA.split(' ')[1],
                tipo_paciente: 1
            }
            let send_data = {
                apellidos: data.apellidos,
                doctor : this.cita.nombre_doctor,
                especialidad: this.cita.descripcion_especialidad,
                hora_turno: this.cita.doctor.medico.HORA_INICIO_TURNO.split(' ')[0]+" "+this.cita.doctor.turno.FECHA_HORA.split(' ')[1],
                locacion: this.cita.descripcion_centro,
                nombres: data.nombres,
                mejor_fecha: 0
            }
            this.showLoading()
            this.navRequest.fecha=new Date(this.cita.doctor.medico.HORA_INICIO_TURNO.split(' ')[0]+" "+this.cita.doctor.turno.FECHA_HORA.split(' ')[1]);
            this.quotesProvider.validateTurn().then(dataV => {
                this.responseValidateTurn = dataV;
                if(this.responseValidateTurn.response != false){
                    this.quotesProvider.addQuotesLocal().then(dataL => {
                        this.responseLocal = dataL;
                        if(this.responseLocal.reponse != false){
                            this.quotesProvider.addQuotes().then(dataR => {
                                this.response = dataR;
                                if(this.response.response == true){
                                    this.loading.dismiss();
                                    let send = {datos: send_data, vista: 1}
                                    this.calendar.hasReadWritePermission().then(data => {
                                        if(data == true){
                                            console.log(this.cita);
                                            // this.calendar.createEvent(
                                            //     'Cita Medica', this.cita.descripcion_centro+",ecuador","Lugar: "+this.cita.descripcion_centro+"\n"+"Medico: "+this.cita.nombre_doctor+"\n"+"Hora: "+this.cita.doctor.turno.FECHA_HORA,
                                            //     this.navRequest.fecha
                                            // );
                                        }else{
                                            // this.calendar.requestReadWritePermission();
                                            // this.calendar.createEvent(
                                            //     'Cita Medica', this.cita.descripcion_centro+",ecuador","Lugar: "+this.cita.descripcion_centro+"\n"+"Medico: "+this.cita.nombre_doctor+"\n"+"Hora: "+this.cita.doctor.turno.FECHA_HORA,
                                            //     new Date(this.cita.doctor.medico.HORA_INICIO_TURNO.split(' ')[0]+" "+this.cita.doctor.turno.FECHA_HORA.split(' ')[1])
                                            // );
                                        }
                                    })
                                    this.navCtrl.setRoot(QuoteDispensaryPage, send)
                                }else{
                                    this.loading.dismiss();
                                    this.showError(this.response.result,"Error");
                                }
                            })
                        }else{
                            this.loading.dismiss();
                            this.showError(this.responseLocal.result,"Error");
                            this.cita.disabled_reserva = false;
                        }
                    })
                }else{
                    this.loading.dismiss();
                    this.showError(this.responseValidateTurn.result, "Error");
                    this.cita.disabled_reserva = false;
                }
            });
        })
    }
    goToDispensary(){
        this.navCtrl.push(SettingDispensaryPage, this.cita)
    }
    goToDoctor(){
        this.navCtrl.push(SettingsDoctorPage, this.cita)
    }
    showLoading(){
      this.loading = this.loadingCtrl.create({
          content: "Registrando Cita por favor espere ...",
          dismissOnPageChange: false
      })
      this.loading.present()
    }
    showError(text:string, title:string){
        let alert = this.alertCtrl.create({
            title: title,
            subTitle: text,
            buttons:['OK']
        })
        alert.present(prompt)
    }
    goToHome(){
        this.navCtrl.setRoot(HomePage)
    }
    goToQuoteDoctor(){
        this.navCtrl.push(SettingsQuotesPage, this.cita)
    }
    goToQuotes(){
        this.navCtrl.setRoot(MenuQuotesPage)
    }
    goToSpecialite(){
        this.navCtrl.setRoot(MenuSpecialtiesPage)
    }
    goToFavorite(){
        this.navCtrl.setRoot(FavoriteDoctorsPage)
    }
    goToDispensaryPage(){
        this.navCtrl.push(QuotesDispensaryPage)
    }
}
