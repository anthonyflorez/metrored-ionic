import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, Loading, AlertController } from 'ionic-angular';
import { QuotesProvider } from "../../../../providers/quotes/quotes";
@Component({
    selector: 'page-settings-quotes',
    templateUrl: 'settings-quotes.html',
    providers: [QuotesProvider],
})
export class SettingsQuotesPage{
    loading:Loading;
    private navRequest:any
    medicos: any
    keys: any
    title: string
    viewTitle: boolean = false
    viewContent :boolean = false
    constructor(public navCtrl: NavController, public navParams: NavParams, 
                private loadingCtrl:LoadingController, private alertCtrl:AlertController, private quoteProvider: QuotesProvider){
        this.navRequest = navParams.data;
        this.load();
    }

    load(){
        console.log(this.navRequest);
        this.quoteProvider.dataSetMedico = {
            nombre: '',
            //id: 803,
            id:  this.navRequest.id_centro,
            fk_cita: 0

        }
        this.quoteProvider.getQuoteDoctorId().then(data => {
            this.medicos = data;
            this.medicos = this.medicos.result;
            if(typeof this.medicos == "string")
            {
                this.title = this.medicos
                this.viewTitle = true;
            }
            else{
                this.keys = Object.keys(this.medicos);   
                this.viewContent = true
            }
            this.loading.dismiss()
        })
    }
    validateDuracion(time){
        let slice = time.split(' ');
        let timeSlide = slice[1].split(':');
        let returnTime;
        if(timeSlide[0] > 0){
            returnTime = timeSlide[0]+":"+timeSlide[1]+" Hora";
        }
        else{
            returnTime = timeSlide[1]+" Minutos";
        }
        return returnTime;
    }
    setDoctor(doctor){
        this.navRequest.fecha_old=doctor.medico.turno.FECHA_HORA;
        this.navRequest.nombre_doctor = doctor.medico.NOMBRE_MEDICO;
        this.navRequest.doctor = doctor;
        this.navRequest.disabled_reserva = true;
        this.navCtrl.pop()
        
    }
    showLoading(){
      this.loading = this.loadingCtrl.create({
          content: "Cargando información\n espere por favor...",
          dismissOnPageChange: false
      })
      this.loading.present()
    }

    showError(text:string){
        let alert = this.alertCtrl.create({
            title: "Error",
            subTitle: text,
            buttons:['OK']
        })
        alert.present(prompt)
    }
}