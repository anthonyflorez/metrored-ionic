import { Component } from '@angular/core';
import { NavController, NavParams, ModalController, LoadingController, Loading } from 'ionic-angular';
import { QuotesProvider } from "../../../providers/quotes/quotes";
import { Storage } from "@ionic/storage";
// import { QuotePage } from "../quote/quote";
import { ModalSelectPage } from "../../modal-select/modal-select";
// import { MenuQuotesPage } from "../menu-quotes/menu-quotes";
import { MenuSpecialtiesPage } from "../../specialties/menu-specialties";
import { HomePage } from "../../home/home";
import { FavoriteDoctorsPage } from "../../favorite-doctors/favorite-doctors";
import { QuotesDispensaryPage } from "../../quotes-dispensary/quotes-settings/quotes-dispensary";
import { QuoteDispensaryPage } from "../quote/quote-dispensary";
@Component({
    selector: 'page-search-quotes-dispensary',
    templateUrl: 'search-quotes-dispensary.html',
    providers: [QuotesProvider]
})
export class SearchQuotesDispensaryPage{
    citas:any
    paciente: any;
    afiliado:any
    beneficiarios:any
    response:any
    keys: any
    nombre_filtro: string
    loading: Loading
    message_none: string
    visibleDispensary: boolean = false
    constructor(public navCtrl: NavController, public navParams: NavParams, public quotesProvider: QuotesProvider, private storage: Storage, 
                public modalCtrl:ModalController, public loadingCtrl: LoadingController){
        storage.get('dispensario').then(data => {
          if(data == true){
            this.visibleDispensary = true
          }
        });
        this.paciente = "0";
        this.nombre_filtro = "Filtrar por..."
    }
    ionViewWillEnter(){
        this.loadSelect()
    }
    loadSelect(){
        this.showLoading()
        this.storage.get('persona').then((data) => {
            this.afiliado = data;
            this.quotesProvider.id = data.id;
            this.quotesProvider.getPacienteAfilidos().then(data => {
                this.citas = data
                this.beneficiarios = this.citas.result;
            })
            this.quotesProvider.historyQuotesdispensary().then(dataR => {
                this.response = dataR;
                if(typeof this.response.result != 'string'){
                   this.response = this.response.result;
                   this.keys = Object.keys(this.response);
                }
                else{
                    this.message_none = this.response.result
                }
                this.loading.dismiss();
            })
        })
    }
    goToDetails(detail, tipo){
        let send = {datos: detail, vista: tipo}
        this.navCtrl.push(QuoteDispensaryPage, send)
    }
    openModal(afiliado, beneficiarios){
        let modal = this.modalCtrl.create(ModalSelectPage, {afiliado, beneficiarios})
        modal.onDidDismiss(data => {
            this.nombre_filtro = data.nombres +" "+ data.apellidos
            this.paciente = data.cedula
            if(typeof data.cedula === 'undefined'){
                this.paciente = data.documento
            }
        })
        modal.present()
    }
    goToQuotes(){
        // this.navCtrl.setRoot(MenuQuotesPage)
    }
    goToSpecialite(){
        this.navCtrl.setRoot(MenuSpecialtiesPage)
    }
    goToHome(){
        this.navCtrl.setRoot(HomePage)
    }
    gotoQuotesDispensary(){
      this.navCtrl.push(QuotesDispensaryPage);
    }
    showLoading(){
        this.loading = this.loadingCtrl.create({
            content: "Cargando citas...",
            dismissOnPageChange: false
        })
        this.loading.present()
    }
    goToFavorite(){
        this.navCtrl.setRoot(FavoriteDoctorsPage)
    }
}