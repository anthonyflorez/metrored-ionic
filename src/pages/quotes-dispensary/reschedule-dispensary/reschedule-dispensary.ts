import { Component } from '@angular/core';
import { Storage } from "@ionic/storage";
import { NavController, NavParams, LoadingController, Loading, AlertController, ModalController } from 'ionic-angular';
import { QuoteDispensaryPage } from "../quote/quote-dispensary";
import { SettingDispensaryPage } from "../quotes-settings/settings-dispensary/settings-dispensary";
import { SettingsDoctorPage } from "../quotes-settings/settings-doctor/settings-doctor";
import { SettingsQuotesPage } from "../quotes-settings/settings-quote/settings-quotes";
import { QuotesProvider } from "../../../providers/quotes/quotes";
import { CentersProvider } from "../../../providers/centers/centers";
import { SpecialtyProvider } from "../../../providers/specialty/specialty";
import { ModalSelectPage } from "../../modal-select/modal-select";
import { HomePage } from "../../home/home";
import { MenuQuotesDispensaryPage } from "../menu-quotes-dispensary/menu-quotes-dispensary";
import { MenuSpecialtiesPage } from "../../specialties/menu-specialties";
import { FavoriteDoctorsPage } from "../../favorite-doctors/favorite-doctors";
import { QuotesDispensaryPage } from "../../quotes-dispensary/quotes-settings/quotes-dispensary";
import { Calendar } from '@ionic-native/calendar';
import { QuotesDispensaryProvider } from "../../../providers/quotes-dispensary/quotes-dispensary";
/**
 * Generated class for the ReschedulePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@Component({
    selector: 'page-reschedule-dispensary',
    templateUrl: 'reschedule-dispensary.html',
    providers: [QuotesProvider, CentersProvider, SpecialtyProvider, QuotesDispensaryProvider]
})
export class RescheduleDispensaryPage {
    public navRequest: any;
    public cita: {
        descripcion_especialidad: string,
        id_especialidad: number,
        descripcion_centro: string,
        disabled_centro: boolean,
        id_centro: number,
        disabled_doctor: boolean,
        doctor: any,
        nombre_doctor: string,
        disabled_reserva: boolean,
        disable_mejor_cita: boolean
    };
    loading: Loading;
    responseValidateTurn: any;
    responseLocal: any;
    response: any;
    responseDispensary: any
    responsDoctor: any
    responseBeforeQuote: any
    visibleDispensary: boolean = false
    constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController,
        public alertCtrl: AlertController, private quotesProvider: QuotesProvider, public modalCtrl: ModalController,
        private centerProvider: CentersProvider, private specialtyProvider: SpecialtyProvider, private storage: Storage,
        private calendar: Calendar, private quotesDispensaryProvider: QuotesDispensaryProvider) {
        storage.get('dispensario').then(data => {
            if (data == true) {
                this.visibleDispensary = true
            }
        });
        this.cita = {
            id_especialidad: 0,
            descripcion_especialidad: 'Dispensario',
            descripcion_centro: 'Doctor',
            disabled_centro: false,
            id_centro: 0,
            disabled_doctor: false,
            doctor: [],
            nombre_doctor: 'Cita',
            disabled_reserva: false,
            disable_mejor_cita: false
        }
        this.navRequest = this.navParams.data;
        this.storage.get('persona').then(data => {
            this.quotesDispensaryProvider.dispesary = {
                id: data.id,
                nombre: this.navRequest.especialidad
            }
            this.quotesDispensaryProvider.listDoctorDispensaryId().then(dataD => {
                this.responseDispensary = dataD;
                this.quotesDispensaryProvider.id = this.responseDispensary.result.id
                this.cita.descripcion_especialidad = this.navRequest.especialidad
                this.cita.id_especialidad = this.responseDispensary.result.id
                this.quotesDispensaryProvider.listDoctorDispensaryDoctorId().then(dataDr => {
                    this.responsDoctor = dataDr
                    console.log(this.responsDoctor);
                    this.cita.descripcion_centro = this.responsDoctor.result.ob_hm_nombre_medico
                    this.cita.id_centro = this.responsDoctor.result.ob_hm_id_medico;
                })
            })
        })
        this.cita.disabled_centro = true
        this.cita.disabled_doctor = true,
            this.cita.nombre_doctor = this.navRequest.doctor
        this.cita.disabled_centro = true
        this.cita.disabled_reserva = true
    }

    ionViewDidLoad() {

    }
    openModal() {
        let modal = this.modalCtrl.create(ModalSelectPage)
        modal.onDidDismiss(data => {
            console.log(data);
        })
        modal.present()
    }
    goToCentro() {
        this.navCtrl.push(SettingDispensaryPage, this.cita)
    }
    goToQuote() {
        this.showLoading()
        let countDoctor = Object.keys(this.cita.doctor).length
        if (countDoctor > 0) {
            // //aca van las variables de session
            this.quotesProvider.addQuote = {
                apellidos: this.navRequest.apellidos,
                celular: '0',
                email: '',
                hora_turno: this.cita.doctor.turno.FECHA_HORA,
                horario: this.cita.doctor.medico.CODIGO_HORARIO,
                nombres: this.navRequest.nombres,
                paciente: this.navRequest.hm_fk_persona,
                turno: this.cita.doctor.turno.NUMERO_TURNO,
                cedula: (typeof this.navRequest.documento == "undefined") ? this.navRequest.cedula : this.navRequest.documento
            }
            this.quotesProvider.addQuoteLocal = {
                paciente: this.navRequest.hm_fk_persona,
                hora_turno: this.cita.doctor.medico.HORA_INICIO_TURNO.split(' ')[0] + " " + this.cita.doctor.turno.FECHA_HORA.split(' ')[1],
                hora_final_turno: this.cita.doctor.medico.FECHA_FINAL_TURNO,
                horario: this.cita.doctor.medico.CODIGO_HORARIO,
                paciente_local: this.navRequest.ob_hm_fk_persona,
                tipo_persona: this.navRequest.tipo_persona,
                turno: this.cita.doctor.turno.NUMERO_TURNO,
                locacion: this.cita.descripcion_centro,
                doctor: this.cita.nombre_doctor,
                especialidad: this.cita.descripcion_especialidad,
                mejor_dia: 0,
                estado: 2,
                fecha_old: "",
                fecha_old_final: ""
            }
            this.quotesProvider.validateTurnData = {
                id: this.navRequest.ob_hm_fk_persona,
                fecha: this.cita.doctor.turno.FECHA_HORA,
                tipo_paciente: this.navRequest.tipo_persona
            }
            this.quotesProvider.beforeQuote = {
                horario: this.navRequest.hm_fk_horario,
                id: this.navRequest.id,
                turno: this.navRequest.numero_turno
            }
            let send_data = {
                apellido: this.navRequest.apellidos,
                doctor: this.cita.nombre_doctor,
                especialidad: this.cita.descripcion_especialidad,
                hora_turno: this.cita.doctor.medico.HORA_INICIO_TURNO.split(' ')[0] + " " + this.cita.doctor.turno.FECHA_HORA.split(' ')[1],
                locacion: this.cita.descripcion_centro,
                nombres: this.navRequest.nombres
            }
            console.log(this.cita)
            //console.log(this.quotesProvider.addQuote, this.quotesProvider.addQuoteLocal, this.quotesProvider.validateTurnData, this.quotesProvider.beforeQuote, send_data);
            this.quotesProvider.validateTurn().then(dataV => {
                this.responseValidateTurn = dataV;
                if (this.responseValidateTurn.response != false) {
                    this.quotesProvider.rescheduleQuote().then(dataB => {
                        this.responseBeforeQuote = dataB
                        if (this.responseBeforeQuote.response != false) {
                            this.quotesProvider.addQuotesLocal().then(dataL => {
                                this.responseLocal = dataL;
                                if (this.responseLocal.reponse != false) {
                                    this.quotesProvider.addQuotes().then(dataR => {
                                        this.response = dataR;
                                        if (this.response.response == true) {
                                            this.loading.dismiss();
                                            // this.calendar.createEvent(
                                            //     'Cita Medica', this.cita.descripcion_centro+",ecuador","Lugar: "+this.cita.descripcion_centro+"\n"+"Medico: "+this.cita.nombre_doctor+"\n"+"Hora: "+this.cita.doctor.turno.FECHA_HORA,
                                            //     new Date(this.cita.doctor.medico.HORA_INICIO_TURNO.split(' ')[0]+" "+this.cita.doctor.turno.FECHA_HORA.split(' ')[1])
                                            // );
                                            let send = { datos: send_data, vista: 1 }
                                            this.navCtrl.setRoot(QuoteDispensaryPage, send)
                                        } else {
                                            this.loading.dismiss();
                                            this.showError(this.response.result);
                                        }
                                    })
                                } else {
                                    this.loading.dismiss();
                                    this.showError(this.responseLocal.result);
                                    this.cita.disabled_reserva = false;
                                }
                            })
                        }
                    })
                } else {
                    this.loading.dismiss();
                    this.showError(this.responseValidateTurn.result);
                    this.cita.disabled_reserva = false;
                }
            });
        }
        else {
            this.loading.dismiss();
            this.navCtrl.pop()
            //this.navCtrl.setRoot(SearchQuotesPage)
        }
    }
    goToQuoteDoctor() {
        this.navCtrl.push(SettingsQuotesPage, this.cita)
    }
    goToDispensary() {
        this.navCtrl.push(SettingDispensaryPage, this.cita)
    }
    goToDoctor() {
        this.navCtrl.push(SettingsDoctorPage, this.cita)
    }
    showLoading() {
        this.loading = this.loadingCtrl.create({
            content: "Registrando Cita por favor espere ...",
            dismissOnPageChange: false
        })
        this.loading.present()
    }
    showError(text: string) {
        let alert = this.alertCtrl.create({
            title: "Error",
            subTitle: text,
            buttons: ['OK']
        })
        alert.present(prompt)
    }
    goToHome() {
        this.navCtrl.setRoot(HomePage)
    }
    goToQuotes() {
        //this.navCtrl.setRoot(MenuQuotesPage)
    }
    goToSpecialite() {
        this.navCtrl.setRoot(MenuSpecialtiesPage)
    }
    goToFavorite() {
        this.navCtrl.setRoot(FavoriteDoctorsPage)
    }
    gotoQuotesDispensary() {
        this.navCtrl.push(QuotesDispensaryPage);
    }
}
