import { Component } from '@angular/core';
import { NavController, NavParams} from 'ionic-angular';
import { Storage } from "@ionic/storage";
import { HomePage } from "../../home/home";
import { QuotesDispensaryPage } from "../quotes-settings/quotes-dispensary";
import { SearchQuotesDispensaryPage } from "../search-quotes-dispensary/search-quotes-dispensary";
import { HistoryQuotesDispensaryPage } from "../history-quotes-dispensary/history-quotes-dispensary";
import { MenuSpecialtiesPage } from "../../specialties/menu-specialties";
import { FavoriteDoctorsPage } from "../../favorite-doctors/favorite-doctors";
@Component({
    selector: 'page-menu-quotes-dispensary',
    templateUrl: 'menu-quotes-dispensary.html',
})
export class MenuQuotesDispensaryPage{
    pages: Array<{title: string, component: any, img:string}>;
    visibleDispensary: boolean = false
    constructor(public navCtrl: NavController, public navParams: NavParams, private storage: Storage){
        storage.get('dispensario').then(data => {
          if(data == true){
            this.visibleDispensary = true
          }
        });
        this.pages = [
            { title: 'Solicitar Citas', component: QuotesDispensaryPage, img: 'assets/img/icono-p5-80.png' },
            { title: 'Citas Programadas', component: SearchQuotesDispensaryPage, img: 'assets/img/icono-p6-80.png' },
            { title: 'Histórico Citas', component: HistoryQuotesDispensaryPage, img: 'assets/img/icono-p8-80.png' },
        ]
    }
    ionViewDidLoad() {
        console.log('ionViewDidLoad RegisterPage');
    }
    goToPage(p){
        this.navCtrl.push(p.component);
    }
    goToHome(){
        this.navCtrl.setRoot(HomePage);
    }
    gotoQuotesDispensary(){
      this.navCtrl.push(QuotesDispensaryPage);
    }
    goToSpecialties(){
        this.navCtrl.setRoot(MenuSpecialtiesPage)
    }
    goToFavorite(){
        this.navCtrl.setRoot(FavoriteDoctorsPage)
    }
}