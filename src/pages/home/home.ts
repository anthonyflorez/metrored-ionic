import { Component } from '@angular/core';
import { IonicPage, NavController, MenuController } from 'ionic-angular';
import { PostsProvider } from "../../providers/posts/posts";
import { MenuQuotesPage } from "../quotes/menu-quotes/menu-quotes";
import { MenuSpecialtiesPage } from "../specialties/menu-specialties";
import { ArticlePage } from "../article/article";
import { FavoriteDoctorsPage } from "../favorite-doctors/favorite-doctors";
import { MenuQuotesDispensaryPage } from "../quotes-dispensary/menu-quotes-dispensary/menu-quotes-dispensary";
import { Storage } from "@ionic/storage";
import { TokenPushProvider } from "../../providers/token-push/token-push";
import { FCM } from '@ionic-native/fcm';
import { Push, PushToken } from "@ionic/cloud-angular";
import { Network } from '@ionic-native/network';
/**
 * Generated class for the HomePage tabs.
 *
 * See https://angular.io/docs/ts/latest/guide/dependency-injection.html for
 * more info on providers and Angular DI.
 */
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  providers: [PostsProvider, TokenPushProvider]
})
export class HomePage {
  posts: any;
  idPersona: number
  countAds: number
  visibleImage: boolean = false
  visibleSlider: boolean = false
  visibleDispensary: boolean = false
  promotions: any
  internetStatus: boolean = true;
  constructor(public navCtrl: NavController, public postsProvider: PostsProvider, private menuCtrl: MenuController, private storage: Storage,
    private tokenPushProvider: TokenPushProvider, public push: Push, private fcm: FCM, private network: Network) {
    storage.get('dispensario').then(data => {
      if (data == true) {
        this.visibleDispensary = true
      }
    });
    console.log("cambio")

    // watch network for a disconnect
    let disconnectSubscription = this.network.onDisconnect().subscribe(() => {
      console.log('network was disconnected :-(');
    });

    // stop disconnect watch
    disconnectSubscription.unsubscribe();


    // watch network for a connection
    let connectSubscription = this.network.onConnect().subscribe(() => {
      console.log('network connected!');
      // We just got a connection but we need to wait briefly
      // before we determine the connection type. Might need to wait.
      // prior to doing any api requests as well.
      setTimeout(() => {
        if (this.network.type === 'wifi') {
          console.log('we got a wifi connection, woohoo!');
        }
      }, 3000);
    });


    if (this.network.type === 'none') {
      this.internetStatus = false;
    }

    this.network.onDisconnect().subscribe(() => {
      this.internetStatus = false;
    });

    this.network.onConnect().subscribe(() => {
      this.internetStatus = true;
    });


    postsProvider.listPromotions().then(data => {
      this.promotions = data;
      this.promotions = JSON.parse(this.promotions)
      if (this.promotions.length == 1) {
        this.visibleImage = true
      }
      else if (this.promotions.length > 1) {
        this.visibleSlider = true
      }
      console.log(this.promotions);
    });
    postsProvider.listPosts().then(data => {
      this.posts = data
    });
    this.menuCtrl.enable(true);
    this.storage.get('persona').then(data => {
      this.fcm.getToken().then(token => {
        this.tokenPushProvider.addTokenData = {
          alias: push.token.id,
          id: data.id,
          token: token
        }
        this.tokenPushProvider.addToken().then(data => {
        });
      });
    })
  }
  gotoQuotes() {
    this.navCtrl.setRoot(MenuQuotesPage);
  }

  gotoQuotesDispensary() {
    this.navCtrl.setRoot(MenuQuotesDispensaryPage);
  }

  goToSpecialties() {
    this.navCtrl.setRoot(MenuSpecialtiesPage)
  }

  goToArticle(post) {
    this.navCtrl.push(ArticlePage, post)
  }
  goToFavorite() {
    this.navCtrl.setRoot(FavoriteDoctorsPage)
  }
  viewArticle(post) {
    this.navCtrl.push(ArticlePage, post)
  }
}
