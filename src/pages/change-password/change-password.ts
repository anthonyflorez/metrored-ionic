import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, Loading, AlertController } from 'ionic-angular';
import { FormBuilder, Validators, FormGroup } from "@angular/forms";
import { ValidateRpassword } from "../../validators/rpassword";
import { AuthProvider } from "../../providers/auth/auth";
import { Storage } from "@ionic/storage";
/**
 * Generated class for the ChangePasswordPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@Component({
  selector: 'page-change-password',
  templateUrl: 'change-password.html',
  providers: [AuthProvider]
})
export class ChangePasswordPage {

  changeForm: FormGroup;
  cellForm: FormGroup;
  submitAttempt: boolean = false;
  loading: Loading
  response: any
  phone: string = null
  constructor(public navCtrl: NavController, public navParams: NavParams, public formBuilder: FormBuilder,
    public loadingCtrl: LoadingController, public alertCtrl: AlertController, private auth: AuthProvider, private storage: Storage) {
    this.changeForm = formBuilder.group({
      password: ['', Validators.compose([Validators.required])],
      rpassword: ['', Validators.compose([Validators.required])]
    }, {
        validator: ValidateRpassword.checkPassword
      }
    );
    this.cellForm = formBuilder.group({
      phone: ['', Validators.compose([Validators.minLength(7), Validators.maxLength(15), Validators.required, Validators.pattern('[0-9]*')])],
    }
    );
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChangePasswordPage');
  }
  changePassword() {
    this.submitAttempt = true;
    if (!this.changeForm.valid) {
      this.showError("Favor ingresar contraseña valida: \n combinacion de letras y números", "Error");
    }
    else {
      this.showLoading()
      this.storage.get('persona').then(data => {
        this.auth.change_password = {
          id: data.id,
          password: this.changeForm.value.password
        }
        this.auth.changePassword().then(data => {
          this.showError("Contraseña modificada correctamente", "Actualización de Datos")
          this.navCtrl.pop()
        })
      })
    }
  }
  changeCell() {
    this.showLoading()
    this.phone=this.cellForm.controls.phone.value
    if (this.cellForm.valid == false) {
      this.showError("Error en el Celular", "Error")
    }
    else {
      this.storage.get('persona').then(data => {
        this.auth.change_phone = {
          id: data.id,
          celular: this.phone
        }
        this.auth.changePhone().then(dataR => {
          data.celular = this.phone;
          this.storage.set('persona', data);
          this.showError("Celular modificado correctamente", "Actualización de Datos")
          this.navCtrl.pop()
        })
      })
    }
    this.loading.dismiss()
  }
  showLoading() {
    this.loading = this.loadingCtrl.create({
      content: "Espere por favor...",
      dismissOnPageChange: true
    })
    this.loading.present()
  }

  showError(text: string, title: string) {
    //this.loading.dismiss()

    let alert = this.alertCtrl.create({
      title: title,
      subTitle: text,
      buttons: ['OK']
    })
    alert.present(prompt)
  }
}
