import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Slides, MenuController } from 'ionic-angular';
import { LoginPage } from '../login/login'
import { Storage } from "@ionic/storage";
/**
 * Generated class for the TourPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@Component({
  selector: 'page-tour',
  templateUrl: 'tour.html',
})
export class TourPage {
  @ViewChild(Slides) slides: Slides
  splash = true;
  tabBarElement: any;
  sliderData : Array<{img:string, message:string}>
  autoplay: number = 0;
  messageSlider:string = "";
  constructor(public navCtrl: NavController, public navParams: NavParams, public menuCtrl: MenuController, private storage: Storage) {
    this.menuCtrl.enable(false);
    this.sliderData = [
      {img: 'assets/img/bienvenida-image-ultrasonido-500.jpg', message: "<h2>Citas Médicas</h2><p>Agenda de la manera más<br />fácil todas tus citas médicas.</p>"},
      {img: 'assets/img/bienvenida-laboratorio-500.jpg', message: "<h2>Diario de Salud</h2><p>Guarda y encuentra todo lo relacionado<br />con tu salud en la App de Metrored.</p>"},
      // {img: 'assets/img/bn-citas-medicas-particulares.jpg', message: "<h2>Citas Particulares</h2><p>Agenda de la manera más fácil<br />tus citas médicas particulares.</p>"},
      {img: 'assets/img/bienvenida-chequeos-ocupacionales-500.jpg', message: "<h2>Citas Empresariales</h2><p>Agenda de la manera más fácil tus citas<br />médicas empresariales a través de la App de Metrored.</p>"},
      {img: 'assets/img/tips-metrored.jpg', message: "<h2>Tips Metrored</h2><p>Encuentra los mejores tips<br />de salud y bienestar.</p>"}
    ]
    this.storage.set('tour',true);
  }

  ionViewDidLoad() {
    setTimeout(() =>{
      this.messageSlider = this.sliderData[0].message
      this.splash = false
    }, 4000);
  }

  goToLogin(event){
    this.navCtrl.setRoot(LoginPage);
  }
  
  slideChanged(data){
    let currentIndex = this.slides.getActiveIndex();
    let position = currentIndex - 1;
    if(position == 5){
      this.messageSlider = this.sliderData[0].message
    }
    else if(position == -1){
      this.messageSlider = this.sliderData[4].message
    }
    else{
       this.messageSlider = this.sliderData[position].message
    }
    //console.log(this.sliderData[position].message, position);
  }
}
