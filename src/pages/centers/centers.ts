import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { PostsProvider } from "../../providers/posts/posts";
import { ArticlePage } from "../article/article";
import { HomePage } from "../home/home";
import { MenuQuotesPage } from "../quotes/menu-quotes/menu-quotes";
import { MenuSpecialtiesPage } from "../specialties/menu-specialties";
import { FavoriteDoctorsPage } from "../favorite-doctors/favorite-doctors";
import { SpecialitiesCentersPage } from "../specialities-centers/specialities-centers";
import { MenuQuotesDispensaryPage } from "../quotes-dispensary/menu-quotes-dispensary/menu-quotes-dispensary";
import { Storage } from "@ionic/storage";
/**
 * Generated class for the CentersPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@Component({
  selector: 'page-centers',
  templateUrl: 'centers.html',
  providers: [PostsProvider]
})
export class CentersPage {

  public centers: any
  visibleDispensary: boolean = false
  constructor(public navCtrl: NavController, public navParams: NavParams, private postProvider: PostsProvider, private storage: Storage) {
    storage.get('dispensario').then(data => {
      if(data == true){
        this.visibleDispensary = true
      }
    });
  }

  ionViewDidLoad() {
    this.postProvider.listCenters().then(data => {
      this.centers = data;
    })  
  }
  goToSpecialiteInfo(centro){
    this.navCtrl.push(SpecialitiesCentersPage,centro)
  }
  goToHome(){
    this.navCtrl.setRoot(HomePage);
  }
  goToArticle(data){
    this.navCtrl.push(ArticlePage, data)
  }
  goToSpecialite(){
    this.navCtrl.setRoot(MenuSpecialtiesPage)
  }
  gotoQuotes(){
    this.navCtrl.setRoot(MenuQuotesPage);
  }
  goToFavorite(){
    this.navCtrl.setRoot(FavoriteDoctorsPage)
  }
  gotoQuotesDispensary(){
    this.navCtrl.setRoot(MenuQuotesDispensaryPage);
  }
}
