import { Component } from '@angular/core';
import { NavController, NavParams, Loading, AlertController, MenuController, LoadingController } from 'ionic-angular';
import { QuotesProvider } from "../../../providers/quotes/quotes";
import { RegisterBeneficiaryPage } from "../register-beneficiary";
import { Storage } from "@ionic/storage";
import { HomePage } from "../../home/home";
import { MenuQuotesPage } from "../../quotes/menu-quotes/menu-quotes";
import { MenuSpecialtiesPage } from "../../specialties/menu-specialties";
import { FavoriteDoctorsPage } from "../../favorite-doctors/favorite-doctors";
import { MenuQuotesDispensaryPage } from "../../quotes-dispensary/menu-quotes-dispensary/menu-quotes-dispensary";
@Component({
    selector:'page-list-beneficiary',
    templateUrl: 'list-beneficiary.html',
    providers: [QuotesProvider]
})
export class ListBenefiaryPage{
    public document: number;
    nombre:string;
    response:any;
    afiliado:any;
    beneficiarios:any;
    loading: Loading;
    visibleDispensary: boolean = false
    constructor(public navCtrl:NavController, public navParams: NavParams, private quotesProvider: QuotesProvider, private loadingCtrl: LoadingController,
                private storage: Storage){
        storage.get('dispensario').then(data => {
          if(data == true){
            this.visibleDispensary = true
          }
        });
    }
    load(){
        this.showLoading();
        this.storage.get('persona').then((data) => {
            this.afiliado = data;
            this.quotesProvider.id = data.id;
            this.quotesProvider.getPacienteAfilidos()
            .then(dataR => {
                this.response = dataR;
                this.beneficiarios = this.response.result;
                this.loading.dismiss();
            })
        });
    }
    toArray(val){
        return Array.from(val);
    }
    showLoading(){
      this.loading = this.loadingCtrl.create({
          content: "Cargando información\n espere por favor...",
          dismissOnPageChange: false
      })
      this.loading.present()
    }
    addBeneficiario(){
        this.navCtrl.push(RegisterBeneficiaryPage);
    }
    ionViewWillEnter(){
        this.load();
    }
    removeBeneficiary(paciente, index){
        this.storage.get('persona').then(data => {
            this.beneficiarios.splice(index, 1)
            this.quotesProvider.id = paciente.id;
            this.quotesProvider.deleteBeneficiary = {
                id_beneficiario: paciente.id,
                id_paciente: data.id
            }
            this.quotesProvider.removeBeneficiary().then(data => {
            })
        })
    }
    goToHome(){
        this.navCtrl.setRoot(HomePage)
    }
    goToQuotes(){
        this.navCtrl.setRoot(MenuQuotesPage)
    }
    goToSpecialite(){
        this.navCtrl.setRoot(MenuSpecialtiesPage)
    }
    goToFavorite(){
        this.navCtrl.setRoot(FavoriteDoctorsPage)
    }
    gotoQuotesDispensary(){
      this.navCtrl.setRoot(MenuQuotesDispensaryPage);
    }
}