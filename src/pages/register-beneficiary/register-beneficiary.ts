import { Component } from '@angular/core';
import { NavController, NavParams, Loading, LoadingController, Alert, AlertController } from 'ionic-angular';
import { FormBuilder, Validators, FormGroup } from "@angular/forms";
import { NumberValidator } from "../../validators/number";
import { EmailValidator } from "../../validators/email";
import { AuthProvider } from "../../providers/auth/auth";
import { Storage } from "@ionic/storage";
/**
 * Generated class for the RegisterBeneficiaryPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@Component({
  selector: 'page-register-beneficiary',
  templateUrl: 'register-beneficiary.html',
  providers: [AuthProvider]
})
export class RegisterBeneficiaryPage {

  registeredBeneficiaryForm: FormGroup;
  submitAttempt: boolean = false;
  response:any
  loading:Loading
  constructor(public navCtrl: NavController, public navParams: NavParams,  public formBuilder: FormBuilder, private auth: AuthProvider,
              public loadingCtrl: LoadingController, public alertCtrl: AlertController, private storage: Storage) {
      this.registeredBeneficiaryForm = formBuilder.group({
        identification: ['', Validators.compose([Validators.required, Validators.minLength(1), Validators.maxLength(40)]), NumberValidator.checkNumber],
        type_document: ['-1', Validators.required, NumberValidator.checkTypeDocument],
        nombres: ['',Validators.compose([Validators.required, Validators.pattern('[a-zA-Zñáéíóú| ]*$')])],
        apellidos: ['', Validators.compose([Validators.required, Validators.pattern('[a-zA-Zñáéíóú| ]*$')])],
        parentesco: ['-1', Validators.required, NumberValidator.checkParentesco],
        afiliado:[1],
        email: ['', Validators.required, EmailValidator.mailFormat],
        celular: ['', Validators.compose([Validators.minLength(7),Validators.maxLength(15), Validators.required, Validators.pattern('[0-9]*')])]
      })


      
  }

  ionViewDidLoad() {
    this.storage.get('persona').then(data => {
        this.registeredBeneficiaryForm.value.afiliado = data.id
    })
  }

  register(){
    this.submitAttempt = true;
    console.log(this.registeredBeneficiaryForm.value)
    if(!this.registeredBeneficiaryForm.valid){
    }
    else{
      this.showLoading()
      this.storage.get('persona').then(data => {
        this.auth.dataBeneficiario = {
          afiliado: data.id,
          apellidos: this.registeredBeneficiaryForm.value.apellidos,
          identification: this.registeredBeneficiaryForm.value.identification,
          nombres: this.registeredBeneficiaryForm.value.nombres,
          parentesco: this.registeredBeneficiaryForm.value.parentesco,
          type_document: this.registeredBeneficiaryForm.value.type_document,
          email: this.registeredBeneficiaryForm.value.email,
          celular: this.registeredBeneficiaryForm.value.celular
        };
          this.auth.addBeneficiarion().then(data => {
            this.response = data
            if(this.response.response == true){
              this.loading.dismiss()
              this.showError(this.response.result, "Exito");
              this.navCtrl.pop()
            }else{
              this.loading.dismiss()
              this.showError(this.response.result, "Error");
            }
          });
      })
    }
  }
  showLoading(){
      this.loading = this.loadingCtrl.create({
          content: "Espere por favor...",
          dismissOnPageChange: true
      })
      this.loading.present()
  }
  showError(text:string, title){
      //this.loading.dismiss()

      let alert = this.alertCtrl.create({
          title: title,
          subTitle: text,
          buttons:['OK']
      })
      alert.present(prompt)
  }
}
