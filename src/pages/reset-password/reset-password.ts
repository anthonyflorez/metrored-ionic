import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, Loading, AlertController } from 'ionic-angular';
import { FormBuilder, Validators, FormGroup } from "@angular/forms";
import { NumberValidator } from "../../validators/number";
import { EmailValidator } from "../../validators/email";
import { AuthProvider } from "../../providers/auth/auth";
import { RegisterPage } from "../register/register";
import { LoginPage } from "../login/login";
/**
 * Generated class for the ResetPasswordPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@Component({
  selector: 'page-reset-password',
  templateUrl: 'reset-password.html',
  providers: [AuthProvider]
})
export class ResetPasswordPage {
  
  resetForm: FormGroup;
  submitAttempt: boolean = false;
  loading: Loading;
  response: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public formBuilder: FormBuilder, 
              public loadingCtrl: LoadingController, public alertCtrl: AlertController, private auth: AuthProvider) {
    this.resetForm = formBuilder.group({
      identification: ['', Validators.required],
      email: ['', Validators.required, EmailValidator.mailFormat],
      type_document: ['-1', Validators.required, NumberValidator.checkTypeDocument]
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ResetPasswordPage');
  }
  goToLogin(){
    this.submitAttempt = true;
    if(!this.resetForm.valid){
      this.showError("Favor ingresar contraseña valida: \n combinacion de letras y números");
    }
    else{
      this.showLoading();
      this.auth.dataReset = this.resetForm.value;
      this.auth.resetPassword().then(data => {
        this.response = data;
        if(this.response.response){
          this.navCtrl.setRoot(LoginPage,{"reset":true,email:this.resetForm.value.email});
        }else{
          this.loading.dismiss();
          this.showError(this.response.result);
        }
      });
    }
  }
  goToCreate(){
    this.navCtrl.push(RegisterPage)
  }
  showLoading(){
      this.loading = this.loadingCtrl.create({
          content: "Espere por favor...",
          dismissOnPageChange: true
      })
      this.loading.present()
  }

  showError(text:string){
      //this.loading.dismiss()

      let alert = this.alertCtrl.create({
          title: "Error",
          subTitle: text,
          buttons:['OK']
      })
      alert.present(prompt)
  }
}
