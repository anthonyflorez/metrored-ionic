import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, LoadingController, Loading, AlertController, MenuController,Nav } from 'ionic-angular';
import { FormBuilder, Validators, FormGroup } from "@angular/forms";
import { EmailValidator } from "../../validators/email";
import { AuthProvider } from "../../providers/auth/auth";
import { HomePage } from "../home/home";
import { ResetPasswordPage } from "../reset-password/reset-password";
import { RegisterPage } from "../register/register";
import { Storage } from "@ionic/storage";
import { TabsPage } from '../tabs/tabs';
import { TokenPushProvider } from "../../providers/token-push/token-push";
import { FCM } from '@ionic-native/fcm';
/**
/**
 * Generated class for the LoginPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  @ViewChild(Nav) nav: Nav;

  public login:any = {};
  loginForm: FormGroup;
  submitAttempt: boolean = false;
  loading: Loading;
  response: any;
  response2:any;
  reset:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public formBuilder: FormBuilder, private loadingCtrl: LoadingController, 
              private alertCtrl: AlertController, public auth: AuthProvider, private emailValidator: EmailValidator, private menuCtrl: MenuController,
              private storage: Storage,public tokenprovider: TokenPushProvider,private fcm: FCM) {
    if('reset' in this.navParams.data){
      this.reset = this.navParams.data;
      if(this.reset.reset === true){
        this.showError("En los próximos minutos llegará al correo: "+this.reset.email+"\n su nueva contraseña", 'Cambio de Contraseña');
      }
    }
    this.loginForm = formBuilder.group({
      email: ['', Validators.required, EmailValidator.mailFormat],
      password: ['', Validators.required]
    });
    this.menuCtrl.enable(false);
  }

  ionViewDidLoad() {
    this.showLoading()
    this.storage.get('session').then(data => {
      if(data != null && typeof data != 'undefined'){
        this.loading.dismiss()
        this.navCtrl.setRoot(HomePage)
      }
    })
    this.loading.dismiss()
  }
  goToHome(){
    this.submitAttempt = true;

    if(!this.loginForm.valid){
      this.showError("Faltan datos por llenar");
    }
    else{
      this.showLoading();
      this.auth.data = this.loginForm.value;
      this.auth.login().then(data => {
        this.response = data;
        if(this.response.response == true)
        {
          this.navCtrl.setRoot(HomePage);
          this.fcm.getToken().then(token => {
            this.tokenprovider.token_id = {
              token: token,
              id_persona:this.response.result.persona.id
            }
            this.tokenprovider.setTokenPush().then(data => {})


 
        });
        }
        else
        {
          this.loading.dismiss();
          this.showError(this.response.result);
        }
      });
    }
  }

  
  showLoading(){
      this.loading = this.loadingCtrl.create({
          content: "Espere por favor...",
          dismissOnPageChange: true
      })
      this.loading.present()
  }

  showError(text:string, title:string = ''){
      //this.loading.dismiss()
      if(title == ''){
        title = 'Error';
      }

      let alert = this.alertCtrl.create({
          title: title,
          subTitle: text,
          buttons:['OK']
      })
      alert.present(prompt)
  }
  goToResetPassword(){
    this.navCtrl.push(ResetPasswordPage);
  }
  presentAlert(title, subTitle, textbutton) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: subTitle,
      buttons: [textbutton]
    });
    alert.present();
  }

  goToCreate(){
    this.navCtrl.push(RegisterPage);
  }
}
