import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { MoreInfoDoctorPage } from "../more-info-doctor/more-info-doctor";
import { HomePage } from "../home/home";
import { MenuQuotesPage } from "../quotes/menu-quotes/menu-quotes";
import { MenuSpecialtiesPage } from "../specialties/menu-specialties";
import { FavoriteDoctorsPage } from "../favorite-doctors/favorite-doctors";
import { MenuQuotesDispensaryPage } from "../quotes-dispensary/menu-quotes-dispensary/menu-quotes-dispensary";
import { Storage } from "@ionic/storage";
/**
 * Generated class for the SpecialitiesCentersPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@Component({
  selector: 'page-specialities-centers',
  templateUrl: 'specialities-centers.html',
})
export class SpecialitiesCentersPage {

  especialidadesubicaciones: any
  keys : any
  hometitle : any
  visibleDispensary: boolean = false
  constructor(public navCtrl: NavController, public navParams: NavParams, private storage: Storage) {
    storage.get('dispensario').then(data => {
      if(data == true){
        this.visibleDispensary = true
      }
    });
    this.especialidadesubicaciones = this.navParams.data

    if (this.especialidadesubicaciones.ob_hm_titulo_especialidad === undefined || this.especialidadesubicaciones.ob_hm_titulo_especialidad === null) {
      this.hometitle = this.especialidadesubicaciones.ob_hm_nombre_ubicacion;
    }else{
      this.hometitle = this.especialidadesubicaciones.ob_hm_titulo_especialidad;
    }

    console.log(this.hometitle);

    this.keys = Object.keys(this.especialidadesubicaciones.ubicaciones);
    this.keys.sort()
    console.log(this.especialidadesubicaciones);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SpecialitiesCentersPage');
  }
  goTodoctor(title,doctor){
    this.navCtrl.push(MoreInfoDoctorPage, {tSeccion: title, lsDoctor:doctor})
  }
  goToHome(){
    this.navCtrl.setRoot(HomePage)
  }

  gotoQuotes(){
    this.navCtrl.setRoot(MenuQuotesPage);
  }

  gotoQuotesDispensary(){
    this.navCtrl.setRoot(MenuQuotesDispensaryPage);
  }

  goToSpecialties(){
    this.navCtrl.setRoot(MenuSpecialtiesPage)
  }

  goToFavorite(){
        this.navCtrl.setRoot(FavoriteDoctorsPage)
  }
}
