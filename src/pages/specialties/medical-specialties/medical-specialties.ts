import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Loading, LoadingController } from 'ionic-angular';
import { PostsProvider } from "../../../providers/posts/posts";
import { ArticlePage } from "../../article/article";
import { HomePage } from "../../home/home";
import { FavoriteDoctorsPage } from "../../favorite-doctors/favorite-doctors";
import { MenuQuotesPage } from "../../quotes/menu-quotes/menu-quotes";
import { MenuSpecialtiesPage } from "../menu-specialties";
import { SpecialitiesCentersPage } from "../../specialities-centers/specialities-centers";
import { MenuQuotesDispensaryPage } from "../../quotes-dispensary/menu-quotes-dispensary/menu-quotes-dispensary";
import { ServiceLoading } from "../../../providers/service-loading/service-loading";
import { Storage } from "@ionic/storage";
@Component({
    selector: 'page-medical-specialties',
    templateUrl: 'medical-specialties.html',
    providers: [PostsProvider]
})
export class MedicalSpecialtiesPage{
    specialtiesList:any;
    loading:Loading
    alert_present:boolean
    visibleDispensary: boolean = false
    constructor(public navCtrl: NavController, public navParams: NavParams, public specialties: PostsProvider, private loadingCtrl: LoadingController, private storage: Storage, private serviceloading: ServiceLoading) {
        storage.get('dispensario').then(data => {
          if(data == true){
            this.visibleDispensary = true
          }
        });
        this.showLoading()
        this.storage.get('persona').then(data => {
            this.specialties.id = data.id
            this.specialties.listEspecialidades().then(data => {
                this.specialtiesList = data;
                this.loading.dismiss()
            }).catch(err=>{
                this.loading.dismiss();
                if(this.alert_present==false){
                this.serviceloading.Loading();
                this.navCtrl.pop()
                this.alert_present=true
            }
         });
        })
    }
    
    goToArticle(post){
        this.navCtrl.push(ArticlePage, post)
    }
    goToCentros(especialidad){
        this.navCtrl.push(SpecialitiesCentersPage,especialidad)
    }
    showLoading(){
      this.loading = this.loadingCtrl.create({
          content: "Cargando informacion",
          dismissOnPageChange: true
      })
      this.loading.present()
    }
    goToHome(){
        this.navCtrl.setRoot(HomePage);
    }
    goToFavorite(){
        this.navCtrl.setRoot(FavoriteDoctorsPage)
    }
    gotoQuotes(){
        this.navCtrl.setRoot(MenuQuotesPage);
    }
    goToSpecialite(){
        this.navCtrl.setRoot(MenuSpecialtiesPage)
    }
    gotoQuotesDispensary(){
      this.navCtrl.setRoot(MenuQuotesDispensaryPage);
    }
}