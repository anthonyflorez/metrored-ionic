import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Navbar, MenuController } from 'ionic-angular';
import { MedicalSpecialtiesPage } from "./medical-specialties/medical-specialties";
import { CentersPage } from "../centers/centers";
import { HomePage } from "../home/home";
import { MenuQuotesPage } from "../quotes/menu-quotes/menu-quotes";
import { FavoriteDoctorsPage } from "../favorite-doctors/favorite-doctors";
import { MenuQuotesDispensaryPage } from "../quotes-dispensary/menu-quotes-dispensary/menu-quotes-dispensary";
import { Storage } from "@ionic/storage";
/**
 * Generated class for the MenuSpecialtiesPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@Component({
  selector: 'page-menu-specialties',
  templateUrl: 'menu-specialties.html',
})
export class MenuSpecialtiesPage {
  
  @ViewChild(Navbar) navBar: Navbar;
  options: Array<{ title: string, component: any, img:string}>;visibleDispensary: boolean = false;

  constructor(public navCtrl: NavController, public navParams: NavParams, public menuCtrl: MenuController, private storage: Storage) {
    storage.get('dispensario').then(data => {
      if(data == true){
        this.visibleDispensary = true
      }
    });
    this.options = [
      { title: 'Especialidades Médicas', component: MedicalSpecialtiesPage, img: 'assets/img/icono-p23-82.png'},
      { title: 'Centros Médicos', component: CentersPage, img: 'assets/img/icono-p24-82.png'},
    ];
  }

  ionViewDidLoad() {
    this.navBar.backButtonClick = (e:UIEvent) => {
      this.menuCtrl.open();
      this.navCtrl.pop();
    }
  }
  openPage(option){
    this.navCtrl.push(option.component);
  }
  gotoQuotes(){
    this.navCtrl.setRoot(MenuQuotesPage);
  }
  goToHome(){
    this.navCtrl.setRoot(HomePage)
  }
  goToFavorite(){
    this.navCtrl.setRoot(FavoriteDoctorsPage)
  }
  gotoQuotesDispensary(){
    this.navCtrl.setRoot(MenuQuotesDispensaryPage);
  }
}
