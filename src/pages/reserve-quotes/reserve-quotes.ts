import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, Loading, AlertController, ViewController } from 'ionic-angular';
import { QuotesProvider } from "../../providers/quotes/quotes";
import { Storage } from "@ionic/storage";
import { QuotePage } from "../quotes/quote/quote";

import { HomePage } from "../home/home";
import { MenuQuotesPage } from "../quotes/menu-quotes/menu-quotes";
import { MenuSpecialtiesPage } from "../specialties/menu-specialties";
import { MenuQuotesDispensaryPage } from "../quotes-dispensary/menu-quotes-dispensary/menu-quotes-dispensary";
import { Calendar } from '@ionic-native/calendar';

/**
 * Generated class for the ReserveQuotesPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@Component({
    selector: 'page-reserve-quotes',
    templateUrl: 'reserve-quotes.html',
    providers: [QuotesProvider]
})
export class ReserveQuotesPage {

    medicos: any
    keys: any
    visibleQuote: boolean = false
    response: any
    beneficiarios: any
    afiliado: any
    visiblePaciente: boolean = false
    doctor: any
    responseValidateTurn: any
    responseLocal: any
    loading: Loading
    visibleNotQuotes: boolean = false
    visibleDispensary: boolean = false
    constructor(public navCtrl: NavController, public navParams: NavParams, private quoteProvider: QuotesProvider, private storage: Storage,
        public alertCtrl: AlertController, public loadingCtrl: LoadingController, public viewCtrl: ViewController, private calendar: Calendar) {
        storage.get('dispensario').then(data => {
            if (data == true) {
                this.visibleDispensary = true
            }
        });
    }

    ionViewDidLoad() {
        this.showLoading("Cargando citas...")
        this.quoteProvider.dataSetMedico = {
            nombre: '',
            id: this.navParams.data.hm_fk_doctor,
            fk_cita: 0
        }
        this.quoteProvider.getQuoteDoctorFavorite().then(data => {
            this.medicos = data;
            this.medicos = this.medicos.result;
            this.visibleQuote = true;
            if (typeof this.medicos !== "string") {
                this.keys = Object.keys(this.medicos);
                this.visibleQuote = true;
            }
            else {
                this.visibleNotQuotes = true;
            }
            this.loading.dismiss()
        })
        this.storage.get('persona').then((data) => {
            this.afiliado = data;
            this.quoteProvider.id = data.id;
            this.quoteProvider.getPacienteAfilidos()
                .then(dataR => {
                    this.response = dataR;
                    this.beneficiarios = this.response.result;
                })
        });
    }
    validateDuracion(time) {
        let slice = time.split(' ');
        let timeSlide = slice[1].split(':');
        let returnTime;
        if (timeSlide[0] > 0) {
            returnTime = timeSlide[0] + ":" + timeSlide[1] + " Hora";
        }
        else {
            returnTime = timeSlide[1] + " Minutos";
        }
        return returnTime;
    }
    setDoctor(doctor) {
        this.visibleQuote = false;
        this.visiblePaciente = true;
        this.doctor = doctor
    }
    selectPaciente(paciente, tipo) {
        this.quoteProvider.addQuote = {
            apellidos: paciente.apellidos,
            celular: (typeof paciente.celular == 'undefined') ? 0 : paciente.celular,
            email: paciente.email,
            hora_turno: this.doctor.medico.HORA_INICIO_TURNO.split(' ')[0] + " " + this.doctor.turno.FECHA_HORA.split(' ')[1],
            horario: this.doctor.medico.CODIGO_HORARIO,
            nombres: paciente.nombres,
            paciente: paciente.hm_fk_persona,
            turno: this.doctor.turno.NUMERO_TURNO,
            cedula: (typeof paciente.documento == "undefined") ? 0 : paciente.documento
        }
        this.quoteProvider.addQuoteLocal = {
            paciente: paciente.hm_fk_persona,
            hora_turno: this.doctor.medico.HORA_INICIO_TURNO.split(' ')[0] + " " + this.doctor.turno.FECHA_HORA.split(' ')[1],
            hora_final_turno: this.doctor.medico.FECHA_FINAL_TURNO,
            horario: this.doctor.medico.CODIGO_HORARIO,
            paciente_local: paciente.id,
            tipo_persona: tipo,
            turno: this.doctor.turno.NUMERO_TURNO,
            locacion: this.doctor.medico.DESCRIPCION,
            doctor: this.navParams.data.nombre_doctor,
            especialidad: this.navParams.data.especialidad,
            mejor_dia: 0,
            estado: 1,
            fecha_old: "",
            fecha_old_final: ""
        }
        this.quoteProvider.validateTurnData = {
            id: paciente.id,
            fecha: this.doctor.medico.HORA_INICIO_TURNO.split(' ')[0] + " " + this.doctor.turno.FECHA_HORA.split(' ')[1],
            tipo_paciente: tipo
        }
        let send_data = {
            apellido: paciente.apellidos,
            doctor: this.navParams.data.nombre_doctor,
            especialidad: this.navParams.data.especialidad,
            hora_turno: this.doctor.medico.HORA_INICIO_TURNO.split(' ')[0] + " " + this.doctor.turno.FECHA_HORA.split(' ')[1],
            locacion: this.doctor.medico.DESCRIPCION,
            nombres: paciente.nombres
        }
        this.showLoading("Registrando cita...")
        console.log(this.doctor)
        this.quoteProvider.validateTurn().then(dataV => {
            this.responseValidateTurn = dataV;
            console.log(this.responseValidateTurn);
            if (this.responseValidateTurn.response != false) {
                this.quoteProvider.addQuotesLocal().then(dataL => {
                    this.responseLocal = dataL;
                    if (this.responseLocal.reponse != false) {
                        this.quoteProvider.addQuotes().then(dataR => {
                            this.response = dataR;
                            if (this.response.response == true) {
                                this.loading.dismiss();
                                let send = { datos: send_data, vista: 1 }
                                this.navCtrl.setRoot(QuotePage, send)
                                var startDate = new Date(this.doctor.medico.HORA_INICIO_TURNO.split(' ')[0] + " " + this.doctor.medico.HORA_INICIO_TURNO.split(' ')[1]); // beware: month 0 = january, 11 = december
                                //  console.log(startDate);
                                var endDate = new Date(this.doctor.medico.HORA_INICIO_TURNO.split(' ')[0] + " " + this.doctor.medico.HORA_INICIO_TURNO.split(' ')[1]);
                                var title = "Cita Médica";
                                var eventLocation = this.doctor.medico.DESCRIPCION + " ECUADOR";
                                var notes = "LUGAR: " + this.doctor.medico.DESCRIPCION + "\n" + "MÉDICO: " + this.doctor.medico.NOMBRE_MEDICO + "\n" + "PACIENTE: " + "\n" + "HORA: " + this.doctor.turno.FECHA_HORA.split(' ')[1];
                                // var success = function(message) { alert("Success: " + JSON.stringify(message)); };
                                // var error = function(message) { alert("Error: " + message); };
                                console.log("creo")
                                //   this.calendar.createEvent(title,eventLocation,notes,startDate,endDate);
                            } else {
                                this.loading.dismiss();
                                this.showError(this.response.result);
                            }
                        })
                    } else {
                        this.loading.dismiss();
                        this.showError(this.responseLocal.result);
                    }
                })
            } else {
                this.loading.dismiss();
                this.showError(this.responseValidateTurn.result);
            }
        });
    }
    backMenu() {
        console.log(this.visibleQuote);
        if (this.visibleQuote) {
            this.viewCtrl.dismiss()
        }
        if (this.visiblePaciente) {
            this.visibleQuote = true;
            this.visiblePaciente = false;
        }
    }
    showLoading(title: string) {
        this.loading = this.loadingCtrl.create({
            content: title,
            dismissOnPageChange: false
        })
        this.loading.present()
    }
    showError(text: string) {
        let alert = this.alertCtrl.create({
            title: "Error",
            subTitle: text,
            buttons: ['OK']
        })
        alert.present(prompt)
    }

    goToHome() {
        this.navCtrl.setRoot(HomePage);
    }
    goToQuotes() {
        this.navCtrl.setRoot(MenuQuotesPage)
    }
    goToSpecialite() {
        this.navCtrl.setRoot(MenuSpecialtiesPage)
    }
    gotoQuotesDispensary() {
        this.navCtrl.setRoot(MenuQuotesDispensaryPage);
    }
}
