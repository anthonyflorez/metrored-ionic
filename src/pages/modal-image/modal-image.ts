import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import { FileTransfer, FileTransferObject } from "@ionic-native/file-transfer";
declare var cordova: any;
/**
 * Generated class for the ModalImagePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@Component({
  selector: 'page-modal-image',
  templateUrl: 'modal-image.html',
})
export class ModalImagePage {
  lastImage:string = null
  navRequest:any
  constructor(public navCtrl: NavController, public navParams: NavParams, private transfer: FileTransfer, private viewController: ViewController) {
    this.navRequest = this.navParams.data
  }

  ionViewDidLoad() {
    this.lastImage = this.navRequest.file
  }
  public uploadImage(){
    let url = "http://metroredapp.hmetro.med.ec/metrored/public/api/local/citas/addReceta";
    let targetPath = this.pathForImage(this.lastImage)
    let filename = this.lastImage
    let options = {
        fileKey: "file",
        fileName: filename,
        chunkedMode: false,
        mimeType: "multipart/form-data",
        params : {'name': filename, 'id': this.navRequest.id},
        httpMethod: 'POST',
        headers: {'Authorization':'Bearer ZXN0ZSBlcyBlbCB0b2tlbiBxdWUgc2UgdmEgYSB1dGlsaXphciBkZSBwcnVlYmE='}
    }
    const filetransfer: FileTransferObject = this.transfer.create();
    filetransfer.upload(targetPath,url, options).then(data => {
        this.navRequest.images.push({
          url: 'http://metroredapp.hmetro.med.ec/metrored/public/uploads/'+data.response
        })
        this.viewController.dismiss()
    },error => {
        console.log(error);
    })
  }
  public pathForImage(img) {
    if (img === null) {
        return '';
    } else {
        return cordova.file.dataDirectory + img;
    }
  }
  close(){
    this.viewController.dismiss()
  }
}
