import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';

/**
 * Generated class for the ModalImageQuotePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@Component({
  selector: 'page-modal-image-quote',
  templateUrl: 'modal-image-quote.html',
})
export class ModalImageQuotePage {
  navRequest: any
  constructor(public navCtrl: NavController, public navParams: NavParams, private viewController:ViewController) {
    this.navRequest = this.navParams.data
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ModalImageQuotePage');
  }
  close(){
    this.viewController.dismiss()
  }
}
