import { Component } from '@angular/core';
import { NavController, NavParams, Loading, LoadingController, AlertController, ModalController } from 'ionic-angular';
import { QuotesProvider } from "../../providers/quotes/quotes";
import { Storage } from "@ionic/storage";
import { ReserveQuotesPage } from "../reserve-quotes/reserve-quotes";
import { ReserveQuotesPage2 } from "../reserve-quotes2/reserve-quotes";
import { HomePage } from "../home/home";
import { MenuQuotesPage } from "../quotes/menu-quotes/menu-quotes";
import { MenuSpecialtiesPage } from "../specialties/menu-specialties";
import { MenuQuotesDispensaryPage } from "../quotes-dispensary/menu-quotes-dispensary/menu-quotes-dispensary";
/**
 * Generated class for the FavoriteDoctorsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@Component({
  selector: 'page-favorite-doctors',
  templateUrl: 'favorite-doctors.html',
  providers: [QuotesProvider]
})
export class FavoriteDoctorsPage {

  result: any
  favoriteDoctors: any
  loading:Loading
  visibleDispensary: boolean = false
  constructor(public navCtrl: NavController, public navParams: NavParams, private quoteProvider: QuotesProvider, private storage: Storage, public alertCtrl:AlertController,
              public loadingCtrl:LoadingController, public modalCtrl: ModalController) {
    storage.get('dispensario').then(data => {
      if(data == true){
        this.visibleDispensary = true
      }
    });
    this.showLoading()
    this.storage.get('persona').then(data => {
      this.quoteProvider.dataSetMedico = {
        fk_cita: 0,
        id: data.id,
        nombre: ''
      }
      this.quoteProvider.getAllfavoriteDoctors().then(data => {
          this.result = data;
          this.favoriteDoctors = this.result.result
          this.loading.dismiss()
      }).catch(error => {
        this.showError(error, "error");
      })
    })
  }

  ionViewDidLoad() { 
  }
  removeFavorite(doctor, index){
    this.quoteProvider.dataSetMedicoFavorito = {
      id:doctor.id_persona,
      nombre: doctor.nombre_doctor.nombre_doctor,
      id_medico: doctor.hm_fk_doctor
    };
    this.quoteProvider.setMedicoFavorito().then(data => {
    })
    this.favoriteDoctors.splice(index, 1)
  }
  // registerReserve(doctor){
  //   let modal = this.modalCtrl.create(ReserveQuotesPage, doctor)
  //   modal.onDidDismiss(data => {
  //     console.log(data);
  //   })
  //   modal.present()
  // }


  registerReserve(doctor){
    this.navCtrl.push(ReserveQuotesPage2,doctor);
    // this.navRequest.descripcion_centro = centro.DESCRIPCION
    // //this.navRequest.id_centro = centro.FK_INSTITUCION
    // this.navRequest.id_centro = centro.PK_CODIGO
    // this.navRequest.disabled_doctors = true;
    // this.navCtrl.pop()
}
    showLoading(){
      this.loading = this.loadingCtrl.create({
          content: "Cargando información\n espere por favor...",
          dismissOnPageChange: false
      })
      this.loading.present()
    }

    showError(text:string, title:string){
        let alert = this.alertCtrl.create({
            title: title,
            subTitle: text,
            buttons:['OK']
        })
        alert.present(prompt)
    }
    goToHome(){
      this.navCtrl.setRoot(HomePage);
    }
    goToQuotes(){
      this.navCtrl.setRoot(MenuQuotesPage)
    }
    goToSpecialite(){
      this.navCtrl.setRoot(MenuSpecialtiesPage)
    }
    gotoQuotesDispensary(){
      this.navCtrl.setRoot(MenuQuotesDispensaryPage);
    }
}
