import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { HomePage } from "../home/home";
import { MenuQuotesPage } from "../quotes/menu-quotes/menu-quotes";
import { MenuSpecialtiesPage } from "../specialties/menu-specialties";
import { FavoriteDoctorsPage } from "../favorite-doctors/favorite-doctors";
import { QuotesDispensaryPage } from "../quotes-dispensary/quotes-settings/quotes-dispensary";
import { Storage } from "@ionic/storage";
import { QuotesProvider } from "../../providers/quotes/quotes";
/**
 * Generated class for the MoreInfoDoctorPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@Component({
  selector: 'page-more-info-doctor',
  templateUrl: 'more-info-doctor.html',
  providers: [QuotesProvider]
})
export class MoreInfoDoctorPage {
  doctors:any
  rate: any
  title: any
  visibleDispensary: boolean = false
  titleButton: string = 'Agregar a favoritos'
  constructor(public navCtrl: NavController, public navTitle: NavParams, public navParams: NavParams, private storage: Storage,
              private quoteProvider: QuotesProvider ) {
    storage.get('dispensario').then(data => {
      if(data == true){
        this.visibleDispensary = true
      }
    });
    this.doctors = navParams.data.lsDoctor;
    this.title = navParams.data.tSeccion;
  }

  ionViewDidLoad() {
  }
  onModelChange(doctor){
    this.storage.get('persona').then(data => {
      this.quoteProvider.dataSetMedicoFavorito = {
        id: data.id,
        id_medico: doctor.fk_hm_doctor,
        nombre: doctor.ob_hm_titulo_doctor
      }
      this.quoteProvider.setMedicoFavorito();
      if(doctor.seleccionado == false){
        doctor.seleccionado = true
      }
      else{
        doctor.seleccionado = false
      }
    })
  }
  goToHome(){
    this.navCtrl.setRoot(HomePage)
  }
  gotoQuotes(){
    this.navCtrl.setRoot(MenuQuotesPage);
  }

  gotoQuotesDispensary(){
    this.navCtrl.setRoot(QuotesDispensaryPage);
  }

  goToSpecialties(){
    this.navCtrl.setRoot(MenuSpecialtiesPage)
  }

  goToFavorite(){
        this.navCtrl.setRoot(FavoriteDoctorsPage)
  }
}
