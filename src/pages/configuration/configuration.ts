import { Component, ViewChild } from '@angular/core';
import { Navbar, NavController, NavParams, MenuController } from 'ionic-angular';
import { ChangePasswordPage } from "../change-password/change-password";
import { HomePage } from "../home/home";
/**
 * Generated class for the ConfigurationPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@Component({
  selector: 'page-configuration',
  templateUrl: 'configuration.html',
})
export class ConfigurationPage {
  @ViewChild(Navbar) navBar: Navbar;
  options: Array<{ title: string, component: any, img:string}>;

  constructor(public navCtrl: NavController, public navParams: NavParams, public menuCtrl: MenuController) {
    // this.options = [
    //   { title: 'Notificaciones push', component: 'algo', img: 'assets/img/icono-p21-80.png' },
    //   { title: 'Notificación email', component: 'algo2', img: 'assets/img/icono-p22-80.png'},
    //   { title: 'Cambiar número celular', component: 'algo2', img: 'assets/img/icono-p22-80.png'},
    //   { title: 'Cambiar contraseña', component: ChangePasswordPage, img: 'assets/img/icono-p22-81.png'}
    // ];
  }

  ionViewDidLoad() {
    this.navBar.backButtonClick = (e:UIEvent) => {
      this.menuCtrl.open();
      this.navCtrl.pop();
    }
  }
  goToPage(){
    this.navCtrl.push(ChangePasswordPage)
  }
  goToHome(){
    this.navCtrl.setRoot(HomePage)
  }
}
