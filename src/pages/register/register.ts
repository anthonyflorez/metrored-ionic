import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, Loading, AlertController, MenuController } from 'ionic-angular';
import { FormBuilder, Validators, FormGroup } from "@angular/forms";
import { NumberValidator } from "../../validators/number";
import { EmailValidator } from "../../validators/email";
import { ValidateRpassword } from "../../validators/rpassword";
import { AuthProvider } from "../../providers/auth/auth";
import { HomePage } from "../home/home";
import { LoginPage } from "../login/login";
/**
 * Generated class for the RegisterPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
  providers: [AuthProvider, EmailValidator]
})
export class RegisterPage {

  registeredForm: FormGroup;
  submitAttempt: boolean = false;
  loading: Loading;
  response: any;
  typeField:string;
  constructor(public navCtrl: NavController, public navParams: NavParams, public formBuilder: FormBuilder, 
              public loadingCtrl: LoadingController, public alertCtrl: AlertController, private auth: AuthProvider, private emailValidator: EmailValidator,
              private menuCtrl: MenuController) {
                
      this.registeredForm = formBuilder.group({
        identification: ['', Validators.compose([Validators.required, Validators.minLength(1), Validators.maxLength(40)]), NumberValidator.checkNumber],
        email: ['', Validators.required, EmailValidator.mailFormat],
        type_document: ['-1', Validators.required, NumberValidator.checkTypeDocument],
        nombres: ['',Validators.compose([Validators.required, Validators.pattern('[a-zA-Zñáéíóú| ]*$')])],
        apellidos: ['', Validators.compose([Validators.required, Validators.pattern('[a-zA-Zñáéíóú| ]*$')])],
        celular: ['', Validators.compose([Validators.minLength(7),Validators.maxLength(15), Validators.required, Validators.pattern('[0-9]*')])],
        password: ['', Validators.compose([Validators.minLength(8), Validators.required])],
        rpassword: ['', Validators.compose([Validators.required])]
        },{
          validator: ValidateRpassword.checkPassword
        }
      );
      this.menuCtrl.enable(false);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }
  goToHome(){
    this.submitAttempt = true;
    if(!this.registeredForm.valid){
      this.presentAlert("Faltan datos por llenar o \n estan incorrectos","Error","Cerrar");
    }
    else{
      this.showLoading();
      this.auth.dataUsuario = this.registeredForm.value;
      this.auth.addUser().then(data => {
        this.response = data;
        if(this.response.response == true)
        {
          this.presentAlert("Gracias, por registrarse en nuestra aplicación","Exito","Cerrar");
          this.navCtrl.setRoot(LoginPage);
        }
        else
        {
          this.loading.dismiss();
          this.presentAlert("Datos incorrectos, intente nuevamente","Error","Cerrar");
        }
      })
      .catch(error => {
          this.loading.dismiss();
          this.showError(error);
      });
    }
  }
  presentAlert(subtitle, title,nom_button) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: subtitle,
      buttons: [nom_button]
    });
    alert.present();
  }
  goToBackLogin(){
    this.navCtrl.pop()
  }

  showLoading(){
      this.loading = this.loadingCtrl.create({
          content: "Espere por favor...",
          dismissOnPageChange: true
      })
      this.loading.present()
  }

  showError(text:string){
      //this.loading.dismiss()

      let alert = this.alertCtrl.create({
          title: "Error",
          subTitle: text,
          buttons:['OK']
      })
      alert.present(prompt)
  }
}
