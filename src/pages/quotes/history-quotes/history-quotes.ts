import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController, NavParams, ModalController, LoadingController, Loading } from 'ionic-angular';
import { QuotesProvider } from "../../../providers/quotes/quotes";
import { Storage } from "@ionic/storage";
import { ModalSelectPage } from "../../modal-select/modal-select";
import { QuotePage } from "../quote/quote";
import { MenuQuotesPage } from "../menu-quotes/menu-quotes";
import { MenuSpecialtiesPage } from "../../specialties/menu-specialties";
import { HomePage } from "../../home/home";
import { FavoriteDoctorsPage } from "../../favorite-doctors/favorite-doctors";
import { MenuQuotesDispensaryPage } from "../../quotes-dispensary/menu-quotes-dispensary/menu-quotes-dispensary";
import { ServiceLoading } from "../../../providers/service-loading/service-loading";
@Component({
    selector: 'page-history-quotes',
    templateUrl: 'history-quotes.html',
    providers: [QuotesProvider]
})
export class HistoryQuotesPage{
    citas:any
    paciente: any;
    afiliado:any
    beneficiarios:any
    response:any
    keys: any
    nombre_filtro: string
    message_none: string
    loading: Loading
    visibleDispensary: boolean = false
    constructor(public navCtrl: NavController, public navParams: NavParams, public quotesProvider: QuotesProvider, private storage: Storage,
                public modalCtrl:ModalController, public loadingCtrl: LoadingController,private  serviceloading: ServiceLoading ){
        storage.get('dispensario').then(data => {
          if(data == true){
            this.visibleDispensary = true
          }
        });
        //this.citas = this.quotesProvider.getMedicos();
        this.paciente = "0";
        this.nombre_filtro = "Filtrar por..."
        this.loadSelect()
    }
    loadSelect(){
        this.showLoading()
        this.storage.get('persona').then((data) => {
            this.afiliado = data;
            this.quotesProvider.id = data.id;
            this.quotesProvider.getPacienteAfilidos().then(data => {
                this.citas = data
                this.beneficiarios = this.citas.result;
                this.loading.dismiss();
            }).catch(err=>{
                this.loading.dismiss();
                this.serviceloading.Loading();
                this.navCtrl.pop()
         })
            this.quotesProvider.getHistory().then(dataR => {
                this.response = dataR;
                if(typeof this.response.result != 'string'){
                  this.response = this.response.result;
                  this.keys = Object.keys(this.response);
                }
                else{
                    this.message_none = this.response.result
                }
                this.loading.dismiss();
            }).catch(err=>{
                this.loading.dismiss();
               
                this.navCtrl.pop()
         })
        })
        this.loading.dismiss();
    }
    goToDetails(detail, tipo){
        let send = {datos: detail, vista: tipo}
        this.navCtrl.push(QuotePage, send)
    }
    openModal(afiliado, beneficiarios){
        let modal = this.modalCtrl.create(ModalSelectPage, {afiliado, beneficiarios})
        modal.onDidDismiss(data => {

            this.nombre_filtro = data.nombres +" "+ data.apellidos
            this.paciente = data.cedula
            console.log(data)
            if(typeof data.cedula === 'undefined'){
                console.log("aqui dendro ")
                this.paciente = data.documento
            }
        })
        modal.present()
    }
    showLoading(){
        this.loading = this.loadingCtrl.create({
            content: "Cargando citas...",
            dismissOnPageChange: false
        })
        this.loading.present()
    }
    goToQuotes(){
        this.navCtrl.setRoot(MenuQuotesPage)
    }
    goToSpecialite(){
        this.navCtrl.setRoot(MenuSpecialtiesPage)
    }
    goToHome(){
        this.navCtrl.setRoot(HomePage)
    }
    goToFavorite(){
        this.navCtrl.setRoot(FavoriteDoctorsPage)
    }
    gotoQuotesDispensary(){
      this.navCtrl.setRoot(MenuQuotesDispensaryPage);
    }
    mes(index){
        let position = parseInt(index) - 1;
        let meses = [
            'Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'
        ]
        return meses[position];
    }
}