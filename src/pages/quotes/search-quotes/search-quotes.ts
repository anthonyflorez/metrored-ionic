import { Component } from '@angular/core';
import { NavController, NavParams, ModalController, LoadingController, Loading } from 'ionic-angular';
import { QuotesProvider } from "../../../providers/quotes/quotes";
import { Storage } from "@ionic/storage";
import { QuotePage } from "../quote/quote";
import { ModalSelectPage } from "../../modal-select/modal-select";
import { MenuQuotesPage } from "../menu-quotes/menu-quotes";
import { MenuSpecialtiesPage } from "../../specialties/menu-specialties";
import { HomePage } from "../../home/home";
import { FavoriteDoctorsPage } from "../../favorite-doctors/favorite-doctors";
import { MenuQuotesDispensaryPage } from "../../quotes-dispensary/menu-quotes-dispensary/menu-quotes-dispensary";
import { ServiceLoading } from "../../../providers/service-loading/service-loading";
import { Calendar } from '@ionic-native/calendar';

@Component({
    selector: 'page-search-quotes',
    templateUrl: 'search-quotes.html',
    providers: [QuotesProvider]
})
export class SearchQuotesPage{
    citas:any
    paciente: any;
    afiliado:any
    beneficiarios:any
    response:any
    response2:any
    id_afiliado:number
    keys: any
    nombre_filtro: string
    loading: Loading
    message_none: string
    alert_present:boolean
    visibleDispensary: boolean = false
    constructor(public navCtrl: NavController, public navParams: NavParams, public quotesProvider: QuotesProvider, private storage: Storage, 
                public modalCtrl:ModalController, public loadingCtrl: LoadingController, private serviceloading: ServiceLoading){
        console.log("esta aqui")
        storage.get('dispensario').then(data => {
          if(data == true){
            this.visibleDispensary = true
          }
        });
        this.paciente = "0";
        this.nombre_filtro = "Filtrar por..."
        this.alert_present=false

    }

    ionViewWillEnter(){
        this.loadSelect()
    }

    loadSelect(){
        this.showLoading()
        this.storage.get('persona').then((data) => {
            this.afiliado = data;
            this.quotesProvider.id = data.id;
            this.quotesProvider.getPacienteAfilidos().then(data => {
                this.citas = data
                this.beneficiarios = this.citas.result;
                this.loading.dismiss();
            }).catch(err=>{
                this.loading.dismiss();
                this.serviceloading.Loading();
                this.navCtrl.pop()
         })
            this.quotesProvider.historyQuotes().then(dataR => {
                this.response = dataR;
                if(typeof this.response.result != 'string'){
                   this.response = this.response.result;
                   this.keys = Object.keys(this.response);
                }
                else{
                    this.message_none = this.response.result
                }
                this.loading.dismiss();
            }).catch(err=>{
                this.loading.dismiss();
                if(this.alert_present==false){
                this.serviceloading.Loading();
                this.navCtrl.pop()
                this.alert_present=true
            }
         })

        })
    }
    goToDetails(detail, tipo){
        let send = {datos: detail, vista: tipo,textarea:false}
        console.log(send)
        this.navCtrl.push(QuotePage, send)
    
    }
    openModal(afiliado, beneficiarios){
        let modal = this.modalCtrl.create(ModalSelectPage, {afiliado, beneficiarios})
        modal.onDidDismiss(data => {
            this.nombre_filtro = data.nombres +" "+ data.apellidos
            this.paciente = data.cedula
            if(typeof data.cedula === 'undefined'){
                this.paciente = data.documento
            }
        })
        modal.present()
    }
    goToQuotes(){
        this.navCtrl.setRoot(MenuQuotesPage)
    }
    goToSpecialite(){
        this.navCtrl.setRoot(MenuSpecialtiesPage)
    }
    goToHome(){
        this.navCtrl.setRoot(HomePage)
    }
    gotoQuotesDispensary(){
      this.navCtrl.setRoot(MenuQuotesDispensaryPage);
    }
    showLoading(){
        this.loading = this.loadingCtrl.create({
            content: "Cargando citas...",
            dismissOnPageChange: false
        })
        this.loading.present()
    }
    goToFavorite(){
        this.navCtrl.setRoot(FavoriteDoctorsPage)
    }
    mes(index){
        let position = parseInt(index) - 1;
        let meses = [
            'Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'
        ]
        return meses[position];
    }
}
