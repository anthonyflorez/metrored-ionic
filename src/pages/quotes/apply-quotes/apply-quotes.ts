import { Component } from '@angular/core';
import { NavController, NavParams, Loading, AlertController, MenuController, LoadingController } from 'ionic-angular';
import { QuotesSettingsPage } from "./quotes-settings/quotes-settings";
import { QuotesProvider } from "../../../providers/quotes/quotes";
import { RegisterBeneficiaryPage } from "../../register-beneficiary/register-beneficiary";
import { Storage } from "@ionic/storage";
import { HomePage } from "../../home/home";
import { MenuQuotesPage } from "../menu-quotes/menu-quotes";
import { MenuSpecialtiesPage } from "../../specialties/menu-specialties";
import { FavoriteDoctorsPage } from "../../favorite-doctors/favorite-doctors";
import { MenuQuotesDispensaryPage } from "../../quotes-dispensary/menu-quotes-dispensary/menu-quotes-dispensary";
import { ServiceLoading } from "../../../providers/service-loading/service-loading";

@Component({
    selector:'page-apply-quotes',
    templateUrl: 'apply-quotes.html',
    providers: [QuotesProvider]
})
export class ApplyQuotesPage{
    public document: number;
    nombre:string;
    response:any;
    afiliado:any;
    beneficiarios:any;
    loading: Loading;
    alert_present:boolean
    visibleDispensary: boolean = false
    constructor(public navCtrl:NavController, public navParams: NavParams, private quotesProvider: QuotesProvider, private loadingCtrl: LoadingController,
                private storage: Storage,private serviceloading:ServiceLoading){
        storage.get('dispensario').then(data => {
          if(data == true){
            this.visibleDispensary = true
          }
        });
    }
    load(){
        this.showLoading();
        this.storage.get('persona').then((data) => {
            console.log(data);
            this.afiliado = data;
            this.quotesProvider.id = data.id;
            this.quotesProvider.getPacienteAfilidos()
            .then(dataR => {
                this.response = dataR;
                this.beneficiarios = this.response.result;

                this.loading.dismiss();
                console.log(dataR)
                
            }).catch(err=>{
                this.loading.dismiss();
                if(this.alert_present==false){
                this.serviceloading.Loading();
                this.navCtrl.pop()
                this.alert_present=true
            }
         });

            
        });
    }
    selectPaciente(paciente, tipo){
        let navDataParams = {paciente, tipo}
        console.log(navDataParams);
        this.navCtrl.push(QuotesSettingsPage,navDataParams);
    }
    toArray(val){
        return Array.from(val);
    }
    showLoading(){
      this.loading = this.loadingCtrl.create({
          content: "Cargando información\n espere por favor...",
          dismissOnPageChange: false
      })
      this.loading.present()
    }
    addBeneficiario(){
        this.navCtrl.push(RegisterBeneficiaryPage);
    }
    ionViewWillEnter(){
        this.load();
    }

    goToHome(){
        this.navCtrl.setRoot(HomePage)
    }
    goToQuotes(){
        this.navCtrl.setRoot(MenuQuotesPage)
    }
    goToSpecialite(){
        this.navCtrl.setRoot(MenuSpecialtiesPage)
    }
    goToFavorite(){
        this.navCtrl.setRoot(FavoriteDoctorsPage)
    }
    gotoQuotesDispensary(){
      this.navCtrl.setRoot(MenuQuotesDispensaryPage);
    }
}
