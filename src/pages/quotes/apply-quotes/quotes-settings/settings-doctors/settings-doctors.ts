import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, Loading, AlertController } from 'ionic-angular';
import { QuotesProvider } from "../../../../../providers/quotes/quotes";
import { ServiceLoading } from "../../../../../providers/service-loading/service-loading";
@Component({
    selector:'page-settings-doctors',
    templateUrl: 'settings-doctors.html',
    providers: [QuotesProvider]
})
export class SettingsDoctorsPage{
    navRequest: any
    responseDoctors: any
    doctors: any
    loading: Loading
    visibleTitle: boolean = false
    visibleContent: boolean = false
    title: string
    constructor(public navCtrl: NavController, public navParams: NavParams, private quotesProviders: QuotesProvider, private loadingCtrl: LoadingController,
                private alertCtrl: AlertController, private serviceloading: ServiceLoading){
        this.showLoading()
        this.navRequest = this.navParams.data;
        this.quotesProviders.quote = {
            id_centro: this.navRequest.id_centro,
            id_especialidad: this.navRequest.id_especialidad
        }
        this.quotesProviders.getDoctorQuote().then(data => {
            this.responseDoctors = data
            if(typeof this.doctors === "string"){
                this.visibleTitle = true
            }
            else{
                this.visibleContent = true
                this.doctors = this.responseDoctors.result
            }
            this.loading.dismiss()
        }).catch(err=>{
            this.loading.dismiss();
            this.serviceloading.Loading();
            this.navCtrl.pop()
        })
    }
    addDoctors(doctor){
        this.navRequest.disabled_doctor = true;
        this.navRequest.descripcion_doctor = doctor.NOMBRE_MEDICO
        this.navRequest.id_doctor = doctor.CODIGO_MEDICO
        this.navCtrl.pop()
    }
    showLoading(){
        this.loading = this.loadingCtrl.create({
            content: "Cargando información\n espere por favor...",
            dismissOnPageChange: false
        })
        this.loading.present()
      }
  
      showError(text:string){
          let alert = this.alertCtrl.create({
              title: "Error",
              subTitle: text,
              buttons:['OK']
          })
          alert.present(prompt)
      }
}