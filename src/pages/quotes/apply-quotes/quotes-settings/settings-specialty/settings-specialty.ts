import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, Loading, AlertController } from 'ionic-angular';
import { SpecialtyProvider } from "../../../../../providers/specialty/specialty";
import { ServiceLoading } from "../../../../../providers/service-loading/service-loading";
@Component({
    selector:'page-settings-specialty',
    templateUrl: 'settings-specialty.html',
    providers: [SpecialtyProvider]
})
export class SettingSpecialtyPage{
    reponse:any;
    especialidades:any;
    loading: Loading;
    alert_present:boolean
    private navRequest:any
    constructor(public navCtrl: NavController, public navParams: NavParams, private specialtyProvider: SpecialtyProvider, private loadingCtrl: LoadingController,
                private alertCtrl:AlertController, private serviceloading:ServiceLoading){
        this.load();
        this.navRequest = navParams.data;
    }


    load(){
        this.showLoading();
        this.specialtyProvider.getEspecialidades().then(data => {
            this.reponse = data;
            this.especialidades = this.reponse.result;
            this.loading.dismiss();
        }).catch(err=>{
            this.loading.dismiss();
                if(this.alert_present==false){
                this.serviceloading.Loading();
                this.navCtrl.pop()
                this.alert_present=true
            }
        })
    }
    showLoading(){
      this.loading = this.loadingCtrl.create({
          content: "Cargando información\n espere por favor...",
          dismissOnPageChange: false
      })
      this.loading.present()
    }

    showError(text:string){
        let alert = this.alertCtrl.create({
            title: "Error",
            subTitle: text,
            buttons:['OK']
        })
        alert.present(prompt)
    }

    addEspecialidad(especialidad){
        this.navRequest.descripcion_especialidad = especialidad.DESCRIPCION;
        this.navRequest.id_especialidad = especialidad.PK_CODIGO;
        this.navRequest.disabled_centro = true;
        this.navCtrl.pop()
    }
}