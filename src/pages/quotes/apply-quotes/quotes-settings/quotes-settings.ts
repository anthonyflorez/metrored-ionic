import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, LoadingController, Loading, AlertController, ModalController, Navbar } from 'ionic-angular';
import { QuotePage } from "../../quote/quote";
import { CenterPage } from "./settings-center/center";
import { SettingSpecialtyPage } from "./settings-specialty/settings-specialty";
import { DoctorPage } from "./settings-doctor/doctor";
import { QuotesProvider } from "../../../../providers/quotes/quotes";
import { Storage } from "@ionic/storage";
import { ModalSelectPage } from "../../../modal-select/modal-select";
import { HomePage } from "../../../home/home";
import { MenuQuotesPage } from "../../menu-quotes/menu-quotes";
import { MenuSpecialtiesPage } from "../../../specialties/menu-specialties";
import { FavoriteDoctorsPage } from "../../../favorite-doctors/favorite-doctors";
import { QuotesDispensaryPage } from "../../../quotes-dispensary/quotes-settings/quotes-dispensary";
import { SettingsDoctorsPage } from "../quotes-settings/settings-doctors/settings-doctors";
import { Calendar } from '@ionic-native/calendar';
@Component({
    selector: 'page-quotes-settings',
    templateUrl: 'quotes-setting.html',
    providers: [QuotesProvider]
})
export class QuotesSettingsPage {
    @ViewChild(Navbar) navBar: Navbar;
    loading: Loading;
    response: any;
    msgpromt: string
    public navRequest: any;
    public cita: {
        descripcion_especialidad: string,
        id_especialidad: number,
        descripcion_centro: string,
        disabled_centro: boolean,
        id_centro: number,
        disabled_doctor: boolean,
        doctor: any,
        nombre_doctor: string,
        disabled_reserva: boolean,
        disable_mejor_cita: boolean,
        descripcion_doctor: string
        disabled_doctors: boolean,
        id_doctor: number,
        state_best_date: number,
        paciente: string
    };
    responseValidateTurn: any;
    responseLocal: any;
    bestDate: boolean = false

    visibleDispensary: boolean = false
    constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController,
        public alertCtrl: AlertController, private quotesProvider: QuotesProvider, private storage: Storage, public modalCtrl: ModalController,
        private calendar: Calendar) {
        storage.get('dispensario').then(data => {
            if (data == true) {
                this.visibleDispensary = true
            }
        });
        this.msgpromt = 'se perderán los cambios de la cita, ¿Desea Continuar?'


        this.cita = {
            id_especialidad: 0,
            descripcion_especialidad: 'Especialidad',
            descripcion_centro: 'Centro médico',
            disabled_centro: false,
            id_centro: 0,
            disabled_doctor: false,
            doctor: [],
            nombre_doctor: 'Cita',
            disabled_reserva: false,
            disable_mejor_cita: false,
            descripcion_doctor: 'Doctor',
            disabled_doctors: false,
            id_doctor: 0,
            state_best_date: 0,
            paciente: navParams.data.paciente.nombres + " " + navParams.data.paciente.apellidos
        }
        this.navRequest = this.navParams.data;
        if (this.navRequest.paciente.estado != 1) {
            this.showError('El usuario esta bloqueado o su afiliación esta inactiva.');
            this.navCtrl.pop()
        }
    }
    openModal() {
        let modal = this.modalCtrl.create(ModalSelectPage)
        modal.onDidDismiss(data => {
        })
        modal.present()
    }
    goToCentro() {
        this.navCtrl.push(CenterPage, this.cita)
    }
    goToQuote() {
        let BestDay = 0;

        if (this.bestDate == true) {
            BestDay = 1;
        }

        //aca van las variables de session
        this.quotesProvider.addQuote = {
            apellidos: this.navRequest.paciente.apellidos,
            celular: (typeof this.navRequest.paciente.celular == 'undefined') ? 0 : this.navRequest.paciente.celular,
            email: this.navRequest.paciente.email,
            hora_turno: this.cita.doctor.turno.FECHA_HORA,
            horario: this.cita.doctor.medico.CODIGO_HORARIO,
            nombres: this.navRequest.paciente.nombres,
            paciente: this.navRequest.paciente.hm_fk_persona,
            turno: this.cita.doctor.turno.NUMERO_TURNO,
            cedula: (typeof this.navRequest.paciente.documento == "undefined") ? this.navRequest.paciente.cedula : this.navRequest.paciente.documento
        }
        this.quotesProvider.addQuoteLocal = {
            paciente: this.navRequest.paciente.hm_fk_persona,
            hora_turno: this.cita.doctor.medico.HORA_INICIO_TURNO.split(' ')[0] + " " + this.cita.doctor.turno.FECHA_HORA.split(' ')[1],
            hora_final_turno: this.cita.doctor.medico.FECHA_FINAL_TURNO,
            horario: this.cita.doctor.medico.CODIGO_HORARIO,
            paciente_local: this.navRequest.paciente.id,
            tipo_persona: this.navRequest.tipo,
            turno: this.cita.doctor.turno.NUMERO_TURNO,
            locacion: this.cita.descripcion_centro,
            doctor: this.cita.descripcion_doctor,
            especialidad: this.cita.descripcion_especialidad,
            mejor_dia: BestDay,
            estado: 1,
            fecha_old: "",
            fecha_old_final: ""
        }
        this.quotesProvider.validateTurnData = {
            id: this.navRequest.paciente.id,
            fecha: this.cita.doctor.medico.HORA_INICIO_TURNO.split(' ')[0] + " " + this.cita.doctor.turno.FECHA_HORA.split(' ')[1],
            tipo_paciente: this.navRequest.tipo
        }
        let send_data = {
            apellidos: this.navRequest.paciente.apellidos,
            doctor: this.cita.descripcion_doctor,
            especialidad: this.cita.descripcion_especialidad,
            hora_turno: this.cita.doctor.medico.HORA_INICIO_TURNO.split(' ')[0] + " " + this.cita.doctor.turno.FECHA_HORA.split(' ')[1],
            locacion: this.cita.descripcion_centro,
            nombres: this.navRequest.paciente.nombres,
            mejor_fecha: BestDay,
            hm_fk_horario: this.cita.doctor.medico.CODIGO_HORARIO,
            numero_turno: this.cita.doctor.turno.NUMERO_TURNO,
        }
        //console.log(this.quotesProvider.addQuoteLocal);
        this.showLoading()
        this.quotesProvider.validateTurn().then(dataV => {
            this.responseValidateTurn = dataV;
            if (this.responseValidateTurn.response !== false) {
                this.quotesProvider.addQuotes().then(dataL => {
                    this.responseLocal = dataL;
                    if (this.responseLocal.response !== false) {
                        this.quotesProvider.addQuotesLocal().then(dataR => {
                            this.response = dataR;
                            if (this.response.response !== false) {
                                this.loading.dismiss();
                                let send = { datos: send_data, vista: 1 }

                                var anio = Number(this.cita.doctor.turno.FECHA_HORA.slice(0, 4));
                                var mes = Number(this.cita.doctor.turno.FECHA_HORA.slice(5, 7) - 1);
                                var dia = Number(this.cita.doctor.turno.FECHA_HORA.slice(8, 10));
                                var hora = Number(this.cita.doctor.turno.FECHA_HORA.slice(11, 13));
                                var minutos = Number(this.cita.doctor.turno.FECHA_HORA.slice(14, 16));
                                var anio2 = Number(this.cita.doctor.medico.FECHA_FINAL_TURNO.slice(0, 4));
                                var mes2 = Number(this.cita.doctor.medico.FECHA_FINAL_TURNO.slice(5, 7) - 1);
                                var dia2 = Number(this.cita.doctor.medico.FECHA_FINAL_TURNO.slice(8, 10));
                                var hora2 = Number(this.cita.doctor.medico.FECHA_FINAL_TURNO.slice(11, 13));
                                var minutos2 = Number(this.cita.doctor.medico.FECHA_FINAL_TURNO.slice(14, 16));
                                var startDate = new Date(anio, mes, dia, hora, minutos, 0, 0); // beware: month 0 = january, 11 = december
                                var endDate = new Date(anio2, mes2, dia2, hora2, minutos2, 0, 0);
                                this.calendar.hasReadWritePermission().then(data => {
                                    var title = "Cita Médica";
                                    var eventLocation = this.cita.descripcion_centro + " ECUADOR";
                                    var notes = "LUGAR: " + this.cita.descripcion_centro + "\n" + "MÉDICO: " + this.cita.descripcion_doctor + "\n" + this.cita.paciente.toUpperCase() + "\n" + "HORA: " + this.cita.doctor.turno.FECHA_HORA.split(' ')[1];
                                    var success = function (message) { alert("Success: " + JSON.stringify(message)); };
                                    var error = function (message) { alert("Error: " + message); };
                                    if (data == true) {

                                        this.calendar.createEvent(title, eventLocation, notes, startDate, endDate);

                                    } else {
                                        this.calendar.requestReadWritePermission();
                                        this.calendar.createEvent(title, eventLocation, notes, startDate, endDate);
                                    }
                                })
                                this.navCtrl.setRoot(QuotePage, send)
                            } else {
                                this.loading.dismiss();
                                this.showError(this.response.result);
                            }
                        })
                    } else {
                        this.loading.dismiss();
                        this.showError(this.responseLocal.result);
                        this.cita.disabled_reserva = false;
                    }
                })
            } else {
                this.loading.dismiss();
                this.showError(this.responseValidateTurn.result);
                this.cita.disabled_reserva = false;
            }
        });
    }
    goToEspecialidad() {
        this.navCtrl.push(SettingSpecialtyPage, this.cita)
    }
    goToDoctor() {
        this.navCtrl.push(DoctorPage, this.cita)
    }
    goToDoctors() {
        this.navCtrl.push(SettingsDoctorsPage, this.cita)
    }
    showLoading() {
        this.loading = this.loadingCtrl.create({
            content: "Registrando Cita por favor espere ...",
            dismissOnPageChange: false
        })
        this.loading.present()
    }
    showError(text: string) {
        let alert = this.alertCtrl.create({
            title: "Error",
            subTitle: text,
            buttons: ['OK']
        })
        alert.present(prompt)
    }
    goToHome() {
        this.presentConfirm('Aviso', this.msgpromt, 'cancelar', 'aceptar', 2, HomePage)
        // this.navCtrl.setRoot(HomePage)
    }
    goToQuotes() {
        this.presentConfirm('Aviso', this.msgpromt, 'cancelar', 'aceptar', 2, MenuQuotesPage)

        // this.navCtrl.setRoot(MenuQuotesPage)
    }
    goToSpecialite() {
        this.presentConfirm('Aviso', this.msgpromt, 'cancelar', 'aceptar', 2, MenuSpecialtiesPage)
        // this.navCtrl.setRoot(MenuSpecialtiesPage)
    }
    goToFavorite() {
        this.presentConfirm('Aviso', this.msgpromt, 'cancelar', 'aceptar', 2, FavoriteDoctorsPage)
        // this.navCtrl.setRoot(FavoriteDoctorsPage)
    }
    gotoQuotesDispensary() {
        this.presentConfirm('Aviso', this.msgpromt, 'cancelar', 'aceptar', 2, QuotesDispensaryPage)

        // this.navCtrl.push(QuotesDispensaryPage);
    }

    ionViewDidLoad() {
        this.navBar.backButtonClick = (e: UIEvent) => {

            this.presentConfirm('Aviso', this.msgpromt, 'cancelar', 'aceptar', 1, '')
        }
    }

    presentConfirm(title, message, text_button_left, text_button_right, tipo, controller) {
        if (this.cita.descripcion_especialidad != 'Especialidad'
            || this.cita.descripcion_centro != 'Centro médico'
            || this.cita.nombre_doctor != 'Cita'
            || this.cita.descripcion_doctor != 'Doctor') {
            let alert = this.alertCtrl.create({
                title: title,
                message: message,
                buttons: [
                    {
                        text: text_button_left,
                        role: 'cancel',
                        handler: () => {
                            console.log('Cancel clicked');
                        }
                    },
                    {
                        text: text_button_right,
                        handler: () => {
                            if (tipo == 1) {
                                this.navCtrl.pop()
                            } else if (tipo == 2) {
                                this.navCtrl.setRoot(controller)
                            }
                        }
                    }
                ]
            });
            alert.present();
        }else{
            if (tipo == 1) {
                this.navCtrl.pop()
            } else if (tipo == 2) {
                this.navCtrl.setRoot(controller)
            }
        }
        

    }

    acceptTerms() {
        if (this.bestDate === true) {
            this.cita.state_best_date = 1;
            let alert = this.alertCtrl.create({
                title: 'Información',
                subTitle: 'Encontraremos una mejor cita para ti en caso de una cancelación ',
                buttons: [
                    {
                        text: 'Si',
                        handler: data => {
                            this.bestDate = true
                        }
                    },
                    {
                        text: 'no',
                        handler: data => {
                            this.bestDate = false;
                        }
                    }
                ]
            })
            alert.present(prompt)
        } else {
            this.cita.state_best_date = 0;
        }
    }
}
