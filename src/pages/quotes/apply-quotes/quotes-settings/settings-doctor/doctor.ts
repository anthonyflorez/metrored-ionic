import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, Loading, AlertController } from 'ionic-angular';
import { QuotesProvider } from "../../../../../providers/quotes/quotes";
import { ServiceLoading } from "../../../../../providers/service-loading/service-loading";
@Component({
    selector: 'page-doctor',
    templateUrl: 'doctor.html',
    providers: [QuotesProvider]
})
export class DoctorPage{
    date:string;
    medicos:any;
    medicosResult:any;
    keys:any;
    navRequest:any;
    loading:Loading;
    alert_present:boolean
    title: string = null
    visibleContent: boolean = false
    visibleTitle: boolean = false
    constructor(public navCtrl: NavController, public navParams: NavParams, public medicosProviders: QuotesProvider, private loadingCtrl: LoadingController,
                private alertCtrl: AlertController, private serviceloading: ServiceLoading){
        this.navRequest = navParams.data;
        this.load()
    }
    load(){
        this.showLoading()
        this.medicosProviders.dataSetMedico = {
            nombre: this.navParams.data.id_centro,
            id: this.navParams.data.id_doctor,
            fk_cita: this.navParams.data.id_especialidad
        }
        this.medicosProviders.getQuoteDoctorId().then(data => {
            this.medicos = data;
            if(typeof this.medicos.result == "string"){
               this.visibleTitle = true
               this.title = this.medicos.result
            }
            else{
                this.visibleContent = true
                this.medicos = this.medicos.result;
                this.keys = Object.keys(this.medicos);
            }
            this.loading.dismiss()
        }).catch(err=>{
            this.loading.dismiss();
                if(this.alert_present==false){
                this.serviceloading.Loading();
                this.navCtrl.pop()
                this.alert_present=true
            }
        })
    }
    validateDuracion(time){
        let slice = time.split(' ');
        let timeSlide = slice[1].split(':');
        let returnTime;
        if(timeSlide[0] > 0){
            returnTime = timeSlide[0]+":"+timeSlide[1]+" Hora";
        }
        else{
            returnTime = timeSlide[1]+" Minutos";
        }
        return returnTime;
    }

    setDoctor(doctor){
        this.navRequest.nombre_doctor = doctor.medico.FM;
        this.navRequest.doctor = doctor;
        this.navRequest.disabled_reserva = true;
        this.navRequest.disable_mejor_cita = true;
        this.navCtrl.pop()
    }
    showLoading(){
      this.loading = this.loadingCtrl.create({
          content: "Cargando información\n espere por favor...",
          dismissOnPageChange: false
      })
      this.loading.present()
    }

    showError(text:string){
        let alert = this.alertCtrl.create({
            title: "Error",
            subTitle: text,
            buttons:['OK']
        })
        alert.present(prompt)
    }
}