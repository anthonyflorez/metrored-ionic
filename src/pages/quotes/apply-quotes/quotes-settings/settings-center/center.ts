import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, Loading, AlertController } from 'ionic-angular';
import { CentersProvider } from "../../../../../providers/centers/centers";
import { Geolocation } from "@ionic-native/geolocation";
import { NativeGeocoder, NativeGeocoderForwardResult } from '@ionic-native/native-geocoder';
import { ServiceLoading } from "../../../../../providers/service-loading/service-loading";

@Component({
    selector: 'page-center',
    templateUrl: 'center.html',
    providers: [CenterPage],
})
export class CenterPage{
    public lat:any;
    public lon:any;
    public centers:any;
    public centers2:any;
    public latitud:number;
    public longitud:number;
    public exactitud:any;
    public searchCountry:boolean;
    private radio:number;
    private pushbutton:boolean;
    public selected:boolean
    loading:Loading;
    alert_present:boolean
    private navRequest:any
    constructor(public navCtrl: NavController, public navParams: NavParams, private centerProvider: CentersProvider, private geolocation: Geolocation,
                private nativeGeocoder: NativeGeocoder, private loadingCtrl:LoadingController, private alertCtrl:AlertController, private serviceloading: ServiceLoading){
                    geolocation.getCurrentPosition().then((data) => {
                    this.latitud = data.coords.latitude;
                    this.longitud = data.coords.longitude;
                    });
        // geolocation.getCurrentPosition().then((data) => {
        //     this.exactitud=data.coords.accuracy;
        //     for (var i = 0; i < 100; i++) {
        //         if(this.exactitud<=400){
        //             this.latitud = data.coords.latitude;
        //             this.longitud = data.coords.longitude;
        //         }
        //     }
        // });
        this.selected=false;
        this.navRequest = navParams.data;
        this.searchCountry = false;
        this.pushbutton=false;
        this.radio = 6378; //esto esta en Km
        this.load();
    }

    load(){
        this.showLoading()
        this.centerProvider.especialidad = this.navRequest.id_especialidad
        this.centerProvider.getCentros().then(data => {
               this.centers = data;
               
               let inCenter = "";
               this.centers.forEach(center => {
                    center.distance = 999999999999999;
                    inCenter = center.DESCRIPCION +","+ inCenter;
               });
               this.centerProvider.centro = inCenter.slice(0, -1)
               this.centerProvider.getCenterIn().then(dataLL => {
               })
            this.loading.dismiss()
        }).catch(err=>{
            this.loading.dismiss();
            this.serviceloading.Loading();
            this.navCtrl.pop()
     })
        this.centerProvider.getCentros2().then(data => {
            this.centers2 = data;
            let inCenter = "";
            this.centers2.forEach(center => {
                 center.distance = 999999999999999;
                 inCenter = center.DESCRIPCION +","+ inCenter;
            });
            this.centerProvider.centro = inCenter.slice(0, -1)
            this.centerProvider.getCenterIn().then(dataLL => {
             console.log(dataLL);
            })
         this.loading.dismiss()
         }).catch(err=>{
            this.loading.dismiss();
            if(this.alert_present==false){
            this.serviceloading.Loading();
            this.navCtrl.pop()
            this.alert_present=true
        }
     })
    }

    private getDistance(lat1, lat2, lon1, lon2){
        var rad = function(x) {return x*Math.PI/180;}
        var R = 6378.137; //Radio de la tierra en km
        var dLat = rad( lat2 - lat1 );
        var dLong = rad( lon2 - lon1 );
        var a = Math.sin(dLat/2) * Math.sin(dLat/2) + Math.cos(rad(lat1)) * Math.cos(rad(lat2)) * Math.sin(dLong/2) * Math.sin(dLong/2);
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        var d = R * c;
        return d.toFixed(3); //Retorna tres decimales  
    }

    private setDistance(){
        this.centers2.forEach(center => {
            center.distance=this.getDistance(center.latitud, this.latitud, center.longitud,this.longitud);
          })
    }

    private order(data, key, orden) {
        return data.sort(function (a, b) {

            var x = parseInt(a[key],10),
            y =parseInt(b[key],10);
    
            if (orden === 'asc') {
                return ((x < y) ? -1 : ((x > y) ? 1 : 0));
            }
    
            if (orden === 'desc') {
                return ((x > y) ? -1 : ((x < y) ? 1 : 0));
            }
        });
    }

    reorder(){
        this.setDistance();
        this.order(this.centers2, 'distance', 'asc')
        console.log(this.centers2);
        this.selected=true;
    }

    private setCurrentPosition() {
        if ("geolocation" in navigator) {
          navigator.geolocation.getCurrentPosition((position) => {
            this.latitud = position.coords.latitude;
            console.log(this.latitud);
            this.longitud = position.coords.longitude;
           
          });
        }
        return this.latitud;
      }    
    addCentro(centro){
        this.navRequest.descripcion_centro = centro.DESCRIPCION
        //this.navRequest.id_centro = centro.FK_INSTITUCION
        this.navRequest.id_centro = centro.PK_CODIGO
        this.navRequest.disabled_doctors = true;
        this.navCtrl.pop()
    }
    showLoading(){
      this.loading = this.loadingCtrl.create({
          content: "Cargando información\n espere por favor...",
          dismissOnPageChange: false
      })
      this.loading.present()
    }

    showError(text:string){
        let alert = this.alertCtrl.create({
            title: "Error",
            subTitle: text,
            buttons:['OK']
        })
        alert.present(prompt)
    }
}
