import { Component, ViewChild, ElementRef } from '@angular/core';
import { Content } from 'ionic-angular';
import { Storage } from "@ionic/storage";
import { NavController, NavParams, LoadingController, Loading, AlertController, ModalController } from 'ionic-angular';
import { QuotePage } from "../quote/quote";
import { CenterPage } from "../apply-quotes/quotes-settings/settings-center/center";
import { SettingSpecialtyPage } from "../apply-quotes/quotes-settings/settings-specialty/settings-specialty";
import { DoctorPage } from "../apply-quotes/quotes-settings/settings-doctor/doctor";
import { QuotesProvider } from "../../../providers/quotes/quotes";
import { CentersProvider } from "../../../providers/centers/centers";
import { SpecialtyProvider } from "../../../providers/specialty/specialty";
import { ModalSelectPage } from "../../modal-select/modal-select";
import { HomePage } from "../../home/home";
import { MenuQuotesPage } from "../menu-quotes/menu-quotes";
import { MenuSpecialtiesPage } from "../../specialties/menu-specialties";
import { FavoriteDoctorsPage } from "../../favorite-doctors/favorite-doctors";
import { SearchQuotesPage } from "../search-quotes/search-quotes";
import { MenuQuotesDispensaryPage } from "../../quotes-dispensary/menu-quotes-dispensary/menu-quotes-dispensary";
import { Calendar } from '@ionic-native/calendar';
import { SettingsDoctorsPage } from "../apply-quotes/quotes-settings/settings-doctors/settings-doctors";
import { Keyboard } from '@ionic-native/keyboard';
/**
 * Generated class for the ReschedulePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@Component({
    selector: 'page-reschedule-push',
    templateUrl: 'reschedule-push.html',
    providers: [QuotesProvider, CentersProvider, SpecialtyProvider]
})
export class ReschedulePushPage {
    @ViewChild('map') mapElement: ElementRef;
    @ViewChild(Content) content: Content;
    @ViewChild('note') container: ElementRef;
    public navRequest: any;
    public cita: {
        descripcion_especialidad: string,
        id_especialidad: number,
        descripcion_centro: string,
        disabled_centro: boolean,
        id_centro: number,
        disabled_doctor: boolean,
        doctor: any,
        nombre_doctor: string,
        disabled_reserva: boolean,
        descripcion_doctor: string
        disabled_doctors: boolean,
        fecha_old: string,
        id_doctor: number,
        paciente: string,
        HORA_INICIO_TURNO: string,
        CODIGO_HORARIO_NEW: number,
        NUMERO_TURNO: number
    };
    loading: Loading;
    responseValidateTurn: any;
    responsevalidatedis: any;
    responseLocal: any;
    response: any;
    responseCenter: any
    responsSpecialite: any
    responseBeforeQuote: any
    visibleDispensary: boolean = false
    responseDoctor: any
    view: boolean
    notas: any
    constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController,
        public alertCtrl: AlertController, private quotesProvider: QuotesProvider, public modalCtrl: ModalController,
        private centerProvider: CentersProvider, private specialtyProvider: SpecialtyProvider, private storage: Storage,
        private calendar: Calendar, public elRef: ElementRef, private keyboard: Keyboard) {
        storage.get('dispensario').then(data => {
            if (data == true) {
                this.visibleDispensary = true
            }
        });
        this.keyboard.onKeyboardShow().subscribe(() => {
            this.view = false;
        });
        this.keyboard.onKeyboardHide().subscribe(() => {
            this.view = true;
        });

        console.log(navParams)
        this.navRequest = this.navParams.data.data;
        let dateString = this.navRequest.HORA_INICIO_TURNO;
        let i = 0;
        while (i < 5) {
            if (dateString.match("-") != null) {
                if (dateString.match("-").length != 0) {
                    dateString = dateString.replace("-", "/");
                } else {
                    break;
                }
            } else {
                break;
            }
            i++;
        }
        var date = new Date(Date.parse(dateString));
        let days = [
            'Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'
        ]
        let months = [
            'Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'
        ]
        this.cita = {
            id_especialidad: 0,
            descripcion_especialidad: this.navRequest.ESPECIALIDAD,
            descripcion_centro: this.navRequest.DESCRIPCION,
            disabled_centro: true,
            id_centro: 0,
            disabled_doctor: true,
            doctor: {},
            nombre_doctor: days[date.getDay()] + " " + date.getDate() + " DE " + months[date.getMonth()],
            disabled_reserva: true,
            descripcion_doctor: this.navRequest.NOMBRE_MEDICO_NEW,
            disabled_doctors: true,
            fecha_old: this.navRequest.FECHA_INICIAL_OLD,
            id_doctor: 0,
            paciente: this.navRequest.NOMBRES + " " + this.navRequest.APELLIDOS,
            HORA_INICIO_TURNO: this.navRequest.HORA_INICIO_TURNO,
            CODIGO_HORARIO_NEW: this.navRequest.CODIGO_HORARIO_NEW,
            NUMERO_TURNO: this.navRequest.NUMERO_TURNO
        }

        this.specialtyProvider.especialidad = this.navRequest.ESPECIALIDAD
        console.log(navParams.data.data.DESCRIPCION)
        console.log(this.cita.descripcion_centro)
        this.specialtyProvider.getSpecialiteByName().then(data => {
            this.responsSpecialite = data

            this.cita.descripcion_especialidad = this.responsSpecialite.DESCRIPCION
            this.cita.id_especialidad = this.responsSpecialite.PK_CODIGO
            // this.notas=this.navRequest.NOTAS;

            this.centerProvider.centro = this.cita.descripcion_centro;
            this.centerProvider.getCentroByName().then(dataC => {
                this.responseCenter = dataC
                this.cita.id_centro = this.responseCenter.PK_CODIGO
                this.cita.descripcion_centro = this.responseCenter.DESCRIPCION
                this.quotesProvider.dataMedico = {
                    id_centro: this.cita.id_centro,
                    id_especialidad: this.cita.id_especialidad,
                    nombre: navParams.data.data.NOMBRE_MEDICO_NEW
                }
                this.quotesProvider.getMedicoName().then(dataM => {
                    this.responseDoctor = dataM
                    this.cita.descripcion_doctor = this.responseDoctor.result.NOMBRE_MEDICO
                    this.cita.id_doctor = this.responseDoctor.result.CODIGO_MEDICO
                })
            })
        })
        // this.cita.disabled_centro = true
        // this.cita.disabled_doctor = true,
        // this.cita.nombre_doctor = days[date.getDay()]+" "+ date.getDate()+" DE "+months[date.getMonth()];
        // this.cita.disabled_centro = true
        // this.cita.disabled_reserva = true
        // this.cita.disabled_doctors = true

        console.log("****************")
        console.log(this.navRequest);
        console.log("****************")
    }

    //   ionViewDidLoad() {

    //   }
    //   openModal(){
    //     let modal = this.modalCtrl.create(ModalSelectPage)
    //     modal.onDidDismiss(data => {
    //         console.log(data);
    //     })
    //     modal.present()
    //   }
    //   goToCentro(){
    //       this.navCtrl.push(CenterPage, this.cita)
    //   }
    //   goToDoctors(){

    //     this.navCtrl.push(SettingsDoctorsPage, this.cita)
    //     console.log(this.navCtrl)

    //   }
    goToQuote() {

        //   this.showLoading()
        let countDoctor = 1
        if (countDoctor > 0) {
            // //aca van las variables de session
            this.quotesProvider.addQuote = {
                apellidos: this.navRequest.APELLIDOS,
                celular: this.navRequest.CELULAR,
                email: this.navRequest.EMAIL,
                hora_turno: this.cita.HORA_INICIO_TURNO,
                horario: this.cita.CODIGO_HORARIO_NEW,
                nombres: this.navRequest.NOMBRES,
                paciente: this.navRequest.OB_HM_FK_PERSONA_OLD,
                turno: this.cita.NUMERO_TURNO,
                cedula: this.navRequest.CEDULA
            }
            this.quotesProvider.addQuoteLocal = {
                paciente: this.navRequest.OB_HM_FK_PERSONA_OLD,
                hora_turno: this.cita.HORA_INICIO_TURNO,
                hora_final_turno: this.navRequest.HORA_FINAL_TURNO,
                horario: this.cita.CODIGO_HORARIO_NEW,
                paciente_local: this.navRequest.OB_HM_FK_PERSONA_OLD,
                tipo_persona: 1,
                turno: this.cita.NUMERO_TURNO,
                locacion: this.cita.descripcion_centro,
                doctor: this.cita.descripcion_doctor,
                especialidad: this.cita.descripcion_especialidad,
                mejor_dia: 0,
                estado: 1,
                fecha_old: this.navRequest.FECHA_INICIAL_OLD,
                fecha_old_final: this.navRequest.FECHA_FINAL_OLD
            }
            this.quotesProvider.validateTurnData = {
                id: this.navRequest.OB_HM_FK_PERSONA_OLD,
                fecha: this.cita.HORA_INICIO_TURNO,
                tipo_paciente: this.navRequest.tipo_persona
            }

            this.quotesProvider.beforeQuote = {
                horario: this.navRequest.OB_HM_FK_HORARIO_OLD,
                id: this.navRequest.ID,
                turno: this.navRequest.NUMERO_TURNO_OLD
            }
            let send_data = {
                apellido: this.navRequest.apellidos,
                doctor: this.cita.descripcion_doctor,
                especialidad: this.cita.descripcion_especialidad,
                hora_turno: this.cita.HORA_INICIO_TURNO.split(' ')[0] + " " + this.cita.HORA_INICIO_TURNO.split(' ')[1],
                locacion: this.cita.descripcion_centro,
                nombres: this.navRequest.NOMBRES,
                notas: this.notas
            }
            this.quotesProvider.setNotasData = {
                horario: this.cita.CODIGO_HORARIO_NEW,
                notas: this.notas,
                turno: this.navRequest.NUMERO_TURNO_OD
            }

            this.quotesProvider.validateDis(this.cita.CODIGO_HORARIO_NEW, this.cita.NUMERO_TURNO).then(data1 => {
                this.responsevalidatedis = data1
                console.log(data1)
                if (this.responsevalidatedis.response != false) {
                    this.quotesProvider.validateTurn().then(dataV => {
                        this.responseValidateTurn = dataV;
                        console.log("*************")
                        console.log(dataV)
                        console.log("**************")
                        if (this.responseValidateTurn.response != false) {
                            this.quotesProvider.rescheduleQuote().then(dataB => {
                                this.responseBeforeQuote = dataB
                                console.log(this.responseBeforeQuote)
                                if (this.responseBeforeQuote.response != false) {
                                    this.quotesProvider.addQuotesLocal().then(dataL => {
                                        console.log(dataL)
                                        this.responseLocal = dataL;
                                        if (this.responseLocal.reponse != false) {
                                            this.quotesProvider.addQuotes().then(dataR => {
                                                this.response = dataR;
                                                if (this.response.response == true) {
                                                    this.quotesProvider.setNotas2().then(data => {
                                                        console.log("se envio")
                                                    })
                                                    console.log(this.navRequest)
                                                    if (this.navRequest.HORA_INICIO_TURNO != null && this.navRequest.FECHA_HORA_FINAL != null) {
                                                        var anio = Number(this.navRequest.HORA_INICIO_TURNO.slice(0, 4));
                                                        var mes = Number(this.navRequest.HORA_INICIO_TURNO.slice(5, 7)) - 1;
                                                        var dia = Number(this.navRequest.HORA_INICIO_TURNO.slice(8, 10));
                                                        var hora = Number(this.navRequest.HORA_INICIO_TURNO.slice(11, 13));
                                                        var minutos = Number(this.navRequest.HORA_INICIO_TURNO.slice(14, 16));
                                                        var anio2 = Number(this.navRequest.FECHA_HORA_FINAL.slice(0, 4));
                                                        var mes2 = Number(this.navRequest.FECHA_HORA_FINAL.slice(5, 7) - 1);
                                                        var dia2 = Number(this.navRequest.FECHA_HORA_FINAL.slice(8, 10));
                                                        var hora2 = Number(this.navRequest.FECHA_HORA_FINAL.slice(20, 22));
                                                        var minutos2 = Number(this.navRequest.FECHA_HORA_FINAL.slice(23, 26));
                                                        //fecha nueva
                                                        var newstartDate = new Date(anio2, mes2, dia2, hora2, minutos2, 0, 0); // beware: month 0 = january, 11 = december
                                                        var newendDate = new Date(anio2, mes2, dia2, hora2 + 1, minutos2, 0, 0);
                                                        //Fecha vieja
                                                        var startDate = new Date(anio, mes, dia, hora, minutos, 0, 0); // beware: month 0 = january, 11 = december
                                                        var endDate = new Date(anio, mes, dia, hora + 1, minutos, 0, 0);

                                                        var title = "Cita Médica";
                                                        var eventLocation = this.cita.descripcion_centro + " ECUADOR";
                                                        var notes = "LUGAR: " + this.cita.descripcion_centro + "\n" + "MÉDICO: " + this.cita.descripcion_doctor + "\n" + "PACIENTE: " + this.cita.paciente.toUpperCase() + "\n" + "HORA: " + this.navRequest.FECHA_INICIAL_NEW.split(' ')[1];
                                                        var success = function (message) { alert("Success: " + JSON.stringify(message)); };
                                                        var error = function (message) { alert("Error: " + message); };
                                                        this.calendar.deleteEvent('', '', '', startDate, endDate);
                                                        this.calendar.findEvent("", "", "", newstartDate, newendDate).then(data => {
                                                            console.log(data)
                                                            if (data == "") {
                                                                this.calendar.createEvent(title, eventLocation, notes, newstartDate, newendDate);
                                                            }
                                                        });
                                                    }
                                                    let send = { datos: send_data, vista: 1 }
                                                    this.navCtrl.setRoot(QuotePage, send)



                                                } else {
                                                    this.loading.dismiss();
                                                    this.showError(this.response.result);
                                                }
                                            })
                                        } else {
                                            this.loading.dismiss();
                                            this.showError(this.responseLocal.result);
                                            this.cita.disabled_reserva = false;
                                        }
                                    })
                                }
                            })
                        } else {
                            this.loading.dismiss();
                            this.showError(this.responseValidateTurn.result);
                            this.cita.disabled_reserva = false;
                        }
                    });
                } else {
                    this.showError(this.responsevalidatedis.result);
                    this.navCtrl.setRoot(HomePage)
                }
            }).catch(error => {
                console.log(error)
            })

        }
        else {
        
            this.navCtrl.setRoot(SearchQuotesPage)
        }
    }
    showError(text: string) {
        let alert = this.alertCtrl.create({
            title: "Error",
            subTitle: text,
            buttons: ['OK']
        })
        alert.present(prompt)
    }
    //   goToEspecialidad(){
    //       this.navCtrl.push(SettingSpecialtyPage, this.cita)
    //   }
    //   goToDoctor(){
    //       this.cita.fecha_old=this.navRequest.hora_turno;
    //       this.navCtrl.push(DoctorPage, this.cita)
    //   }
    //   showLoading(){
    //     this.loading = this.loadingCtrl.create({
    //         content: "Registrando Cita por favor espere ...",
    //         dismissOnPageChange: false
    //     })
    //     this.loading.present()
    //   }
    //   showError(text:string){
    //       let alert = this.alertCtrl.create({
    //           title: "Error",
    //           subTitle: text,
    //           buttons:['OK']
    //       })
    //       alert.present(prompt)
    //   }
    //   goToHome(){
    //       this.navCtrl.setRoot(HomePage)
    //   }
    //   goToQuotes(){
    //       this.navCtrl.setRoot(MenuQuotesPage)
    //   }
    //   goToSpecialite(){
    //       this.navCtrl.setRoot(MenuSpecialtiesPage)
    //   }
    //   goToFavorite(){
    //       this.navCtrl.setRoot(FavoriteDoctorsPage)
    //   }
    //   gotoQuotesDispensary(){
    //     this.navCtrl.setRoot(MenuQuotesDispensaryPage);
    //   }
    //   scroll(){
    //     let el=this.elRef.nativeElement.querySelector('note');
    //     // this.setCaretPosition(el,0);
    //     setTimeout(()=>{
    //     this.content.scrollTo(0,450,600);
    //     },0);

    //     console.log(this.content._fTop);
    //     console.log(this.content._ftrHeight)
    //     console.log(this.content.scrollTop)
    // }
}
