import { Component, ViewChild, ElementRef } from '@angular/core';
import { NavController, NavParams, LoadingController, Loading, AlertController, Alert,
         ToastController, Platform, ActionSheetController, ModalController } from 'ionic-angular';
import { NativeGeocoder, NativeGeocoderForwardResult } from '@ionic-native/native-geocoder';
import { Keyboard } from '@ionic-native/keyboard';
import { QuotesProvider } from "../../../providers/quotes/quotes";
import { SearchQuotesPage } from "../search-quotes/search-quotes";
import { Storage } from "@ionic/storage";
import { Content } from 'ionic-angular';
import { File } from "@ionic-native/file";
import { FileTransfer, FileTransferObject } from "@ionic-native/file-transfer";
import { FilePath } from "@ionic-native/file-path";
import { Camera } from "@ionic-native/camera";
import { Calendar } from '@ionic-native/calendar';
import { HomePage } from "../../home/home";
import { MenuQuotesPage } from "../menu-quotes/menu-quotes";
import { MenuSpecialtiesPage } from "../../specialties/menu-specialties";
import { FavoriteDoctorsPage } from "../../favorite-doctors/favorite-doctors";
import { ModalImagePage } from "../../modal-image/modal-image";
import { ModalImageQuotePage } from "../../modal-image-quote/modal-image-quote";
import { MenuQuotesDispensaryPage } from "../../quotes-dispensary/menu-quotes-dispensary/menu-quotes-dispensary";
import { CentersProvider } from "../../../providers/centers/centers";
import {
 GoogleMaps,
 GoogleMap,
 CameraPosition,
} from '@ionic-native/google-maps';
import { ReschedulePage } from "../reschedule/reschedule";
declare var google;
declare var cordova: any;
@Component({
    selector: 'page-quote',
    templateUrl: 'quote.html',
    providers: [GoogleMaps, QuotesProvider, CentersProvider]
})
export class QuotePage{
    @ViewChild('map') mapElement: ElementRef;
    @ViewChild(Content) content: Content;
    @ViewChild('note') container: ElementRef;
    
    map:any;
    lat:any;
    lon:any;
    lugar : string;
    private navRequest:any;
    response: any
    loading:Loading
    reultMedicos:any
    visibleButtons: boolean = false
    visibleScreen: boolean = false
    visibleTextArea: boolean = false
    visibleMap:boolean = false
    visibleButtonTextArea:boolean = false
    lastImage:string = null
    visibleUploadButtons:boolean = false
    visibleInfoTextArea:boolean = false
    textArea:any = null
    rate:any
    notas:string
    textareareschedule:boolean
    view:boolean=true
    visibleDispensary: boolean = false
    images:Array<{url:string}>;
    registerQuote: boolean = false
    responseCenter: any
    constructor( private keyboard: Keyboard,public navCtrl: NavController, public navParams: NavParams, private nativeGeocoder: NativeGeocoder, private googleMaps: GoogleMaps, public calendar: Calendar,
                private quotesProvider: QuotesProvider, private loadingCtrl: LoadingController, private alertCtrl: AlertController,
                private camera: Camera, private transfer: FileTransfer, private file: File, private filePath: FilePath,
                public actionSheetCtrl: ActionSheetController, public toastCtrl: ToastController, private platform: Platform, private storage: Storage,
                public modalCtrl:ModalController, private centersProviders: CentersProvider,public elRef: ElementRef){
        storage.get('dispensario').then(data => {
          if(data == true){
            this.visibleDispensary = true
          }
        });

        this.navRequest = navParams.data.datos;
        this.keyboard.disableScroll(true)
        this.centersProviders.centro = this.navRequest.locacion
        this.centersProviders.getCentroByNameLocal().then(data => {
            this.responseCenter = data
            this.loadMap();
        })
        this.keyboard.onKeyboardShow().subscribe(() => {
           this.view=false;
        }); 
         this.keyboard.onKeyboardHide().subscribe(() => {
            this.view=true;
        }); 
       if(this.navParams.data.textarea==false){
        this.textareareschedule=this.navParams.data.textarea
       }else{
        this.textareareschedule=true
       }
       this.notas=this.navParams.data.datos.notas;
       console.log(this.navParams.data.datos.notas)
       
        this.textArea = this.navRequest.notas
        this.storage.get('persona').then(data => {
            this.quotesProvider.dataSetMedico = {
                fk_cita: 0,
                id: data.id,
                nombre: this.navRequest.doctor.toLowerCase()
            }
            this.quotesProvider.getMedicoFavorito().then(data => {
                this.reultMedicos = data;
                if(this.reultMedicos.response === true){
                    this.rate = 1;
                }
            })
        })
        if(navParams.data.vista == 2){
            this.visibleButtons = true
            this.visibleTextArea = true
            this.visibleMap = true
            this.visibleButtonTextArea = true
            this.visibleInfoTextArea = true
        }
        if(navParams.data.vista == 3){
            this.visibleUploadButtons = true
            this.visibleTextArea = true
        }
        if(navParams.data.vista == 1){
            this.visibleButtonTextArea = true
            this.visibleTextArea = true
            this.visibleMap = true
            this.visibleInfoTextArea = true
            this.registerQuote = true
        }
    }
    public presentActionSheet(){
        let actionSheet = this.actionSheetCtrl.create({
            title: 'Seleccionar imagen',
            buttons: [{
                text: 'Cargar desde la galeria',
                handler: () => {
                   this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
                }
            },
            {
                text: 'Desde la camara',
                handler: () => {
                    this.takePicture(this.camera.PictureSourceType.CAMERA)
                }
            },
            {
                text: 'Cancelar',
                role: 'cancel'
            }]
        })
        actionSheet.present()
    }
    takePicture(sourceType){
        let options = {
            quality: 100,
            sourceType: sourceType,
            saveToPhotoAlbum: false,
            correctOrientation: true
        }
        this.camera.getPicture(options).then((imagePath) => {
            if(this.platform.is('android') && sourceType == this.camera.PictureSourceType.PHOTOLIBRARY){
                this.filePath.resolveNativePath(imagePath).then(filePath => {
                    let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1)
                    let currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1)
                    currentName = currentName.substr(0, currentName.lastIndexOf('?'))
                    this.copyFileToLocalDir(correctPath, currentName, this.createFileName())
                })
            }else{
                let currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
                let correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
                this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
            }
        }, error => {
             this.presentToast('Error al leer las imagenes');
        })
    }
    private createFileName() {
        var d = new Date(),
        n = d.getTime(),
        newFileName =  n + ".jpg";
        return newFileName;
    }
    private copyFileToLocalDir(namePath, currentName, newFileName){
        this.file.copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(sucess => {
            let modal = this.modalCtrl.create(ModalImagePage, {file: newFileName, id: this.navRequest.id, images: this.images})
            modal.present()
            //this.lastImage = newFileName
        },error => {
            this.presentToast('Error al procesar las imagenes')
        }).catch(error => {
        })
    }
    private presentToast(text){
        let toast = this.toastCtrl.create({
            message: text,
            duration: 3000,
            position: 'top'
        })
        toast.present()
    }
    public pathForImage(img) {
        if (img === null) {
            return '';
        } else {
            return cordova.file.dataDirectory + img;
        }
    }
    ionViewDidLoad(){
    }
    onModelChange(event){
        this.storage.get('persona').then(data => {
            this.quotesProvider.dataSetMedico = {
                id:data.id,
                nombre: this.navRequest.doctor.toLowerCase(),
                fk_cita: this.navRequest.hm_fk_horario
            };
            this.quotesProvider.setMedico().then(data => {
            })
        })
    }
    loadMap(){
        this.lat = this.responseCenter.result.lat
        this.lon = this.responseCenter.result.lng
        let geocoder = new google.maps.Geocoder();
        let mapa = this.mapElement.nativeElement
        let descripcion = this.navRequest.descripcion_centro
        console.log(this.lat)
        let latLng = new google.maps.LatLng(this.lat, this.lon);
        let mapOptions = {
            center: latLng,
            zoom: 15,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            styles:[
                {
                    "featureType": "all",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "weight": "2.00"
                        }
                    ]
                },
                {
                    "featureType": "all",
                    "elementType": "geometry.stroke",
                    "stylers": [
                        {
                            "color": "#9c9c9c"
                        }
                    ]
                },
                {
                    "featureType": "all",
                    "elementType": "labels.text",
                    "stylers": [
                        {
                            "visibility": "on"
                        }
                    ]
                },
                {
                    "featureType": "landscape",
                    "elementType": "all",
                    "stylers": [
                        {
                            "color": "#f2f2f2"
                        }
                    ]
                },
                {
                    "featureType": "landscape",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "color": "#ffffff"
                        }
                    ]
                },
                {
                    "featureType": "landscape.man_made",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "color": "#ffffff"
                        }
                    ]
                },
                {
                    "featureType": "poi",
                    "elementType": "all",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "road",
                    "elementType": "all",
                    "stylers": [
                        {
                            "saturation": -100
                        },
                        {
                            "lightness": 45
                        }
                    ]
                },
                {
                    "featureType": "road",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "color": "#eeeeee"
                        }
                    ]
                },
                {
                    "featureType": "road",
                    "elementType": "labels.text.fill",
                    "stylers": [
                        {
                            "color": "#7b7b7b"
                        }
                    ]
                },
                {
                    "featureType": "road",
                    "elementType": "labels.text.stroke",
                    "stylers": [
                        {
                            "color": "#ffffff"
                        }
                    ]
                },
                {
                    "featureType": "road.highway",
                    "elementType": "all",
                    "stylers": [
                        {
                            "visibility": "simplified"
                        }
                    ]
                },
                {
                    "featureType": "road.arterial",
                    "elementType": "labels.icon",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "transit",
                    "elementType": "all",
                    "stylers": [
                        {
                            "visibility": "off"
                        }
                    ]
                },
                {
                    "featureType": "water",
                    "elementType": "all",
                    "stylers": [
                        {
                            "color": "#46bcec"
                        },
                        {
                            "visibility": "on"
                        }
                    ]
                },
                {
                    "featureType": "water",
                    "elementType": "geometry.fill",
                    "stylers": [
                        {
                            "color": "#c8d7d4"
                        }
                    ]
                },
                {
                    "featureType": "water",
                    "elementType": "labels.text.fill",
                    "stylers": [
                        {
                            "color": "#070707"
                        }
                    ]
                },
                {
                    "featureType": "water",
                    "elementType": "labels.text.stroke",
                    "stylers": [
                        {
                            "color": "#ffffff"
                        }
                    ]
                }
            ]
        }
        this.map = new google.maps.Map(mapa, mapOptions);
        let marker = new google.maps.Marker({
            position: latLng,
            map: this.map,
            title: descripcion
        });
        let info = new google.maps.InfoWindow({
            content: '<div>'+descripcion+'</div>'
        })
        marker.addListener('click', () => {
            info.open(this.map, marker);
        })
        // this.nativeGeocoder.forwardGeocode(this.navRequest.locacion+",ecuador").then((coordinates: NativeGeocoderForwardResult) => {
        //    this.lat = coordinates.latitude;
        //    this.lon = coordinates.longitude;
        //    let latLng = new google.maps.LatLng(this.lat, this.lon);
        //    let mapOptions = {
        //     center: latLng,
        //     zoom: 15,
        //     mapTypeId: google.maps.MapTypeId.ROADMAP,
        //     styles:[
        //             {
        //                 "featureType": "all",
        //                 "elementType": "geometry.fill",
        //                 "stylers": [
        //                     {
        //                         "weight": "2.00"
        //                     }
        //                 ]
        //             },
        //             {
        //                 "featureType": "all",
        //                 "elementType": "geometry.stroke",
        //                 "stylers": [
        //                     {
        //                         "color": "#9c9c9c"
        //                     }
        //                 ]
        //             },
        //             {
        //                 "featureType": "all",
        //                 "elementType": "labels.text",
        //                 "stylers": [
        //                     {
        //                         "visibility": "on"
        //                     }
        //                 ]
        //             },
        //             {
        //                 "featureType": "landscape",
        //                 "elementType": "all",
        //                 "stylers": [
        //                     {
        //                         "color": "#f2f2f2"
        //                     }
        //                 ]
        //             },
        //             {
        //                 "featureType": "landscape",
        //                 "elementType": "geometry.fill",
        //                 "stylers": [
        //                     {
        //                         "color": "#ffffff"
        //                     }
        //                 ]
        //             },
        //             {
        //                 "featureType": "landscape.man_made",
        //                 "elementType": "geometry.fill",
        //                 "stylers": [
        //                     {
        //                         "color": "#ffffff"
        //                     }
        //                 ]
        //             },
        //             {
        //                 "featureType": "poi",
        //                 "elementType": "all",
        //                 "stylers": [
        //                     {
        //                         "visibility": "off"
        //                     }
        //                 ]
        //             },
        //             {
        //                 "featureType": "road",
        //                 "elementType": "all",
        //                 "stylers": [
        //                     {
        //                         "saturation": -100
        //                     },
        //                     {
        //                         "lightness": 45
        //                     }
        //                 ]
        //             },
        //             {
        //                 "featureType": "road",
        //                 "elementType": "geometry.fill",
        //                 "stylers": [
        //                     {
        //                         "color": "#eeeeee"
        //                     }
        //                 ]
        //             },
        //             {
        //                 "featureType": "road",
        //                 "elementType": "labels.text.fill",
        //                 "stylers": [
        //                     {
        //                         "color": "#7b7b7b"
        //                     }
        //                 ]
        //             },
        //             {
        //                 "featureType": "road",
        //                 "elementType": "labels.text.stroke",
        //                 "stylers": [
        //                     {
        //                         "color": "#ffffff"
        //                     }
        //                 ]
        //             },
        //             {
        //                 "featureType": "road.highway",
        //                 "elementType": "all",
        //                 "stylers": [
        //                     {
        //                         "visibility": "simplified"
        //                     }
        //                 ]
        //             },
        //             {
        //                 "featureType": "road.arterial",
        //                 "elementType": "labels.icon",
        //                 "stylers": [
        //                     {
        //                         "visibility": "off"
        //                     }
        //                 ]
        //             },
        //             {
        //                 "featureType": "transit",
        //                 "elementType": "all",
        //                 "stylers": [
        //                     {
        //                         "visibility": "off"
        //                     }
        //                 ]
        //             },
        //             {
        //                 "featureType": "water",
        //                 "elementType": "all",
        //                 "stylers": [
        //                     {
        //                         "color": "#46bcec"
        //                     },
        //                     {
        //                         "visibility": "on"
        //                     }
        //                 ]
        //             },
        //             {
        //                 "featureType": "water",
        //                 "elementType": "geometry.fill",
        //                 "stylers": [
        //                     {
        //                         "color": "#c8d7d4"
        //                     }
        //                 ]
        //             },
        //             {
        //                 "featureType": "water",
        //                 "elementType": "labels.text.fill",
        //                 "stylers": [
        //                     {
        //                         "color": "#070707"
        //                     }
        //                 ]
        //             },
        //             {
        //                 "featureType": "water",
        //                 "elementType": "labels.text.stroke",
        //                 "stylers": [
        //                     {
        //                         "color": "#ffffff"
        //                     }
        //                 ]
        //             }
        //         ]
        //     };
        //     this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
        //     let marker = new google.maps.Marker({
        //         position: latLng,
        //         map: this.map,
        //         title: this.navRequest.descripcion_centro
        //     });
        //     let info = new google.maps.InfoWindow({
        //         content: '<div>'+this.navRequest.descripcion_centro+'</div>'
        //     })
        //     marker.addListener('click', () => {
        //         info.open(this.map, marker);
        //     })
        // })
    }
    OnModalImage(image){
        let modal = this.modalCtrl.create(ModalImageQuotePage, image)
        modal.present()
    }
    openMaps(){
        // ios
        if (this.platform.is('ios')) {
          window.open('maps://?q=' + this.navRequest.locacion+",ecuador" + '&saddr=' + this.lat + ',' + this.lon + '&daddr=' + this.lat + ',' + this.lon, '_system');
        };
        // android
        if (this.platform.is('android')) {
          window.open("geo://" +this.lat+","+this.lon+"?q="+this.navRequest.locacion+",ecuador", '_system','location = yes')
        };
    }
    cancelarCita(datosCitas){
        this.quotesProvider.dataDeleteQuote = {
            hora_turno: this.navRequest.hora_turno,
            id_horario: this.navRequest.hm_fk_horario,
            id_local: this.navRequest.id,
            id_turno: this.navRequest.numero_turno
        }
        this.showLoading()
        this.quotesProvider.deleteQuote().then(data => {
            this.response = data;
            if(this.response.response == true){

                    this.loading.dismiss();
                    this.showError(this.response.result, "Exito");
                    this.navCtrl.pop();
                    var startDate =  new  Date (this.navRequest.hora_turno);
                    var endDate =  new  Date (this.navRequest.hora_turno);
                    this.calendar.requestReadWritePermission();
                    // this.calendar.deleteEvent('','','',startDate,endDate)
            }
            else{
                var startDate =  new  Date (this.navRequest.hora_turno);
                var endDate =  new  Date (this.navRequest.hora_turno);
                this.calendar.requestReadWritePermission();
                // this.calendar.deleteEvent('','','',startDate,endDate)
                this.loading.dismiss()
                this.showError(this.response.result, "Error");
            }
        })
    }
  
    scroll(){
        let el=this.elRef.nativeElement.querySelector('note');
        // this.setCaretPosition(el,0);
        setTimeout(()=>{
        this.content.scrollTo(0,450,600);
        },0);
       
        console.log(this.content._fTop);
        console.log(this.content._ftrHeight)
        console.log(this.content.scrollTop)
    }
    
    sendNote(){
        console.log(this.navRequest);
        this.quotesProvider.remoteNote = {
            horario: this.navRequest.hm_fk_horario,
            notas: this.textArea,
            turno: this.navRequest.numero_turno
        }
        console.log(this.quotesProvider.remoteNote);
        this.quotesProvider.setRemoteNote().then(data =>{
            
        })
        this.quotesProvider.setNotasData = {
            horario: this.navRequest.hm_fk_horario,
            notas: this.textArea,
            turno: this.navRequest.numero_turno
        }
        

        
        
        let alert = this.alertCtrl.create({
            title: 'Confirmar Envío',
            message: '¿Autoriza enviar datos médicos?',
            buttons: [
              {
                text: 'No',
                role: 'cancel',
                handler: () => {

                }
              },
              {
                text: 'Si',
                handler: () => {
                    this.quotesProvider.setNotas().then(data => {
                        this.presentAlert(); 
                    })
                }
              }
            ]
          });
          alert.present();
    }

    presentAlert() {
        let alert = this.alertCtrl.create({
          title: 'Confirmación de grabado',
          subTitle: 'La nota se ha grabado exitósamente.',
          buttons: ['Aceptar']
        });
        alert.present();
      }

    showLoading(){
      this.loading = this.loadingCtrl.create({
          content: "Cargando información\n espere por favor...",
          dismissOnPageChange: false
      })
      this.loading.present()
    }

    showError(text:string, title:string){
        let alert = this.alertCtrl.create({
            title: title,
            subTitle: text,
            buttons:['OK']
        })
        alert.present(prompt)
    }
    goToHome(){
        this.navCtrl.setRoot(HomePage)
    }
    goToQuotes(){
        this.navCtrl.setRoot(MenuQuotesPage)
    }
    goToSpecialite(){
        this.navCtrl.setRoot(MenuSpecialtiesPage)
    }
    goToFavorite(){
        this.navCtrl.setRoot(FavoriteDoctorsPage)
    }
    gotoQuotesDispensary(){
      this.navCtrl.setRoot(MenuQuotesDispensaryPage);
    }
    rescheduleQuote(){
        console.log(this.navRequest);
        this.quotesProvider.id=this.navRequest.id;
        console.log(this.quotesProvider.id);
        // this.quotesProvider.getNota().then(data => {
        //     // console.log(data);
        //     this.navRequest.note=data[0].notas;
        // })
        
        this.navCtrl.push(ReschedulePage, this.navRequest)
    }
}
