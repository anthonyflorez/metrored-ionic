import { Component, ViewChild, ElementRef } from '@angular/core';
import { Content } from 'ionic-angular';
import { Storage } from "@ionic/storage";
import { NavController, NavParams, LoadingController, Loading, AlertController, ModalController } from 'ionic-angular';
import { QuotePage } from "../quote/quote";
import { CenterPage } from "../apply-quotes/quotes-settings/settings-center/center";
import { SettingSpecialtyPage } from "../apply-quotes/quotes-settings/settings-specialty/settings-specialty";
import { DoctorPage } from "../apply-quotes/quotes-settings/settings-doctor/doctor";
import { QuotesProvider } from "../../../providers/quotes/quotes";
import { CentersProvider } from "../../../providers/centers/centers";
import { SpecialtyProvider } from "../../../providers/specialty/specialty";
import { ModalSelectPage } from "../../modal-select/modal-select";
import { HomePage } from "../../home/home";
import { MenuQuotesPage } from "../menu-quotes/menu-quotes";
import { MenuSpecialtiesPage } from "../../specialties/menu-specialties";
import { FavoriteDoctorsPage } from "../../favorite-doctors/favorite-doctors";
import { SearchQuotesPage } from "../search-quotes/search-quotes";
import { MenuQuotesDispensaryPage } from "../../quotes-dispensary/menu-quotes-dispensary/menu-quotes-dispensary";
import { Calendar } from '@ionic-native/calendar';
import { SettingsDoctorsPage } from "../apply-quotes/quotes-settings/settings-doctors/settings-doctors";
import { Keyboard } from '@ionic-native/keyboard';
/**
 * Generated class for the ReschedulePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@Component({
  selector: 'page-reschedule',
  templateUrl: 'reschedule.html',
  providers: [QuotesProvider, CentersProvider, SpecialtyProvider]
})
export class ReschedulePage {
    @ViewChild('map') mapElement: ElementRef;
    @ViewChild(Content) content: Content;
    @ViewChild('note') container: ElementRef;
  public navRequest:any;
  public cita: {
     descripcion_especialidad:string,
     id_especialidad:number,
     descripcion_centro:string,
     disabled_centro: boolean,
     id_centro:number,
     disabled_doctor:boolean,
     doctor: any,
     nombre_doctor:string,
     disabled_reserva:boolean,
     descripcion_doctor: string
     disabled_doctors: boolean,
     fecha_old:string,
     id_doctor: number,
     paciente:string
  };
  loading: Loading;
  responseValidateTurn:any;
  responseLocal:any;
  response: any;
  responseCenter: any
  responsSpecialite: any
  responseBeforeQuote: any
  visibleDispensary: boolean = false
  responseDoctor: any
  view:boolean
  notas:any
  constructor(public navCtrl: NavController, public navParams: NavParams, public loadingCtrl: LoadingController,
    public alertCtrl: AlertController, private quotesProvider: QuotesProvider, public modalCtrl: ModalController,
    private centerProvider: CentersProvider, private specialtyProvider: SpecialtyProvider, private storage: Storage,
    private calendar: Calendar,public elRef: ElementRef,private keyboard: Keyboard){
    storage.get('dispensario').then(data => {
      if(data == true){
        this.visibleDispensary = true
      }
    });
    this.keyboard.onKeyboardShow().subscribe(() => {
        this.view=false;
     }); 
      this.keyboard.onKeyboardHide().subscribe(() => {
         this.view=true;
     }); 
     if(navParams.data.push){
        this.cita = {
            id_especialidad: 0, 
            descripcion_especialidad: 'Especialidad',
            descripcion_centro: 'Centro Médico',
            disabled_centro: false,
            id_centro: 0,
            disabled_doctor: false,
            doctor: {},
            nombre_doctor: 'Cita',
            disabled_reserva: false,
            descripcion_doctor: 'Doctor',
            disabled_doctors: false,
            fecha_old:'',
            id_doctor: 0,
            paciente: navParams.data.nombres+" "+navParams.data.apellidos
        }
     }
   console.log(navParams.data.push);
   console.log(this.navRequest)

  

    this.cita = {
        id_especialidad: 0, 
        descripcion_especialidad: 'Especialidad',
        descripcion_centro: 'Centro Médico',
        disabled_centro: false,
        id_centro: 0,
        disabled_doctor: false,
        doctor: {},
        nombre_doctor: 'Cita',
        disabled_reserva: false,
        descripcion_doctor: 'Doctor',
        disabled_doctors: false,
        fecha_old:'',
        id_doctor: 0,
        paciente: navParams.data.nombres+" "+navParams.data.apellidos
    }
 
 
    this.navRequest = this.navParams.data;
    let date = new Date(this.navRequest.hora_turno)
    let days = [
        'Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'
    ]
    let months = [
        'Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'
    ]
  
    this.specialtyProvider.especialidad = this.navRequest.especialidad
    this.specialtyProvider.getSpecialiteByName().then(data => {
      this.responsSpecialite = data
      this.cita.descripcion_especialidad = this.responsSpecialite.DESCRIPCION
      this.cita.id_especialidad = this.responsSpecialite.PK_CODIGO
      this.notas=this.navRequest.notas;
      this.centerProvider.centro = this.navRequest.locacion;
      this.centerProvider.getCentroByName().then(dataC => {
        this.responseCenter = dataC
        this.cita.id_centro = this.responseCenter.PK_CODIGO
        this.cita.descripcion_centro = this.responseCenter.DESCRIPCION
        this.quotesProvider.dataMedico = {
            id_centro: this.cita.id_centro,
            id_especialidad: this.cita.id_especialidad,
            nombre: this.navRequest.doctor
        }
        this.quotesProvider.getMedicoName().then(dataM => {
            this.responseDoctor = dataM
            this.cita.descripcion_doctor = this.responseDoctor.result.NOMBRE_MEDICO
            this.cita.id_doctor = this.responseDoctor.result.CODIGO_MEDICO
        })
      })
    })
    this.cita.disabled_centro = true
    this.cita.disabled_doctor = true,
    this.cita.nombre_doctor = days[date.getDay()]+" "+ date.getDate()+" DE "+months[date.getMonth()];
    this.cita.disabled_centro = true
    this.cita.disabled_reserva = true
    this.cita.disabled_doctors = true

   
  }

  ionViewDidLoad() {

  }
  openModal(){
    let modal = this.modalCtrl.create(ModalSelectPage)
    modal.onDidDismiss(data => {
        console.log(data);
    })
    modal.present()
  }
  goToCentro(){
      this.navCtrl.push(CenterPage, this.cita)
  }
  goToDoctors(){

    this.navCtrl.push(SettingsDoctorsPage, this.cita)
    console.log(this.navCtrl)
    
  }
  goToQuote(){
      
      this.showLoading()
      let countDoctor = Object.keys(this.cita.doctor).length
      if(countDoctor > 0){
        // //aca van las variables de session
        this.quotesProvider.addQuote = {
            apellidos: this.navRequest.apellidos,
            celular: this.navRequest.celular,
            email: this.navRequest.email,
            hora_turno: this.cita.doctor.turno.FECHA_HORA,
            horario: this.cita.doctor.medico.CODIGO_HORARIO,
            nombres: this.navRequest.nombres,
            paciente: this.navRequest.hm_fk_persona,
            turno: this.cita.doctor.turno.NUMERO_TURNO,
            cedula: (typeof  this.navRequest.documento == "undefined")? this.navRequest.cedula : this.navRequest.documento
        }
        this.quotesProvider.addQuoteLocal = {
            paciente: this.navRequest.hm_fk_persona,
            hora_turno: this.cita.doctor.medico.HORA_INICIO_TURNO.split(' ')[0]+" "+this.cita.doctor.turno.FECHA_HORA.split(' ')[1],
            hora_final_turno: this.cita.doctor.medico.FECHA_FINAL_TURNO,
            horario: this.cita.doctor.medico.CODIGO_HORARIO,
            paciente_local: this.navRequest.ob_hm_fk_persona,
            tipo_persona: this.navRequest.tipo_persona,
            turno: this.cita.doctor.turno.NUMERO_TURNO,
            locacion:this.cita.doctor.medico.DESCRIPCION,
            doctor: this.cita.descripcion_doctor,
            especialidad: this.cita.descripcion_especialidad,
            mejor_dia:1,
            estado:1,
            fecha_old:this.cita.fecha_old,
            fecha_old_final:this.navParams.data.hora_final_turno
        }
        this.quotesProvider.validateTurnData = {
            id: this.navRequest.ob_hm_fk_persona,
            fecha: this.cita.doctor.turno.FECHA_HORA,
            tipo_paciente: this.navRequest.tipo_persona
        }
        this.quotesProvider.beforeQuote = {
          horario: this.navRequest.hm_fk_horario,
          id: this.navRequest.id,
          turno: this.navRequest.numero_turno
        }
        let send_data = {
          apellido: this.navRequest.apellidos,
          doctor : this.cita.descripcion_doctor,
          especialidad: this.cita.descripcion_especialidad,
          hora_turno: this.cita.doctor.medico.HORA_INICIO_TURNO.split(' ')[0]+" "+this.cita.doctor.turno.FECHA_HORA.split(' ')[1],
          locacion: this.cita.doctor.medico.DESCRIPCION,
          nombres: this.navRequest.nombres,
          notas : this.notas
        }
        this.quotesProvider.setNotasData = {
            horario: this.cita.doctor.medico.CODIGO_HORARIO,
            notas: this.notas,
            turno: this.cita.doctor.turno.NUMERO_TURNO,
        }

        this.quotesProvider.validateTurn().then(dataV => {
            this.responseValidateTurn = dataV;
            if(this.responseValidateTurn.response != false){
              this.quotesProvider.rescheduleQuote().then(dataB => {
                this.responseBeforeQuote = dataB
                if(this.responseBeforeQuote.response != false){
                  this.quotesProvider.addQuotesLocal().then(dataL => {
          
                    this.responseLocal = dataL;
                    if(this.responseLocal.reponse != false){
                        this.quotesProvider.addQuotes().then(dataR => {
                            this.response = dataR;
                            if(this.response.response == true){
                                this.loading.dismiss();
                                console.log(this.cita);
                                this.quotesProvider.setNotas2().then(data => {
                                    console.log("se envio")
                                })
                                var newstartDate = new Date(this.cita.doctor.turno.FECHA_HORA.split(' ')[0]+" "+this.cita.doctor.turno.FECHA_HORA.split(' ')[1]); // beware: month 0 = january, 11 = december
                                var newendDate = new Date(this.cita.doctor.medico.FECHA_FINAL_TURNO.split(' ')[0]+" "+this.cita.doctor.medico.FECHA_FINAL_TURNO.split(' ')[1]);
                                var startDate = new Date(this.cita.fecha_old.split(' ')[0]+" "+this.cita.fecha_old.split(' ')[1]); // beware: month 0 = january, 11 = december
                                var endDate = new Date(this.navParams.data.hora_final_turno.split(' ')[0]+" "+this.navParams.data.hora_final_turno.split(' ')[1]);
                                var title = "Cita Médica";
                                var eventLocation = this.cita.descripcion_centro+" ECUADOR";
                                var notes = "LUGAR: "+this.cita.descripcion_centro+"\n"+"MÉDICO: "+this.cita.descripcion_doctor+"\n"+this.cita.paciente.toUpperCase()+"\n"+"HORA: "+this.cita.doctor.turno.FECHA_HORA.split(' ')[1];
                                var success = function(message) { alert("Success: " + JSON.stringify(message)); };
                                var error = function(message) { alert("Error: " + message); };
                                this.calendar.deleteEvent('','','',startDate,endDate);
                                // this.calendar.createEvent(title,eventLocation,notes, newstartDate,newendDate);
                                let send = {datos: send_data, vista: 1}
                                this.navCtrl.setRoot(QuotePage, send)
                                

                                
                            }else{
                                this.loading.dismiss();
                                this.showError(this.response.result);
                            }
                        })
                    }else{
                        this.loading.dismiss();
                        this.showError(this.responseLocal.result);
                        this.cita.disabled_reserva = false;
                    }
                  })
                }
              })
            }else{
                this.loading.dismiss();
                this.showError(this.responseValidateTurn.result);
                this.cita.disabled_reserva = false;
            }
        });
      }
      else{
        this.loading.dismiss();
        this.navCtrl.setRoot(SearchQuotesPage)
      }
  }
  goToEspecialidad(){
      this.navCtrl.push(SettingSpecialtyPage, this.cita)
  }
  goToDoctor(){
      this.cita.fecha_old=this.navRequest.hora_turno;
      this.navCtrl.push(DoctorPage, this.cita)
  }
  showLoading(){
    this.loading = this.loadingCtrl.create({
        content: "Registrando Cita por favor espere ...",
        dismissOnPageChange: false
    })
    this.loading.present()
  }
  showError(text:string){
      let alert = this.alertCtrl.create({
          title: "Error",
          subTitle: text,
          buttons:['OK']
      })
      alert.present(prompt)
  }
  goToHome(){
      this.navCtrl.setRoot(HomePage)
  }
  goToQuotes(){
      this.navCtrl.setRoot(MenuQuotesPage)
  }
  goToSpecialite(){
      this.navCtrl.setRoot(MenuSpecialtiesPage)
  }
  goToFavorite(){
      this.navCtrl.setRoot(FavoriteDoctorsPage)
  }
  gotoQuotesDispensary(){
    this.navCtrl.setRoot(MenuQuotesDispensaryPage);
  }
  scroll(){
    let el=this.elRef.nativeElement.querySelector('note');
    // this.setCaretPosition(el,0);
    setTimeout(()=>{
    this.content.scrollTo(0,450,600);
    },0);
   
    console.log(this.content._fTop);
    console.log(this.content._ftrHeight)
    console.log(this.content.scrollTop)
}
}
