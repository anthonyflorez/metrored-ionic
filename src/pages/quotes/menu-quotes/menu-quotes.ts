import { Component } from '@angular/core';
import { NavController, NavParams} from 'ionic-angular';
import { Storage } from "@ionic/storage";
import { HomePage } from "../../home/home";
import { ApplyQuotesPage } from "../apply-quotes/apply-quotes";
import { SearchQuotesPage } from "../search-quotes/search-quotes";
import { OptionsQuotesPage } from "../options-quotes/options-quotes";
import { HistoryQuotesPage } from "../history-quotes/history-quotes";
import { MenuSpecialtiesPage } from "../../specialties/menu-specialties";
import { FavoriteDoctorsPage } from "../../favorite-doctors/favorite-doctors";
import { MenuQuotesDispensaryPage } from "../../quotes-dispensary/menu-quotes-dispensary/menu-quotes-dispensary";
import { RegisterBeneficiaryPage } from "../../register-beneficiary/register-beneficiary";
import { ListBenefiaryPage } from "../../register-beneficiary/list-beneficiary/list-beneficiary";
@Component({
    selector: 'page-menu-quotes',
    templateUrl: 'menu-quotes.html',
})
export class MenuQuotesPage{
    pages: Array<{title: string, component: any, img:string}>;
    visibleDispensary: boolean = false
    constructor(public navCtrl: NavController, public navParams: NavParams, private storage: Storage){
        storage.get('dispensario').then(data => {
          if(data == true){
            this.visibleDispensary = true
          }
        });
        this.pages = [
            { title: 'Solicitar Citas', component: ApplyQuotesPage, img: 'assets/img/icono-p5-80.png' },
            { title: 'Citas Programadas', component: SearchQuotesPage, img: 'assets/img/icono-p6-80.png' },
            { title: 'Histórico Citas', component: HistoryQuotesPage, img: 'assets/img/icono-p8-80.png' },
            { title: 'Agregar Dependiente', component: ListBenefiaryPage, img: 'assets/img/icono-p28-80.png' },
        ]
    }
    ionViewDidLoad() {
        console.log('ionViewDidLoad RegisterPage');
    }
    goToPage(p){
        this.navCtrl.push(p.component);
    }
    goToHome(){
        this.navCtrl.setRoot(HomePage);
    }
    goToQuotes(){
        this.navCtrl.setRoot(MenuQuotesPage)
    }
    gotoQuotesDispensary(){
      this.navCtrl.setRoot(MenuQuotesDispensaryPage);
    }
    goToSpecialties(){
        this.navCtrl.setRoot(MenuSpecialtiesPage)
    }
    goToFavorite(){
        this.navCtrl.setRoot(FavoriteDoctorsPage)
    }
}