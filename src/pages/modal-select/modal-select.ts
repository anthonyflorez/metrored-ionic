import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';

/**
 * Generated class for the ModalSelectPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@Component({
  selector: 'page-modal-select',
  templateUrl: 'modal-select.html',
})
export class ModalSelectPage {

  afiliado:any
  beneficiarios:any
  keys:any
  constructor(public navCtrl: NavController, public navParams: NavParams, public viewController: ViewController) {
    this.afiliado = this.navParams.data.afiliado;
    this.beneficiarios = this.navParams.data.beneficiarios

  }

  ionViewDidLoad() {
  }
  selectPaciente(item, tipo){
    this.viewController.dismiss(item);
  }
  close(){
    this.viewController.dismiss()
  }
}
