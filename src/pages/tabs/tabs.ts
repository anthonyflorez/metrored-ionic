import { Component } from '@angular/core';
import { IonicPage, NavController, MenuController } from 'ionic-angular';
import { PostsProvider } from "../../providers/posts/posts";
import { MenuQuotesPage } from "../quotes/menu-quotes/menu-quotes";
import { MenuSpecialtiesPage } from "../specialties/menu-specialties";
import { ArticlePage } from "../article/article";
import { FavoriteDoctorsPage } from "../favorite-doctors/favorite-doctors";
import { MenuQuotesDispensaryPage } from "../quotes-dispensary/menu-quotes-dispensary/menu-quotes-dispensary";
import { Storage } from "@ionic/storage";
import { TokenPushProvider } from "../../providers/token-push/token-push";
import { HomePage } from "../home/home";

import { Push, PushToken } from "@ionic/cloud-angular";

@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html',
  providers: [TokenPushProvider]
})
export class TabsPage {
  posts:any;
  idPersona:number
  countAds: number
  visibleImage: boolean = false
  visibleSlider: boolean = false
  visibleDispensary: boolean = true
  promotions: any

  tab0Root = HomePage;
  tab1Root = MenuQuotesPage;
  tab2Root = MenuQuotesDispensaryPage;
  tab3Root = MenuSpecialtiesPage;
  tab4Root = FavoriteDoctorsPage;


  constructor(public navCtrl: NavController, public postsProvider: PostsProvider, private menuCtrl: MenuController, private storage: Storage,
              private tokenPushProvider: TokenPushProvider, public push: Push) {
    storage.get('dispensario').then(data => {
      if(data == true){
        this.visibleDispensary = true
      }
    });
    
    this.menuCtrl.enable(true);  
    this.storage.get('persona').then(data => {
      this.tokenPushProvider.addTokenData = {
        alias: push.token.id,
        id: data.id,
        token: push.token.token
      }
      this.tokenPushProvider.addToken().then(data => {
      });

    })
  }
  gotoQuotes(){
    this.navCtrl.setRoot(MenuQuotesPage);
  }

  gotoQuotesDispensary(){
    this.navCtrl.setRoot(MenuQuotesDispensaryPage);
  }

  goToSpecialties(){
    this.navCtrl.setRoot(MenuSpecialtiesPage)
  }

  goToArticle(post){
    this.navCtrl.push(ArticlePage, post)
  }
  goToFavorite(){
        this.navCtrl.setRoot(FavoriteDoctorsPage)
  }
  viewArticle(post){
    this.navCtrl.push(ArticlePage, post)
  }
}