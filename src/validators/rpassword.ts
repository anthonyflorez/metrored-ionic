import { AbstractControl } from "@angular/forms";

export class ValidateRpassword{
    static checkPassword(aControl: AbstractControl) {
            let password = aControl.get('password').value;
            let rpassword = aControl.get('rpassword').value;
            if(password != rpassword){
                aControl.get('rpassword').setErrors({MatchPassword: true})
            }
            else{
               return null;
            }
    }
}