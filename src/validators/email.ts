import { FormControl } from "@angular/forms";
import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
@Injectable()
export class EmailValidator{

    constructor(private http: Http){}

    checkEmail(control: FormControl): any{
        let headers = new Headers();
        headers.append('Content-Type','application/json');
        headers.append('Accept','application/json');
        let requestOptions = new RequestOptions({ headers: headers });
        return new Promise(resolve => {
            
            this.http.post('http://restmetrored/auth/validateEmail',{"usuario": control.value.toLowerCase()}, requestOptions)
            .map(res => res.json())
            .subscribe(data => {
                if(data.response == true)
                {
                    resolve(null)
                }
                else
                {
                    resolve({
                        "Email ya se encuentra en registrado": true
                    })
                }
            })
        })
    }
    static mailFormat(control: FormControl): any {
        var EMAIL_REGEX = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
        return new Promise(resolve => {
            if(!EMAIL_REGEX.test(control.value)){
                resolve({
                    'email incorrecto': true
                })
            }
            else{
                resolve(null)
            }
        })
    }
}