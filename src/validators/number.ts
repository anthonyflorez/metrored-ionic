import { FormControl } from "@angular/forms";

export class NumberValidator{

    static checkNumber(control: FormControl): any{
        var NUMBER_REGEX = /^[0-9]+$/;
        return new Promise(resolve => {
            if(!NUMBER_REGEX.test(control.value)){
                resolve({
                    'numero incorrecto': true
                })
            }
            else{
                resolve(null)
            }
        })
    }
    static checkTypeDocument(control: FormControl): any{
        return new Promise(resolve => {
            if(control.value.toLowerCase() === "-1"){
                resolve({
                    'documento incorrecto': true
                })
            }
            else{
                resolve(null)
            }
        })
    }
    static checkParentesco(control: FormControl): any{
        return new Promise(resolve => {
            if(control.value.toLowerCase() === "-1"){
                resolve({
                    'parentesco incorrecto': true
                })
            }
            else{
                resolve(null)
            }
        })
    }
}