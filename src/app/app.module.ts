import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { HttpModule } from "@angular/http";
import { IonicStorageModule } from "@ionic/storage";
import { Geolocation } from "@ionic-native/geolocation";
import { NativeGeocoder } from '@ionic-native/native-geocoder';
import { File } from '@ionic-native/file';
import { FileTransfer } from '@ionic-native/file-transfer';
import { FilePath } from '@ionic-native/file-path';
import { Camera } from "@ionic-native/camera";
import { SecureStorage, SecureStorageObject  } from '@ionic-native/secure-storage';
import { Ionic2RatingModule } from "ionic2-rating"; 
import { BackgroundMode } from '@ionic-native/background-mode';
import { Calendar } from '@ionic-native/calendar';
import { FCM } from '@ionic-native/fcm';
import { InAppBrowser } from '@ionic-native/in-app-browser';

/** inicio providers */
import { ImagesSliderProvider } from '../providers/images-slider/images-slider';
import { AuthProvider } from '../providers/auth/auth';
import { CloudSettings, CloudModule } from "@ionic/cloud-angular";
import { ServiceFecha } from "../providers/service-fecha/service-fecha";
import {
 GoogleMaps
} from '@ionic-native/google-maps';
import { ScreenOrientation } from "@ionic-native/screen-orientation";
import { Network } from '@ionic-native/network';

import { QuotesProvider } from '../providers/quotes/quotes';
import { ConnectivityServiceProvider } from '../providers/connectivity-service/connectivity-service';
import { SpecialtiesProvider } from '../providers/specialties/specialties';
import { EmailValidator } from '../validators/email';
import { Keyboard } from '@ionic-native/keyboard';

/** fin providers */

/** inicio pages */
import { MyApp } from './app.component';
import { TabsPage } from '../pages/tabs/tabs';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { ConfigurationPage } from '../pages/configuration/configuration';
import { TourPage } from "../pages/tour/tour";
import { LoginPage } from "../pages/login/login";
import { ResetPasswordPage } from "../pages/reset-password/reset-password";
import { RegisterPage } from "../pages/register/register";
import { MenuQuotesPage } from "../pages/quotes/menu-quotes/menu-quotes";
import { ApplyQuotesPage } from "../pages/quotes/apply-quotes/apply-quotes";
import { QuotesSettingsPage } from "../pages/quotes/apply-quotes/quotes-settings/quotes-settings";
import { CenterPage } from "../pages/quotes/apply-quotes/quotes-settings/settings-center/center";
import { DoctorPage } from "../pages/quotes/apply-quotes/quotes-settings/settings-doctor/doctor";
import { QuotePage } from "../pages/quotes/quote/quote";
import { SearchQuotesPage } from "../pages/quotes/search-quotes/search-quotes";
import { OptionsQuotesPage } from "../pages/quotes/options-quotes/options-quotes";
import { HistoryQuotesPage } from "../pages/quotes/history-quotes/history-quotes";
import { MenuSpecialtiesPage } from "../pages/specialties/menu-specialties";
import { MedicalSpecialtiesPage } from "../pages/specialties/medical-specialties/medical-specialties";
import { SettingSpecialtyPage } from "../pages/quotes/apply-quotes/quotes-settings/settings-specialty/settings-specialty";
import { RegisterBeneficiaryPage } from "../pages/register-beneficiary/register-beneficiary";
import { ArticlePage } from "../pages/article/article";
import { CentersPage } from '../pages/centers/centers';
import { ModalSelectPage } from "../pages/modal-select/modal-select";
import { ChangePasswordPage } from "../pages/change-password/change-password";
import { FavoriteDoctorsPage } from "../pages/favorite-doctors/favorite-doctors";
import { ReserveQuotesPage } from "../pages/reserve-quotes/reserve-quotes";
import { ReserveQuotesPage2 } from "../pages/reserve-quotes2/reserve-quotes";
import { ReschedulePage } from "../pages/quotes/reschedule/reschedule";
import { ModalImagePage } from "../pages/modal-image/modal-image";
import { ModalImageQuotePage } from "../pages/modal-image-quote/modal-image-quote";
import { MoreInfoDoctorPage } from "../pages/more-info-doctor/more-info-doctor";
import { SpecialitiesCentersPage } from "../pages/specialities-centers/specialities-centers";
import { QuotesDispensaryPage } from "../pages/quotes-dispensary/quotes-settings/quotes-dispensary";
import { SettingDispensaryPage } from "../pages/quotes-dispensary/quotes-settings/settings-dispensary/settings-dispensary";
import { SettingsDoctorPage } from "../pages/quotes-dispensary/quotes-settings/settings-doctor/settings-doctor";
import { SettingsQuotesPage } from "../pages/quotes-dispensary/quotes-settings/settings-quote/settings-quotes";
import { QuoteDispensaryPage } from "../pages/quotes-dispensary/quote/quote-dispensary";
import { MenuQuotesDispensaryPage } from "../pages/quotes-dispensary/menu-quotes-dispensary/menu-quotes-dispensary";
import { HistoryQuotesDispensaryPage } from "../pages/quotes-dispensary/history-quotes-dispensary/history-quotes-dispensary";
import { SearchQuotesDispensaryPage } from "../pages/quotes-dispensary/search-quotes-dispensary/search-quotes-dispensary";
import { RescheduleDispensaryPage } from "../pages/quotes-dispensary/reschedule-dispensary/reschedule-dispensary";
import { ListBenefiaryPage } from "../pages/register-beneficiary/list-beneficiary/list-beneficiary";
import { SettingsDoctorsPage } from "../pages/quotes/apply-quotes/quotes-settings/settings-doctors/settings-doctors";
import {ScrollToModule} from 'ng2-scroll-to';
import { ReschedulePushPage } from "../pages/quotes/reschedule-push/reschedule-push";
/** fin pages */

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { PostsProvider } from '../providers/posts/posts';
import { DetectFocusDirective } from '../directives/detect-focus/detect-focus';
import { CentersProvider } from '../providers/centers/centers';
import { SortByPipe } from '../pipes/sort-by/sort-by';
import { SpecialtyProvider } from '../providers/specialty/specialty';
import { FilterPipe } from '../pipes/filter/filter';
import { FilterDatePipe } from '../pipes/filter-date/filter-date';
import { FilterQuotesHistoryPipe } from '../pipes/filter-quotes-history/filter-quotes-history';
import { TokenPushProvider } from '../providers/token-push/token-push';
import { QuotesDispensaryProvider } from '../providers/quotes-dispensary/quotes-dispensary';
import { ServiceLoading } from "../providers/service-loading/service-loading";


const cloudSettings: CloudSettings = {
  core : {
    app_id: '49e34f00'
  },
  push : {
    sender_id: '686845306399',
    pluginConfig: {
      ios : {
        badge: true,
        sound: true
      }
    }
  }
}

@NgModule({
  declarations: [
    MyApp,
    TabsPage,
    HomePage,
    ListPage,
    ConfigurationPage,
    TourPage,
    LoginPage,
    ResetPasswordPage,
    RegisterPage,
    MenuQuotesPage,
    ApplyQuotesPage,
    QuotesSettingsPage,
    CenterPage,
    DoctorPage,
    QuotePage,
    SearchQuotesPage,
    OptionsQuotesPage,
    HistoryQuotesPage,
    MenuSpecialtiesPage,
    MedicalSpecialtiesPage,
    DetectFocusDirective,
    SortByPipe,
    SettingSpecialtyPage,
    FilterPipe,
    FilterDatePipe,
    RegisterBeneficiaryPage,
    FilterQuotesHistoryPipe,
    ArticlePage,
    CentersPage,
    ModalSelectPage,
    ChangePasswordPage,
    FavoriteDoctorsPage,
    ReserveQuotesPage,
    ReserveQuotesPage2,
    ReschedulePage,
    ReschedulePushPage,
    ModalImagePage,
    ModalImageQuotePage,
    MoreInfoDoctorPage,
    SpecialitiesCentersPage,
    QuotesDispensaryPage,
    SettingDispensaryPage,
    SettingsQuotesPage,
    SettingsDoctorPage,
    QuoteDispensaryPage,
    MenuQuotesDispensaryPage,
    HistoryQuotesDispensaryPage,
    SearchQuotesDispensaryPage,
    RescheduleDispensaryPage,
    ListBenefiaryPage,
    SettingsDoctorsPage,
  ],
  imports: [
    BrowserModule,
    ScrollToModule.forRoot(),
    IonicModule.forRoot(MyApp,{
      backButtonText: 'Atrás',
      monthNames: ['Enero','Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
      monthShortNames: ['Ene', 'Feb', 'Mar', 'May', 'Jun', 'Jul', 'Ago','Sep', 'Oct', 'Nov', 'Dic'],
      dayNames: ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'],
      dayShortNames: ['Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab'],
      tabsPlacement: 'bottom',
      tabsHideOnSubPages: true
    }),
    [
      HttpModule,
      IonicStorageModule.forRoot({
        name: '__mydb',
        driverOrder: ['sqlite', 'indexeddb', 'websql'],
      })
    ],
    Ionic2RatingModule,
    CloudModule.forRoot(cloudSettings)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    TabsPage,
    HomePage,
    ListPage,
    ConfigurationPage,
    TourPage,
    LoginPage,
    ResetPasswordPage,
    RegisterPage,
    MenuQuotesPage,
    ApplyQuotesPage,
    QuotesSettingsPage,
    CenterPage,
    DoctorPage,
    QuotePage,
    SearchQuotesPage,
    OptionsQuotesPage,
    HistoryQuotesPage,
    MenuSpecialtiesPage,
    MedicalSpecialtiesPage,
    SettingSpecialtyPage,
    RegisterBeneficiaryPage,
    ArticlePage,
    CentersPage,
    ModalSelectPage,
    ChangePasswordPage,
    FavoriteDoctorsPage,
    ReserveQuotesPage,
    ReserveQuotesPage2,
    ReschedulePage,
    ReschedulePushPage,
    ModalImagePage,
    ModalImageQuotePage,
    MoreInfoDoctorPage,
    SpecialitiesCentersPage,
    QuotesDispensaryPage,
    SettingDispensaryPage,
    SettingsDoctorPage,
    SettingsQuotesPage,
    QuoteDispensaryPage,
    MenuQuotesDispensaryPage,
    HistoryQuotesDispensaryPage,
    SearchQuotesDispensaryPage,
    RescheduleDispensaryPage,
    ListBenefiaryPage,
    SettingsDoctorsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ImagesSliderProvider,
    AuthProvider,
    QuotesProvider,
    GoogleMaps,
    ConnectivityServiceProvider,
    SpecialtiesProvider,
    PostsProvider,
    EmailValidator,
    CentersProvider,
    Geolocation,
    NativeGeocoder,
    SpecialtyProvider,
    File,
    FilePath,
    FileTransfer,
    Camera,
    PostsProvider,
    SecureStorage,
    TokenPushProvider,
    BackgroundMode,
    ScreenOrientation,
    Network,
    Calendar,
    QuotesDispensaryProvider,
    InAppBrowser,
    Keyboard,
    ServiceLoading,
    ServiceFecha,
    FCM
  ]
})
export class AppModule {}
