import { Component, ViewChild, ElementRef, ViewChildren, Input } from '@angular/core';
import { Nav, Platform, MenuController, AlertController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Storage } from "@ionic/storage";
import { AuthProvider } from "../providers/auth/auth";
import { TokenPushProvider } from "../providers/token-push/token-push";
import { BackgroundMode } from '@ionic-native/background-mode';
import { Push, PushToken } from "@ionic/cloud-angular";
import { ScreenOrientation } from "@ionic-native/screen-orientation";
import { Network } from '@ionic-native/network';
import { PostsProvider } from "../providers/posts/posts";
import { QuotesProvider } from "../providers/quotes/quotes";
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { NavController, ToastController } from 'ionic-angular';
import { ServiceFecha } from "../providers/service-fecha/service-fecha";
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { ConfigurationPage } from '../pages/configuration/configuration';
import { TourPage } from "../pages/tour/tour";
import { LoginPage } from "../pages/login/login";
import { ResetPasswordPage } from "../pages/reset-password/reset-password";
import { RegisterPage } from "../pages/register/register";
import { MenuQuotesPage } from "../pages/quotes/menu-quotes/menu-quotes";
import { ApplyQuotesPage } from "../pages/quotes/apply-quotes/apply-quotes";
import { ReschedulePage } from "../pages/quotes/reschedule/reschedule";
import { ReschedulePushPage } from "../pages/quotes/reschedule-push/reschedule-push";
import { QuotesSettingsPage } from "../pages/quotes/apply-quotes/quotes-settings/quotes-settings";
import { CenterPage } from "../pages/quotes/apply-quotes/quotes-settings/settings-center/center";
import { DoctorPage } from "../pages/quotes/apply-quotes/quotes-settings/settings-doctor/doctor";
import { QuotePage } from "../pages/quotes/quote/quote";
import { SearchQuotesPage } from "../pages/quotes/search-quotes/search-quotes";
import { OptionsQuotesPage } from "../pages/quotes/options-quotes/options-quotes";
import { MenuSpecialtiesPage } from "../pages/specialties/menu-specialties";
import { MedicalSpecialtiesPage } from "../pages/specialties/medical-specialties/medical-specialties";
import { SettingSpecialtyPage } from "../pages/quotes/apply-quotes/quotes-settings/settings-specialty/settings-specialty";
import { HistoryQuotesPage } from "../pages/quotes/history-quotes/history-quotes";
import { RegisterBeneficiaryPage } from "../pages/register-beneficiary/register-beneficiary";
import { CentersPage } from "../pages/centers/centers";
import { ChangePasswordPage } from "../pages/change-password/change-password";
import { FavoriteDoctorsPage } from "../pages/favorite-doctors/favorite-doctors";
import { ArticlePage } from "../pages/article/article";
import { QuotesDispensaryPage } from "../pages/quotes-dispensary/quotes-settings/quotes-dispensary";
import { Calendar } from '@ionic-native/calendar';
import { MenuQuotesDispensaryPage } from "../pages/quotes-dispensary/menu-quotes-dispensary/menu-quotes-dispensary";
import { ServiceLoading } from "../providers/service-loading/service-loading";
import { FCM } from '@ionic-native/fcm';
import { Toast } from '@ionic-native/toast';
import { Observable } from '../../node_modules/rxjs/Observable';
//declare var PushbotsPlugin;
declare var cordova: any;

@Component({
  templateUrl: 'app.html',
  providers: [AuthProvider, TokenPushProvider, PostsProvider]
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  rootPage: any = TourPage;


  pages: Array<{ title: string, component: any, img: string }>;
  token: any;
  response: any
  response2: any
  response3: any
  response4: any
  id: any
  alert_present: boolean
  keys: any
  ip:any
  private baseUrl = 'https://www.w3schools.com/';
  private isOnline = false;
  datos:{}



  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen, public menuCtrl: MenuController,
    private storage: Storage, private quotesProvider: QuotesProvider, private authProvider: AuthProvider, private backgroundMode: BackgroundMode, public push: Push,
    private screenOrientation: ScreenOrientation, private network: Network, private postProvider: PostsProvider,
    public alertCtrl: AlertController, public calendar: Calendar, public elemetRef: ElementRef, private iab: InAppBrowser, private fcm: FCM,
    public tokenprovider: TokenPushProvider, private toastCtrl: ToastController, private servicefecha: ServiceFecha) {
    this.alert_present = false
    // this.datos={
    //   data:{
    //     APELLIDOS:"florez",
    //     CEDULA:"2301708455",
    //     CELULAR:"111111111",
    //     CODIGO_HORARIO_NEW:"25462401",
    //     DESCRIPCION:"METRORED CAROLINA",
    //     DOCTOR_OLD:"GRANADO BENAVIDES ALMA LUCIA",
    //     EMAIL:"xavierflorezxx@gmail.com",
    //     ESPECIALIDAD:"GASTROENTEROLOGIA",
    //     FECHA_FINAL_NEW:"2018-08-14 00:00:00",
    //     FECHA_FINAL_OLD:"2018-08-28 18:10",
    //     FECHA_INICIAL_OLD:"2018-08-28 17:50",
    //     HORA_FINAL_TURNO:"2018-08-14 17:10",
    //     HORA_INICIO_TURNO:"2018-08-14 16:50",
    //     ID:"20630",
    //     NOMBRES:"xavier",
    //     NOMBRE_MEDICO_NEW:"GRANADO BENAVIDES ALMA LUCIA",
    //     NUMERO_TURNO:"2",
    //     NUMERO_TURNO_OLD:"5",
    //     OB_HM_FK_HORARIO_OLD:"25462601",
    //     OB_HM_FK_PERSONA_OLD:"4481",
    //     PK_CODIGO_NEW:"729",
    //     TIPO_MENSAJE:"1",
    //     wasTapped:false
    //   },
    //   push: true
    // }




    this.platform.pause.subscribe((res) => {
      this.storage.get('dispensario').then((data) => {
        if (data == true) {
          this.storage.get('persona').then(data2 => {
            this.authProvider.id = data2.id
            this.authProvider.UpdatePersonDispensario().then(data3 => {
              this.response4 = data3
              if (this.response4.result == false) {
                this.presentAlert('Atención', 'Hemos actualizado nuestra aplicacion, por favor reinicia tu cuenta.', 'Aceptar')
                this.storage.remove('persona');
                this.storage.remove('session');
                this.storage.remove('dispensario');
                this.nav.setRoot(LoginPage);
              }
            })

          })
        }

        this.storage.get('persona').then(datas => {
          console.log(datas)
          this.authProvider.validatePhone(datas.id).then(data => {
            this.response = data
            if (this.response.response == false) {
              this.presentConfirm2('Aviso', 'Tienes actualizar tu número celular', 'cancel', 'aceptar')
            }
          });
        })
      })
      this.fcm.getToken().then(token => {
        console.log("****1****")

        console.log(token)

        console.log("****1****")
        this.storage.get('persona').then((data) => {
          this.tokenprovider.get_token = {
            id_persona: data.id
          }
          this.tokenprovider.getTokenPush().subscribe(data => {
            this.response3 = data
            console.log(this.response3)
            if (String(this.response3.result.token_conection) == String(token)) {
            } else {
              this.presentAlert('Atención', 'Tienes una sesion abierta en otro dispositivo movil', 'Aceptar')
              this.storage.remove('persona');
              this.storage.remove('session');
              this.storage.remove('dispensario');
              this.nav.setRoot(LoginPage);
            }
          })
        })
      })

      this.storage.get('persona').then((data) => {
        this.quotesProvider.id = data.id;
        this.id = data.id
        this.quotesProvider.historyQuotes().then(dataR => {

          this.response = dataR;
          if (typeof this.response.result != 'string') {
            this.response = this.response.result;
            this.keys = Object.keys(this.response);
            for (const key in this.response) {
              if (this.response.hasOwnProperty(key)) {
                const element = this.response[key];
                console.log(element)
                for (const key2 in element) {
                  console.log(element);
                  if (element.hasOwnProperty(key2)) {
                    const element2 = element[key2];
                    var title = "Cita Médica";
                    var eventLocation = element2.locacion + " ECUADOR";
                    var notes = "LUGAR: " + element2.locacion + "\n" + "MÉDICO: " + element2.doctor + "\n" + element2.nombres
                      + " " + element2.apellidos + "\n" + "HORA: " + element2.hora_turno.split(' ')[1];
                    if (element2.fecha_modifica_turno != null && element2.fecha_modifica_turno_final != null) {
                      let startDate1 = this.servicefecha.convertirModificaturno_inicio(element2)
                      let endDate1 = this.servicefecha.convertirModificaturno_final(element2)
                      this.calendar.deleteEvent("", "", "", startDate1, endDate1);
                    }
                    if (element2.hora_turno != null && element2.hora_final_turno != null) {
                      let startDate1 = this.servicefecha.convertirhora_turno_inicio(element2)
                      let endDate1 = this.servicefecha.convertirhora_turno_final(element2)
                      this.calendar.deleteEvent('', '', '', startDate1, endDate1);
                    }

                  }
                  setTimeout(() => {
                    for (const key2 in element) {
                      if (element.hasOwnProperty(key2)) {
                        const element2 = element[key2];
                        if (element2.hora_turno != null && element2.hora_final_turno != null) {
                          let startDate1 = this.servicefecha.convertirhora_turno_inicio(element2)
                          let endDate1 = this.servicefecha.convertirhora_turno_final(element2)
                          this.calendar.createEvent(title, eventLocation, notes, startDate1, endDate1);
                        }
                      }
                    }
                  }, 1000);

                }
              }

            }
          }
        }).catch(err => {
          console.log("error");
        })

        this.quotesProvider.UpdateQuotes().then(dataR => {
          console.log(dataR);
          this.response2 = dataR;
          for (let i = 0; i < this.response2.result.length; i++) {
            const element = this.response2.result[i];
            if (element.hora_turno != null && element.hora_final_turno != null) {
              var title = "Cita Médica";
              var eventLocation = element.locacion + " ECUADOR";
              var notes = "LUGAR: " + element.locacion + "\n" + "MÉDICO: " + element.doctor + "\n" + element.nombres + " " + element.apellidos + "\n" + "HORA: "
              let startDate1 = this.servicefecha.convertirhora_turno_inicio(element)
              let endDate1 = this.servicefecha.convertirhora_turno_final(element)
              this.calendar.deleteEvent("", "", "", startDate1, endDate1);
            }
          }


          Object.keys(this.response2.result).forEach(element => {
            var startDate = new Date(element);
            var endDate = new Date(element);
            this.calendar.deleteEvent("", "", "", startDate, endDate);
          });

        }).catch(err => {
          console.log("error");
        })
      })
    });
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Inicio', component: HomePage, img: 'assets/img/icono-p14-80.png' },
      { title: 'Citas Médicas', component: MenuQuotesPage, img: 'assets/img/icono-p15-80.png' },
      { title: 'Directorio Médico', component: MenuSpecialtiesPage, img: 'assets/img/icono-p16-80.png' },
      // { title: 'Especialidades Médicas', component: MenuSpecialtiesPage, img: 'assets/img/icono-p17-80.png'},
      //{ title: 'Mi familia y amigos', component: ListPage, img: 'assets/img/icono-p18-80.png'},
      { title: 'Resultados de Laboratorio', component: HomePage, img: 'assets/img/icono-p18-80.png' },
      { title: 'Configuraciones', component: ConfigurationPage, img: 'assets/img/icono-p19-80.png' },
      { title: 'Salir', component: ListPage, img: 'assets/img/icono-p20-80.png' }
    ];


  }

  initializeApp() {

    this.storage.get('persona').then((data) => {
      this.quotesProvider.id = data.id;
      console.log(this.quotesProvider.id)
      this.quotesProvider.historyQuotes().then(dataR => {
        console.log(dataR);
      })
    });
    this.alert_present = false;
    this.network.onchange().subscribe(() => {
      this.alert_present = false;
      setTimeout(() => {
        this.network.onDisconnect().subscribe(() => {
          if (this.alert_present == false || navigator.onLine==false) {
            this.presentAlert("Error", "Para poder hacer uso de ésta app necesitas estar conectado a internet.", "Cerrar");
            this.platform.exitApp()
            this.alert_present = true;
          }

        })
      }, 1000);

    });


    this.platform.ready().then(() => {
      this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      // this.push.register().then((t: PushToken) => {

      //   return this.push.saveToken(t)
      // }).then((t: PushToken) => {
      //   //console.log('Token saved:', t.token);
      // });
      // this.fcm.subscribeToTopic('marketing');

      this.fcm.getToken().then(token => {
        this.tokenprovider.id_token = token
        console.log(this.tokenprovider.id_token)
      })

      this.storage.get('persona').then((data) => {
        this.tokenprovider.id_user = data.id
        console.log(this.tokenprovider.id_user)

        this.tokenprovider.addToken().then(data => {

        }).catch(err => {

        });

      });

      this.fcm.onNotification().subscribe(data => {
        this.storage.get('session').then(datas => {
          // console.log("data")
          // console.log(data)
          // console.log("data")
          // console.log(datas)
          // if(datas.isLogin == true ){
          //   console.log("pasa")
          // }
          if (datas.isLogin == true && data.OB_HM_FK_PERSONA_OLD == parseInt(datas.idPersona)) {
            if (data.TIPO_MENSAJE == 1) {

              if (data.wasTapped) {
                let datos = { data: data, push: true }
                this.presentConfirm(datos)
              } else {
                let datos = { data: data, push: true }
                console.log(datos)
                this.presentConfirm(datos)
              };
            }
          }
          else {
            if (data.wasTapped) {
              let datos = { data: data, push: true }
              this.presentToast(data.title, 4000, 'top')
            } else {
              let datos = { data: data, push: true }

              this.presentToast(data.title, 4000, 'top')
            };
          }

        });
      });

      this.fcm.onTokenRefresh().subscribe(token => {
        console.log(token);

      });

      this.fcm.unsubscribeFromTopic('marketing');
      this.push.rx.notification().subscribe((msg) => {
        this.storage.get('persona').then(data => {
          if (data != "undefinded") {
            let type = msg.payload['tipo'];
            if (type == 1) {
              this.postProvider.listUniquePost(msg.payload['id']).then(dataP => {
                this.nav.setRoot(ArticlePage, dataP)
                let alert = this.alertCtrl.create({
                  title: 'Información',
                  subTitle: 'Hola encontramos una mejor fecha pra tu cita con' + msg.payload['data']['detalles']['especialidad'] + "\n desea reprogamar su cita al dia\n" + msg.payload['data']['FECHA_HORA'],
                  buttons: [
                    {
                      text: 'Si',
                      handler: data => {
                      }
                    },
                    {
                      text: 'no',
                      handler: data => {
                      }
                    }
                  ]
                })
                alert.present(prompt)
                console.log(msg.payload)
              })
              let alert = this.alertCtrl.create({
                title: 'Información',
                subTitle: 'Hola encontramos una mejor fecha pra tu cita con' + msg.payload['data']['detalles']['especialidad'] + "\n desea reprogamar su cita al dia\n" + msg.payload['data']['FECHA_HORA'],
                buttons: [
                  {
                    text: 'Si',
                    handler: data => {
                    }
                  },
                  {
                    text: 'no',
                    handler: data => {
                    }
                  }
                ]
              })
              alert.present(prompt)
              console.log(msg.payload)
            }
            if (type == 2) {
              console.log("hemos encontrado una mejor cita")
              let alert = this.alertCtrl.create({
                title: 'Información',
                subTitle: 'Hola encontramos una mejor fecha pra tu cita con' + msg.payload['data']['detalles']['especialidad'] + "\n desea reprogamar su cita al dia\n" + msg.payload['data']['FECHA_HORA'],
                buttons: [
                  {
                    text: 'Si',
                    handler: data => {
                    }
                  },
                  {
                    text: 'no',
                    handler: data => {
                    }
                  }
                ]
              })
              alert.present(prompt)
              console.log(msg.payload)
            }
          } else {
            this.storage.set('previous_page', msg.payload);
          }
        })
      })

      this.storage.get('tour').then(data => {
        if (data == true) {
          this.rootPage = LoginPage
        }
      })
    });
  }
  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    if (page.title == "Salir") {
      this.storage.get('persona').then((data) => {
        console.log(data.id)
        this.tokenprovider.token_id = {
          token: '',
          id_persona: data.id
        }
        this.tokenprovider.setTokenPush().then(data => {
        })
      })
      this.storage.remove('dispensario');
      this.storage.remove('persona');
      this.storage.remove('session');
      this.nav.setRoot(LoginPage);
    }
    else if (page.title == 'Resultados de Laboratorio') {
      const browser = this.iab.create('http://resultados.hmetro.med.ec/Account/Login', "_system");
      browser.show();
    }
    else if (page.title == 'Configuraciones' || page.title == 'Directorio medico') {
      this.nav.push(page.component);
    }
    else {
      this.nav.setRoot(page.component);
    }
  }
  presentAlert(title, subTitle, textbutton) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: subTitle,
      buttons: [textbutton]
    });
    alert.present();
  }
  backMenu() {
    this.menuCtrl.close();
  }

  capitalize(value){
    if (value) {
      return value.charAt(0).toUpperCase() + value.slice(1).toLowerCase();
    }
    return value;
  }
  presentConfirm(datos) {
    console.log(datos)
    let alert = this.alertCtrl.create({
      title: 'Información',
      message: 'Tenemos disponible una mejor fecha en tu cita médica de ' + this.capitalize(datos.data.ESPECIALIDAD)+ ', actualmente tu cita es para el día '+
      this.servicefecha.getfechahumano(datos.data.FECHA_INICIAL_OLD) +', ¿Desea mover la cita para el día '+
      this.servicefecha.getfechahumano(datos.data.HORA_INICIO_TURNO) + '?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
            this.nav.setRoot(HomePage);

          }
        },
        {
          text: 'Aceptar',
          handler: () => {

            this.nav.setRoot(ReschedulePushPage, datos);
          }
        }
      ]
    });
    alert.present();
  }
  presentToast(message, duration, position) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 10000,
      position: 'top',
      cssClass: 'toast-custom'
    });
    toast.present();
  }
  presentConfirm2(title, message, textextbuttonleft, textextbuttonright) {
    let alert = this.alertCtrl.create({
      title: title,
      message: message,
      buttons: [
        {
          text: textextbuttonleft,
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: textextbuttonright,
          handler: () => {
            this.nav.setRoot(ChangePasswordPage)
          }
        }
      ]
    });
    alert.present();
  }
 
}
